package org.ultramine.mods.bukkit.mixin.entity.item;

import net.minecraft.entity.item.EntityEnderPearl;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.entity.projectile.EntityThrowable;
import net.minecraft.util.DamageSource;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.living.EnderTeleportEvent;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.craftbukkit.entity.CraftPlayer;
import org.bukkit.craftbukkit.event.CraftEventFactory;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;
import org.ultramine.mods.bukkit.interfaces.entity.IMixinEntity;
import org.ultramine.mods.bukkit.interfaces.network.IMixinNetHPS;

@Mixin(EntityEnderPearl.class)
public abstract class MixinEntityEnderPearl extends EntityThrowable
{
	public MixinEntityEnderPearl(World p_i1776_1_)
	{
		super(p_i1776_1_);
	}

	@Overwrite
	protected void onImpact(MovingObjectPosition pos)
	{
		if (pos.entityHit != null)
			pos.entityHit.attackEntityFrom(DamageSource.causeThrownDamage(this, this.getThrower()), 0.0F);
		for (int i = 0; i < 32; ++i)
			this.worldObj.spawnParticle("portal", this.posX, this.posY + this.rand.nextDouble() * 2.0D, this.posZ, this.rand.nextGaussian(), 0.0D, this.rand.nextGaussian());
		if (!this.worldObj.isRemote)
		{
			if (this.getThrower() != null && this.getThrower() instanceof EntityPlayerMP)
			{
				EntityPlayerMP entityplayermp = (EntityPlayerMP) this.getThrower();
				if (entityplayermp.playerNetServerHandler.func_147362_b().isChannelOpen() && entityplayermp.worldObj == this.worldObj)
				{
					EnderTeleportEvent event = new EnderTeleportEvent(entityplayermp, this.posX, this.posY, this.posZ, 5);
					if (MinecraftForge.EVENT_BUS.post(event))
					{
						this.setDead();
						return;
					}
					CraftPlayer player = (CraftPlayer) ((IMixinEntity) entityplayermp).getBukkitEntity();
					Location location = ((IMixinEntity) this).getBukkitEntity().getLocation();
					location.setPitch(player.getLocation().getPitch());
					location.setYaw(player.getLocation().getYaw());
					PlayerTeleportEvent teleportEvent = new PlayerTeleportEvent(player, player.getLocation(), location, PlayerTeleportEvent.TeleportCause.ENDER_PEARL);
					Bukkit.getPluginManager().callEvent(teleportEvent);
					if (!teleportEvent.isCancelled() && !((IMixinNetHPS) entityplayermp.playerNetServerHandler).isDisconnected())
					{
						((IMixinNetHPS) entityplayermp.playerNetServerHandler).teleport(teleportEvent.getTo());
						this.getThrower().fallDistance = 0.0F;
						CraftEventFactory.entityDamage = this;
						this.getThrower().attackEntityFrom(DamageSource.fall, 5.0F);
						CraftEventFactory.entityDamage = null;
					}
				}
			}
			this.setDead();
		}
	}
}
