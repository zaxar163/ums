package org.ultramine.mods.bukkit.mixin.block;

import net.minecraft.block.Block;
import net.minecraft.block.BlockPumpkin;
import net.minecraft.world.World;
import org.bukkit.Bukkit;
import org.bukkit.event.block.BlockRedstoneEvent;
import org.spongepowered.asm.mixin.Mixin;
import org.ultramine.mods.bukkit.interfaces.world.IMixinWorld;

@Mixin(BlockPumpkin.class)
public class MixinBlockPumpkin
{
	public void onNeighborBlockChange(World world, int i, int j, int k, Block block)
    {
	    if (block != null && block.canProvidePower())
	    {
		    org.bukkit.block.Block bukkitBlock = ((IMixinWorld) world).getWorld().getBlockAt(i, j, k);
		    int power = bukkitBlock.getBlockPower();
		    BlockRedstoneEvent eventRedstone = new BlockRedstoneEvent(bukkitBlock, power, power);
		    Bukkit.getPluginManager().callEvent(eventRedstone);
	    }
    }
}
