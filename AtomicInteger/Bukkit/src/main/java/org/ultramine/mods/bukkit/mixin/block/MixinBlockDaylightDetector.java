package org.ultramine.mods.bukkit.mixin.block;

import net.minecraft.block.BlockDaylightDetector;
import net.minecraft.util.MathHelper;
import net.minecraft.world.EnumSkyBlock;
import net.minecraft.world.World;
import org.bukkit.craftbukkit.event.CraftEventFactory;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;

@Mixin(BlockDaylightDetector.class)
public class MixinBlockDaylightDetector
{
	@Overwrite
	public void func_149957_e(World world, int x, int y, int z)
	{
		if (!world.provider.hasNoSky)
		{
			int l = world.getBlockMetadata(x, y, z);
			int i1 = world.getSavedLightValue(EnumSkyBlock.Sky, x, y, z) - world.skylightSubtracted;
			float f = world.getCelestialAngleRadians(1.0F);
			if (f < 3.1415927F)
			{
				f += (0.0F - f) * 0.2F;
			}
			else
			{
				f += (6.2831855F - f) * 0.2F;
			}

			i1 = Math.round((float) i1 * MathHelper.cos(f));
			if (i1 < 0)
			{
				i1 = 0;
			}

			if (i1 > 15)
			{
				i1 = 15;
			}

			if (l != i1)
			{
				i1 = CraftEventFactory.callRedstoneChange(world, x, y, z, l, i1).getNewCurrent();
				world.setBlockMetadataWithNotify(x, y, z, i1, 3);
			}
		}
	}
}
