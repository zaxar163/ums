package org.ultramine.mods.bukkit.mixin.entity.ai;

import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.ai.EntityAIBase;
import net.minecraft.entity.ai.EntityAIDefendVillage;
import net.minecraft.entity.ai.EntityAIHurtByTarget;
import net.minecraft.entity.ai.EntityAINearestAttackableTarget;
import net.minecraft.entity.ai.EntityAIOwnerHurtByTarget;
import net.minecraft.entity.ai.EntityAIOwnerHurtTarget;
import net.minecraft.entity.ai.EntityAITarget;
import net.minecraft.entity.player.EntityPlayer;
import org.bukkit.craftbukkit.entity.CraftEntity;
import org.bukkit.event.entity.EntityTargetEvent;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import org.ultramine.mods.bukkit.interfaces.entity.IMixinEntity;
import org.ultramine.mods.bukkit.interfaces.entity.IMixinEntityCreature;

@Mixin(EntityAITarget.class)
public abstract class MixinEntityAITarget extends EntityAIBase
{
	@Shadow protected EntityCreature taskOwner;

	@Inject(method = "isSuitableTarget", at = @At(value = "RETURN", ordinal = 10, shift = At.Shift.BEFORE))
	private void isSuitableTargetInject(EntityLivingBase p_75296_1_, boolean p_75296_2_, CallbackInfoReturnable<Boolean> cir)
	{
		EntityTargetEvent.TargetReason reason = EntityTargetEvent.TargetReason.RANDOM_TARGET;
		EntityAITarget thisObj = (EntityAITarget) (Object) this;
		if (thisObj instanceof EntityAIDefendVillage)
		{
			reason = EntityTargetEvent.TargetReason.DEFEND_VILLAGE;
		}
		else if (thisObj instanceof EntityAIHurtByTarget)
		{
			reason = EntityTargetEvent.TargetReason.TARGET_ATTACKED_ENTITY;
		}
		else if (thisObj instanceof EntityAINearestAttackableTarget)
		{
			if (p_75296_1_ instanceof EntityPlayer)
				reason = EntityTargetEvent.TargetReason.CLOSEST_PLAYER;
		}
		else if (thisObj instanceof EntityAIOwnerHurtByTarget)
		{
			reason = EntityTargetEvent.TargetReason.TARGET_ATTACKED_OWNER;
		}
		else if (thisObj instanceof EntityAIOwnerHurtTarget)
		{
			reason = EntityTargetEvent.TargetReason.OWNER_ATTACKED_TARGET;
		}
		org.bukkit.event.entity.EntityTargetLivingEntityEvent event = org.bukkit.craftbukkit.event.CraftEventFactory.callEntityTargetLivingEvent(this.taskOwner, p_75296_1_, reason);
		if (event.isCancelled() || event.getTarget() == null)
		{
			this.taskOwner.setAttackTarget(null);
			cir.setReturnValue(false);
			return;
		}
		else if (((IMixinEntity) p_75296_1_).getBukkitEntity() != event.getTarget())
		{
			this.taskOwner.setAttackTarget((EntityLivingBase) ((CraftEntity) event.getTarget()).getHandle());
		}
		if (this.taskOwner instanceof EntityCreature)
		{
			((IMixinEntityCreature) this.taskOwner).setEntityToAttack(((CraftEntity) event.getTarget()).getHandle());
		}
	}
}
