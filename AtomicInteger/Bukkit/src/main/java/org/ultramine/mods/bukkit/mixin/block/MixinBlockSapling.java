package org.ultramine.mods.bukkit.mixin.block;

import net.minecraft.block.BlockBush;
import net.minecraft.block.BlockSapling;
import net.minecraft.world.World;
import org.bukkit.Location;
import org.bukkit.TreeType;
import org.bukkit.block.BlockState;
import org.bukkit.event.world.StructureGrowEvent;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;
import org.spongepowered.asm.mixin.Shadow;
import org.ultramine.mods.bukkit.interfaces.world.IMixinWorld;
import org.ultramine.mods.bukkit.util.UMTreeTypeToBukkit;

import java.util.List;
import java.util.Random;

@Mixin(BlockSapling.class)
public abstract class MixinBlockSapling extends BlockBush
{
	@Shadow public abstract boolean func_149880_a(World p_149880_1_, int p_149880_2_, int p_149880_3_, int p_149880_4_, int p_149880_5_);
	@Shadow public abstract void func_149879_c(World p_149879_1_, int p_149879_2_, int p_149879_3_, int p_149879_4_, Random p_149879_5_);

	@Overwrite
	public void updateTick(World world, int x, int y, int z, Random random)
	{
		if (!world.isRemote)
		{
			super.updateTick(world, x, y, z, random);
			if (world.getBlockLightValue(x, y + 1, z) >= 9 && random.nextInt(7) == 0)
			{
				((IMixinWorld) world).setCaptureTreeGeneration(true);
				this.func_149879_c(world, x, y, z, random);
				((IMixinWorld) world).setCaptureTreeGeneration(false);
				if (world.capturedBlockSnapshots.size() > 0)
				{
					TreeType treeType = UMTreeTypeToBukkit.getBukkitTreeType(BlockSapling.treeType);
					BlockSapling.treeType = null;
					Location location = new Location(((IMixinWorld) world).getWorld(), x, y, z);
					List<net.minecraftforge.common.util.BlockSnapshot> blocks = (List<net.minecraftforge.common.util.BlockSnapshot>) world.capturedBlockSnapshots.clone();
					List<BlockState> blockstates = new java.util.ArrayList<>();
					for (net.minecraftforge.common.util.BlockSnapshot snapshot : blocks)
						blockstates.add(new org.bukkit.craftbukkit.block.CraftBlockState(snapshot));
					world.capturedBlockSnapshots.clear();
					StructureGrowEvent event = null;
					if (treeType != null)
					{
						event = new StructureGrowEvent(location, treeType, false, null, blockstates);
						org.bukkit.Bukkit.getPluginManager().callEvent(event);
					}
					if (event == null || !event.isCancelled())
						for (BlockState blockstate : blockstates)
							blockstate.update(true);
				}
			}
		}
	}
}
