package org.ultramine.mods.bukkit.mixin.entity.monster;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.world.World;
import org.bukkit.craftbukkit.entity.CraftEntity;
import org.bukkit.craftbukkit.event.CraftEventFactory;
import org.bukkit.event.entity.EntityTargetEvent;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;
import org.spongepowered.asm.mixin.Shadow;
import org.ultramine.mods.bukkit.interfaces.entity.monster.IMixinEntitySlime;

@Mixin(net.minecraft.entity.monster.EntitySlime.class)
public abstract class MixinEntitySlime extends EntityLiving implements IMixinEntitySlime
{
	public MixinEntitySlime(World p_i1595_1_)
	{
		super(p_i1595_1_);
	}

	@Shadow
	protected abstract void setSlimeSize(int p_70799_1_);

	@Shadow private int slimeJumpDelay;
	@Shadow protected abstract int getJumpDelay();
	@Shadow protected abstract boolean makesSoundOnJump();
	@Shadow protected abstract String getJumpSound();
	@Shadow public abstract int getSlimeSize();
	private Entity lastTarget;

	@Override
	public void setSlimeSizePub(int size)
	{
		setSlimeSize(size);
	}

	@Overwrite
	protected void updateEntityActionState()
	{
		this.despawnEntity();
		Entity entityplayer = this.worldObj.getClosestVulnerablePlayerToEntity(this, 16.0D);
		EntityTargetEvent event = null;
		if (entityplayer != null && !entityplayer.equals(lastTarget))
			event = CraftEventFactory.callEntityTargetEvent(this, entityplayer, EntityTargetEvent.TargetReason.CLOSEST_PLAYER);
		else if (lastTarget != null && entityplayer == null)
			event = CraftEventFactory.callEntityTargetEvent(this, null, EntityTargetEvent.TargetReason.FORGOT_TARGET);
		if (event != null && !event.isCancelled())
			entityplayer = event.getTarget() == null ? null : ((CraftEntity) event.getTarget()).getHandle();
		this.lastTarget = entityplayer;
		if (entityplayer != null)
			this.faceEntity(entityplayer, 10.0F, 20.0F);
		if (this.onGround && this.slimeJumpDelay-- <= 0)
		{
			this.slimeJumpDelay = this.getJumpDelay();
			if (entityplayer != null)
				this.slimeJumpDelay /= 3;
			this.isJumping = true;
			if (this.makesSoundOnJump())
				this.playSound(this.getJumpSound(), this.getSoundVolume(), ((this.rand.nextFloat() - this.rand.nextFloat()) * 0.2F + 1.0F) * 0.8F);
			this.moveStrafing = 1.0F - this.rand.nextFloat() * 2.0F;
			this.moveForward = this.getSlimeSize();
		}
		else
		{
			this.isJumping = false;
			if (this.onGround)
				this.moveStrafing = this.moveForward = 0.0F;
		}
	}
}
