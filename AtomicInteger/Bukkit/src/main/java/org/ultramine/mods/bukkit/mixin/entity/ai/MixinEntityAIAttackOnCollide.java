package org.ultramine.mods.bukkit.mixin.entity.ai;

import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.ai.EntityAIAttackOnCollide;
import org.bukkit.craftbukkit.event.CraftEventFactory;
import org.bukkit.event.entity.EntityTargetEvent;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(EntityAIAttackOnCollide.class)
public class MixinEntityAIAttackOnCollide
{
	@Shadow EntityCreature attacker;

	@Inject(method = "continueExecuting", at = @At(value = "RETURN"))
	private void continueExecutingInject(CallbackInfoReturnable<Boolean> cir)
	{
		EntityTargetEvent.TargetReason reason = this.attacker.getAttackTarget() == null ? EntityTargetEvent.TargetReason.FORGOT_TARGET : EntityTargetEvent.TargetReason.TARGET_DIED;
		if (this.attacker.getAttackTarget() == null || (this.attacker.getAttackTarget() != null && !this.attacker.getAttackTarget().isEntityAlive()))
			CraftEventFactory.callEntityTargetEvent(attacker, null, reason);
	}
}
