package org.ultramine.mods.bukkit.mixin.entity;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityTrackerEntry;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.S12PacketEntityVelocity;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerVelocityEvent;
import org.bukkit.util.Vector;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Redirect;
import org.ultramine.mods.bukkit.interfaces.entity.IMixinEntity;

@Mixin(EntityTrackerEntry.class)
public abstract class MixinEntityTrackerEntry
{
	@Shadow public Entity myEntity;
	@Shadow public abstract void func_151261_b(Packet p_151261_1_);

	@Redirect(method = "sendLocationToAllClients", at = @At(value = "INVOKE", target = "Lnet/minecraft/entity/EntityTrackerEntry;func_151261_b(Lnet/minecraft/network/Packet;)V"))
	private void sendLocationToAllClientsRedirect(EntityTrackerEntry entityTrackerEntry, Packet packet)
	{
		boolean cancelled = false;
		if (this.myEntity instanceof EntityPlayerMP)
		{
			Player player = (Player) ((IMixinEntity) this.myEntity).getBukkitEntity();
			Vector velocity = player.getVelocity();
			PlayerVelocityEvent event = new PlayerVelocityEvent(player, velocity);
			Bukkit.getPluginManager().callEvent(event);
			if (event.isCancelled())
				cancelled = true;
			else if (!velocity.equals(event.getVelocity()))
				player.setVelocity(velocity);
		}
		if (!cancelled)
			this.func_151261_b((new S12PacketEntityVelocity(this.myEntity)));
	}
}
