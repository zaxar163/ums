package org.ultramine.mods.bukkit.mixin.tileentity;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityBrewingStand;
import org.bukkit.Bukkit;
import org.bukkit.event.inventory.BrewEvent;
import org.bukkit.inventory.BrewerInventory;
import org.bukkit.inventory.InventoryHolder;
import org.objectweb.asm.Opcodes;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.ultramine.mods.bukkit.interfaces.tileentity.IMixinTileEntityBrewingStand;
import org.ultramine.mods.bukkit.interfaces.world.IMixinWorld;
import org.ultramine.mods.bukkit.util.BukkitUtil;

@Mixin(net.minecraft.tileentity.TileEntityBrewingStand.class)
public class MixinTileEntityBrewingStand extends TileEntity implements IMixinTileEntityBrewingStand
{
	@Shadow
	private int brewTime;

	@Override
	public void setBrewTime(int brewTime)
	{
		this.brewTime = brewTime;
	}

	@Inject(method = "brewPotions", cancellable = true, at = @At(value = "FIELD", opcode = Opcodes.GETFIELD, target = "Lnet/minecraft/tileentity/TileEntityBrewingStand;brewingItemStacks:[Lnet/minecraft/item/ItemStack;", shift = At.Shift.AFTER, ordinal = 1))
	private void canBrewInject(CallbackInfo ci)
	{
		InventoryHolder inventoryHolder = BukkitUtil.getInventoryOwner((TileEntityBrewingStand)(Object)this);
		if (inventoryHolder != null)
		{
			BrewEvent event = new BrewEvent(((IMixinWorld) worldObj).getWorld().getBlockAt(xCoord, yCoord, zCoord), (BrewerInventory) inventoryHolder.getInventory());
			Bukkit.getPluginManager().callEvent(event);
			if (event.isCancelled())
				ci.cancel();
		}
	}
}
