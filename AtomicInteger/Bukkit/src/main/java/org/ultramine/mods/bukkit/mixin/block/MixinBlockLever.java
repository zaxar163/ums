package org.ultramine.mods.bukkit.mixin.block;

import net.minecraft.block.BlockLever;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;
import org.bukkit.Bukkit;
import org.bukkit.event.block.BlockRedstoneEvent;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;
import org.ultramine.mods.bukkit.interfaces.world.IMixinWorld;

@Mixin(BlockLever.class)
public class MixinBlockLever
{
	@Inject(method = "onBlockActivated", at = @At(value = "INVOKE", target = "Lnet/minecraft/world/World;setBlockMetadataWithNotify(IIIII)Z"), locals = LocalCapture.CAPTURE_FAILEXCEPTION)
	private void onBlockActivatedInject(World world, int x, int y, int z, EntityPlayer player, int p_149727_6_, float p_149727_7_, float p_149727_8_, float p_149727_9_, CallbackInfoReturnable<Boolean> cir, int i1, int j1, int k1)
	{
		org.bukkit.block.Block block = ((IMixinWorld) world).getWorld().getBlockAt(x, y, z);
		int old = (k1 != 8) ? 15 : 0;
		int current = (k1 == 8) ? 15 : 0;
		BlockRedstoneEvent eventRedstone = new BlockRedstoneEvent(block, old, current);
		Bukkit.getPluginManager().callEvent(eventRedstone);
		if ((eventRedstone.getNewCurrent() > 0) != (k1 == 8))
		{
			cir.setReturnValue(true);
			cir.cancel();
		}
	}
}
