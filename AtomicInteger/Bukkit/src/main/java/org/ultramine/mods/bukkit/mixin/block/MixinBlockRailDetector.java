package org.ultramine.mods.bukkit.mixin.block;

import net.minecraft.block.BlockRailBase;
import net.minecraft.block.BlockRailDetector;
import net.minecraft.entity.item.EntityMinecart;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.world.World;
import org.bukkit.Bukkit;
import org.bukkit.event.block.BlockRedstoneEvent;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;
import org.ultramine.mods.bukkit.interfaces.world.IMixinWorld;

import java.util.List;

@Mixin(BlockRailDetector.class)
public class MixinBlockRailDetector extends BlockRailBase
{
	protected MixinBlockRailDetector(boolean p_i45389_1_)
	{
		super(p_i45389_1_);
	}

	@Overwrite
	private void func_150054_a(World world, int x, int y, int z, int p_150054_5_)
	{
		boolean flag = (p_150054_5_ & 8) != 0;
		boolean flag1 = false;
		float f = 0.125F;
		List list = world.getEntitiesWithinAABB(EntityMinecart.class, AxisAlignedBB.getBoundingBox(x + f, y, z + f, (x + 1) - f, (y + 1) - f, (z + 1) - f));
		if (!list.isEmpty())
		{
			flag1 = true;
		}

		if (flag != flag1)
		{
			org.bukkit.block.Block block = ((IMixinWorld) world).getWorld().getBlockAt(x, y, z);
			BlockRedstoneEvent eventRedstone = new BlockRedstoneEvent(block, flag ? 15 : 0, flag1 ? 15 : 0);
			Bukkit.getPluginManager().callEvent(eventRedstone);
			flag1 = eventRedstone.getNewCurrent() > 0;
		}

		if (flag1 && !flag)
		{
			world.setBlockMetadataWithNotify(x, y, z, p_150054_5_ | 8, 3);
			world.notifyBlocksOfNeighborChange(x, y, z, this);
			world.notifyBlocksOfNeighborChange(x, y - 1, z, this);
			world.markBlockRangeForRenderUpdate(x, y, z, x, y, z);
		}

		if (!flag1 && flag)
		{
			world.setBlockMetadataWithNotify(x, y, z, p_150054_5_ & 7, 3);
			world.notifyBlocksOfNeighborChange(x, y, z, this);
			world.notifyBlocksOfNeighborChange(x, y - 1, z, this);
			world.markBlockRangeForRenderUpdate(x, y, z, x, y, z);
		}

		if (flag1)
		{
			world.scheduleBlockUpdate(x, y, z, this, this.tickRate(world));
		}

		world.func_147453_f(x, y, z, this);
	}
}
