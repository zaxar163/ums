package org.ultramine.mods.bukkit.mixin.block;

import net.minecraft.block.BlockEndPortal;
import net.minecraft.entity.Entity;
import net.minecraft.world.World;
import org.bukkit.Bukkit;
import org.bukkit.event.entity.EntityPortalEnterEvent;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.ultramine.mods.bukkit.interfaces.entity.IMixinEntity;
import org.ultramine.mods.bukkit.interfaces.world.IMixinWorld;

@Mixin(BlockEndPortal.class)
public class MixinBlockEndPortal
{
	@Inject(method = "onEntityCollidedWithBlock", at = @At(value = "INVOKE", target = "Lnet/minecraft/entity/Entity;travelToDimension(I)V"))
	private void onEntityCollidedWithBlockInject(World world, int x, int y, int z, Entity entity, CallbackInfo ci)
	{
		EntityPortalEnterEvent event = new EntityPortalEnterEvent(((IMixinEntity) entity).getBukkitEntity(), new org.bukkit.Location(((IMixinWorld) world).getWorld(), x, y, z));
		Bukkit.getPluginManager().callEvent(event);
	}
}
