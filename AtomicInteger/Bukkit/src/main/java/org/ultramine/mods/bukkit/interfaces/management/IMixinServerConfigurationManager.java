package org.ultramine.mods.bukkit.interfaces.management;

import com.mojang.authlib.GameProfile;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.scoreboard.ServerScoreboard;
import net.minecraft.server.network.NetHandlerLoginServer;
import net.minecraft.world.World;
import org.bukkit.Location;

public interface IMixinServerConfigurationManager
{
	EntityPlayerMP attemptLogin(NetHandlerLoginServer loginlistener, GameProfile gameprofile, String hostname, EntityPlayerMP entity);

	void sendScoreboard(ServerScoreboard sb, EntityPlayerMP player);

	// Copy of original a(Entity, int, WorldServer, WorldServer) method with only location calculation logic
	Location calculateTarget(Location enter, World target);

	// copy of original a(Entity, int, WorldServer, WorldServer) method with only entity repositioning logic
	void repositionEntity(Entity entity, Location exit, boolean portal);
}
