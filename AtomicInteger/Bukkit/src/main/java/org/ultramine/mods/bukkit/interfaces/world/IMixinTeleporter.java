package org.ultramine.mods.bukkit.interfaces.world;

import net.minecraft.entity.Entity;
import net.minecraft.util.ChunkCoordinates;
import org.bukkit.Location;
import org.bukkit.util.Vector;

public interface IMixinTeleporter
{
	ChunkCoordinates findPortal(double x, double y, double z, int short1);

	// Entity repositioning logic split out from original b method and combined with repositioning logic for The End from original a method
	void adjustExit(Entity entity, Location position, Vector velocity);

	boolean createPortal(double x, double y, double z, int b0);
}
