package org.ultramine.mods.bukkit.mixin.entity.ai;

import net.minecraft.block.Block;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.ai.EntityAIBreakDoor;
import net.minecraft.entity.ai.EntityAIDoorInteract;
import net.minecraft.init.Blocks;
import net.minecraft.util.MathHelper;
import net.minecraft.world.EnumDifficulty;
import net.minecraft.world.World;
import org.bukkit.Material;
import org.bukkit.craftbukkit.event.CraftEventFactory;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;
import org.spongepowered.asm.mixin.Shadow;
import org.ultramine.mods.bukkit.interfaces.world.IMixinWorld;

@Mixin(EntityAIBreakDoor.class)
public class MixinEntityAIBreakDoor extends EntityAIDoorInteract
{
	@Shadow private int field_75358_j;
	@Shadow private int breakingTime;

	public MixinEntityAIBreakDoor(EntityLiving p_i1618_1_)
	{
		super(p_i1618_1_);
	}

	@Overwrite
	public void updateTask()
	{
		super.updateTask();

		if (this.theEntity.getRNG().nextInt(20) == 0)
		{
			this.theEntity.worldObj.playAuxSFX(1010, this.entityPosX, this.entityPosY, this.entityPosZ, 0);
		}

		++this.breakingTime;
		int i = (int)((float)this.breakingTime / 240.0F * 10.0F);

		if (i != this.field_75358_j)
		{
			this.theEntity.worldObj.destroyBlockInWorldPartially(this.theEntity.getEntityId(), this.entityPosX, this.entityPosY, this.entityPosZ, i);
			this.field_75358_j = i;
		}

		if (this.breakingTime == 240 && this.theEntity.worldObj.difficultySetting == EnumDifficulty.HARD)
		{
			// CraftBukkit start
			if (org.bukkit.craftbukkit.event.CraftEventFactory.callEntityBreakDoorEvent(this.theEntity, this.entityPosX, this.entityPosY, this.entityPosZ).isCancelled())
			{
			this.updateTask();
			return;
			}
			// CraftBukkit end
			this.theEntity.worldObj.setBlockToAir(this.entityPosX, this.entityPosY, this.entityPosZ);
			this.theEntity.worldObj.playAuxSFX(1012, this.entityPosX, this.entityPosY, this.entityPosZ, 0);
			this.theEntity.worldObj.playAuxSFX(2001, this.entityPosX, this.entityPosY, this.entityPosZ, Block.getIdFromBlock(this.field_151504_e));
		}
	}
}
