package org.ultramine.mods.bukkit.mixin.block;

import net.minecraft.block.Block;
import net.minecraft.block.BlockCommandBlock;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;
import org.spongepowered.asm.mixin.Shadow;

@Mixin(BlockCommandBlock.class)
public abstract class MixinBlockCommandBlock
{
	@Shadow public abstract int tickRate(World p_149738_1_);

	@Overwrite
	public void onNeighborBlockChange(World world, int x, int y, int z, Block block)
	{
		if (!world.isRemote)
		{
			boolean flag = world.isBlockIndirectlyGettingPowered(x, y, z);
			int l = world.getBlockMetadata(x, y, z);
			boolean flag1 = (l & 1) != 0;
			if (flag && !flag1)
			{
				world.setBlockMetadataWithNotify(x, y, z, l | 1, 4);
				world.scheduleBlockUpdate(x, y, z, (BlockCommandBlock) (Object) this, this.tickRate(world));
			}
			else if (!flag && flag1)
			{
				world.setBlockMetadataWithNotify(x, y, z, l & -2, 4);
			}
		}

	}
}
