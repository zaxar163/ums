package org.ultramine.mods.bukkit.mixin.block;

import net.minecraft.block.Block;
import net.minecraft.block.BlockTrapDoor;
import net.minecraft.block.material.Material;
import net.minecraft.world.World;
import net.minecraftforge.common.util.ForgeDirection;
import org.bukkit.Bukkit;
import org.bukkit.event.block.BlockRedstoneEvent;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;
import org.spongepowered.asm.mixin.Shadow;
import org.ultramine.mods.bukkit.interfaces.world.IMixinWorld;

@Mixin(BlockTrapDoor.class)
public abstract class MixinBlockTrapDoor extends Block
{
	@Shadow private static boolean func_150119_a(Block p_150119_0_) {return false;}
	@Shadow public abstract void func_150120_a(World p_150120_1_, int p_150120_2_, int p_150120_3_, int p_150120_4_, boolean p_150120_5_);

	protected MixinBlockTrapDoor(Material p_i45394_1_)
	{
		super(p_i45394_1_);
	}

	@Overwrite
	public void onNeighborBlockChange(World world, int x, int y, int z, Block block)
	{
		if (!world.isRemote)
		{
			int l = world.getBlockMetadata(x, y, z);
			int i1 = x;
			int j1 = z;
			if ((l & 3) == 0)
			{
				j1 = z + 1;
			}

			if ((l & 3) == 1)
			{
				--j1;
			}

			if ((l & 3) == 2)
			{
				i1 = x + 1;
			}

			if ((l & 3) == 3)
			{
				--i1;
			}

			if (!func_150119_a(world.getBlock(i1, y, j1)) && !world.isSideSolid(i1, y, j1, ForgeDirection.getOrientation((l & 3) + 2)))
			{
				world.setBlockToAir(x, y, z);
				this.dropBlockAsItem(world, x, y, z, l, 0);
			}

			boolean flag = world.isBlockIndirectlyGettingPowered(x, y, z);
			if (flag || block.canProvidePower())
			{
				org.bukkit.World bworld = ((IMixinWorld) world).getWorld();
				org.bukkit.block.Block bblock = bworld.getBlockAt(x, y, z);
				int power = bblock.getBlockPower();
				int oldPower = (world.getBlockMetadata(x, y, z) & 4) > 0 ? 15 : 0;
				if (oldPower == 0 ^ power == 0 || block.canProvidePower())
				{
					BlockRedstoneEvent eventRedstone = new BlockRedstoneEvent(bblock, oldPower, power);
					Bukkit.getPluginManager().callEvent(eventRedstone);
					flag = eventRedstone.getNewCurrent() > 0;
				}
				this.func_150120_a(world, x, y, z, flag);
			}
		}
	}
}
