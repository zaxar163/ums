package org.ultramine.mods.bukkit.mixin.world;

import net.minecraft.block.Block;
import net.minecraft.entity.Entity;
import net.minecraft.entity.effect.EntityLightningBolt;
import net.minecraft.init.Blocks;
import net.minecraft.profiler.Profiler;
import net.minecraft.server.MinecraftServer;
import net.minecraft.world.Teleporter;
import net.minecraft.world.World;
import net.minecraft.world.WorldProvider;
import net.minecraft.world.WorldServer;
import net.minecraft.world.WorldSettings;
import net.minecraft.world.storage.ISaveHandler;
import org.bukkit.Bukkit;
import org.bukkit.block.BlockState;
import org.bukkit.craftbukkit.CraftTravelAgent;
import org.bukkit.entity.LightningStrike;
import org.bukkit.event.block.BlockFormEvent;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.weather.LightningStrikeEvent;
import org.bukkit.event.weather.ThunderChangeEvent;
import org.bukkit.event.weather.WeatherChangeEvent;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.Redirect;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import org.ultramine.mods.bukkit.interfaces.entity.IMixinEntity;
import org.ultramine.mods.bukkit.interfaces.world.IMixinWorld;

@Mixin(net.minecraft.world.WorldServer.class)
public abstract class MixinWorldServer extends World
{
	@Shadow private Teleporter worldTeleporter;

	public MixinWorldServer(ISaveHandler p_i45368_1_, String p_i45368_2_, WorldProvider p_i45368_3_, WorldSettings p_i45368_4_, Profiler p_i45368_5_)
	{super(p_i45368_1_, p_i45368_2_, p_i45368_3_, p_i45368_4_, p_i45368_5_);}

	public boolean spawnEntityInWorld(Entity entity)
	{
		return ((IMixinWorld)this).addEntity(entity, CreatureSpawnEvent.SpawnReason.DEFAULT);
	}

	public boolean real_spawnEntityInWorld(Entity entity)
	{
		return super.spawnEntityInWorld(entity);
	}

	@Inject(method = "<init>", at = @At(value = "RETURN"))
	private void init(MinecraftServer p_i45284_1_, ISaveHandler p_i45284_2_, String p_i45284_3_, int p_i45284_4_, WorldSettings p_i45284_5_, Profiler p_i45284_6_, CallbackInfo ci)
	{
		this.worldTeleporter = new CraftTravelAgent((WorldServer) (Object) this);
	}

	@Redirect(method = "func_147456_g", at = @At(value = "INVOKE", target = "Lnet/minecraft/world/WorldServer;setBlock(IIILnet/minecraft/block/Block;)Z", ordinal = 0))
	public boolean setBlockRedirectIceForm(WorldServer world, int x, int y, int z, Block block)
	{
		BlockState blockState = ((IMixinWorld) world).getWorld().getBlockAt(x, y, z).getState();
		blockState.setTypeId(Block.getIdFromBlock(Blocks.ice));
		BlockFormEvent iceBlockForm = new BlockFormEvent(blockState.getBlock(), blockState);
		((IMixinWorld) world).getServer().getPluginManager().callEvent(iceBlockForm);
		if (!iceBlockForm.isCancelled())
			blockState.update(true);
		return false;
	}

	@Redirect(method = "func_147456_g", at = @At(value = "INVOKE", target = "Lnet/minecraft/world/WorldServer;setBlock(IIILnet/minecraft/block/Block;)Z", ordinal = 1))
	public boolean setBlockRedirectSnowForm(WorldServer world, int x, int y, int z, Block block)
	{
		BlockState blockState = ((IMixinWorld) world).getWorld().getBlockAt(x, y, z).getState();
		blockState.setTypeId(Block.getIdFromBlock(Blocks.snow_layer));
		BlockFormEvent snow = new BlockFormEvent(blockState.getBlock(), blockState);
		((IMixinWorld) world).getServer().getPluginManager().callEvent(snow);
		if (!snow.isCancelled())
			blockState.update(true);
		return false;
	}

	@Inject(method = "addWeatherEffect", cancellable = true, at = @At(value = "HEAD"))
	private void addWeatherEffectInject(Entity entity, CallbackInfoReturnable<Boolean> cir)
	{
		if (entity instanceof EntityLightningBolt)
		{
			LightningStrikeEvent lightning = new LightningStrikeEvent(((IMixinWorld) this).getWorld(), (LightningStrike) ((IMixinEntity) entity).getBukkitEntity());
			Bukkit.getPluginManager().callEvent(lightning);
			if (lightning.isCancelled())
			{
				cir.setReturnValue(false);
				cir.cancel();
			}
		}
	}

	@Redirect(method = "resetRainAndThunder", at = @At(value = "INVOKE", target = "Lnet/minecraft/world/WorldProvider;resetRainAndThunder()V"))
	private void resetRainAndThunderRedirect(WorldProvider worldProvider)
	{
		WeatherChangeEvent weather = new WeatherChangeEvent(((IMixinWorld) this).getWorld(), false);
		Bukkit.getPluginManager().callEvent(weather);
		ThunderChangeEvent thunder = new ThunderChangeEvent(((IMixinWorld) this).getWorld(), false);
		Bukkit.getPluginManager().callEvent(thunder);
		if (!weather.isCancelled())
		{
			this.worldInfo.setRainTime(0);
			this.worldInfo.setRaining(false);
		}
		if (!thunder.isCancelled())
		{
			this.worldInfo.setThunderTime(0);
			this.worldInfo.setThundering(false);
		}
		if (!weather.isCancelled() && !thunder.isCancelled())
			provider.resetRainAndThunder(); // Maybe, WorldProvider#worldObj.worldInfo != this.worldInfo
	}
}
