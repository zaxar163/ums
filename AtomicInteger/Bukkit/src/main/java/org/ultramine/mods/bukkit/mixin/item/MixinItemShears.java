package org.ultramine.mods.bukkit.mixin.item;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemShears;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.IShearable;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerShearEntityEvent;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;
import org.ultramine.mods.bukkit.interfaces.entity.IMixinEntity;

import java.util.ArrayList;
import java.util.Random;

@Mixin(ItemShears.class)
public class MixinItemShears
{
//	@Inject(method = "itemInteractionForEntity", cancellable = true, at = @At(value = "INVOKE", target = "Lnet/minecraftforge/common/IShearable;isShearable(Lnet/minecraft/item/ItemStack;Lnet/minecraft/world/IBlockAccess;III)Z", shift = At.Shift.BY, by = 3))
//	@Inject(method = "itemInteractionForEntity", cancellable = true, at = @At(value = "INVOKE", target = "Lnet/minecraft/entity/player/EntityPlayer;worldObj:Lnet/minecraft/world/World;", shift = At.Shift.BY, by = -3))
	/**
	 *
	 * @author AtomicInteger
	 * @reason This overwrite exists, because bug with @Inject in Mixin Framework exists too.
	 */
//	@Overwrite
	public boolean itemInteractionForEntity(ItemStack itemstack, EntityPlayer player, EntityLivingBase entity)
	{
		if (entity.worldObj.isRemote || !(entity instanceof IShearable))
		{
			return false;
		}
		else
		{
			IShearable target = (IShearable) entity;
			if (target.isShearable(itemstack, entity.worldObj, (int) entity.posX, (int) entity.posY, (int) entity.posZ))
			{
				PlayerShearEntityEvent event = new PlayerShearEntityEvent((Player) ((IMixinEntity) player).getBukkitEntity(), ((IMixinEntity) entity).getBukkitEntity());
				Bukkit.getPluginManager().callEvent(event);
				if (event.isCancelled())
					return false;
				ArrayList<ItemStack> drops = target.onSheared(itemstack, entity.worldObj, (int) entity.posX, (int) entity.posY, (int) entity.posZ, EnchantmentHelper.getEnchantmentLevel(Enchantment.fortune.effectId, itemstack));
				Random rand = new Random();
				EntityItem ent;
				for (ItemStack itemStack : drops)
				{
					ent = entity.entityDropItem(itemStack, 1.0F);
					ent.motionY += rand.nextFloat() * 0.05F;
					ent.motionX += (rand.nextFloat() - rand.nextFloat()) * 0.1F;
					ent.motionZ += ((rand.nextFloat() - rand.nextFloat()) * 0.1F);
				}
				itemstack.damageItem(1, entity);
			}
			return true;
		}
	}
}
