package org.ultramine.mods.bukkit.mixin.entity.projectile;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.projectile.EntityPotion;
import net.minecraft.entity.projectile.EntityThrowable;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;
import org.bukkit.craftbukkit.entity.CraftLivingEntity;
import org.bukkit.craftbukkit.event.CraftEventFactory;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.entity.PotionSplashEvent;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;
import org.spongepowered.asm.mixin.Shadow;
import org.ultramine.mods.bukkit.interfaces.entity.IMixinEntity;
import org.ultramine.mods.bukkit.interfaces.entity.projectile.IMixinEntityPotion;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

@Mixin(net.minecraft.entity.projectile.EntityPotion.class)
public abstract class MixinEntityPotion extends EntityThrowable implements IMixinEntityPotion
{
	@Shadow
	public ItemStack potionDamage;

	public MixinEntityPotion(World p_i1776_1_)
	{
		super(p_i1776_1_);
	}

	@Override
	public ItemStack getPotionDamage()
	{
		return potionDamage;
	}

	@Override
	public void setPotionDamage(ItemStack potionDamage)
	{
		this.potionDamage = potionDamage;
	}

	/**
	 * @author AtomicInteger
	 * @reason Cannot implement by @Inject or @Redirect
	 */
	@Overwrite
	protected void onImpact(MovingObjectPosition pos)
	{
		if (!this.worldObj.isRemote)
		{
			List list = Items.potionitem.getEffects(this.potionDamage);
			if (list != null)
			{
				AxisAlignedBB axisalignedbb = this.boundingBox.expand(4.0D, 2.0D, 4.0D);
				List list1 = this.worldObj.getEntitiesWithinAABB(EntityLivingBase.class, axisalignedbb);
				if (list1 != null)
				{
					Iterator iterator = list1.iterator();
					HashMap<LivingEntity, Double> affected = new HashMap<LivingEntity, Double>();
					while (iterator.hasNext())
					{
						EntityLivingBase entitylivingbase = (EntityLivingBase) iterator.next();
						double d0 = this.getDistanceSqToEntity(entitylivingbase);
						if (d0 < 16.0D)
						{
							double d1 = 1.0D - Math.sqrt(d0) / 4.0D;
							if (entitylivingbase == pos.entityHit)
								d1 = 1.0D;
							affected.put((LivingEntity) ((IMixinEntity) entitylivingbase).getBukkitEntity(), d1);
						}
					}
					PotionSplashEvent event = CraftEventFactory.callPotionSplashEvent((EntityPotion) (Object) this, affected);
					if (!event.isCancelled() && !list.isEmpty())   // do not process effects if there are no effects to process
					{
						for (LivingEntity victim : event.getAffectedEntities())
						{
							if (!(victim instanceof CraftLivingEntity))
								continue;
							EntityLivingBase entitylivingbase = ((CraftLivingEntity) victim).getHandle();
							double d1 = event.getIntensity(victim);
							for (Object aList : list)
							{
								PotionEffect potioneffect = (PotionEffect) aList;
								int i = potioneffect.getPotionID();
								if (Potion.potionTypes[i].isInstant())
								{
									Potion.potionTypes[i].affectEntity(this.getThrower(), entitylivingbase, potioneffect.getAmplifier(), d1);
								}
								else
								{
									int j = (int) (d1 * potioneffect.getDuration() + 0.5D);
									if (j > 20)
										entitylivingbase.addPotionEffect(new PotionEffect(i, j, potioneffect.getAmplifier()));
								}
							}
						}
					}
				}
			}
			this.worldObj.playAuxSFX(2002, (int) Math.round(this.posX), (int) Math.round(this.posY), (int) Math.round(this.posZ), this.getPotionDamage().getItemDamage());
			this.setDead();
		}
	}
}
