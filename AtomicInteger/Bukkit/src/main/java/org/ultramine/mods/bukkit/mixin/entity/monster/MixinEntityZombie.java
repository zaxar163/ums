package org.ultramine.mods.bukkit.mixin.entity.monster;

import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.monster.EntityPigZombie;
import net.minecraft.entity.monster.EntityZombie;
import net.minecraft.world.World;
import org.bukkit.Bukkit;
import org.bukkit.event.entity.EntityCombustEvent;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Redirect;
import org.ultramine.mods.bukkit.interfaces.entity.IMixinEntity;

@Mixin(EntityZombie.class)
public class MixinEntityZombie extends EntityMob
{
	public MixinEntityZombie(World p_i1738_1_)
	{
		super(p_i1738_1_);
	}

	@Redirect(method = "onLivingUpdate", at = @At(value = "INVOKE", target = "Lnet/minecraft/entity/monster/EntityZombie;setFire(I)V"))
	private void onLivingUpdateRedirect(EntityZombie entityZombie, int fireDuration)
	{
		if (entityZombie.isImmuneToFire() || entityZombie instanceof EntityPigZombie)
			return;
		EntityCombustEvent event = new EntityCombustEvent(((IMixinEntity) this).getBukkitEntity(), fireDuration);
		Bukkit.getPluginManager().callEvent(event);
		if (!event.isCancelled())
			this.setFire(event.getDuration());
	}
}
