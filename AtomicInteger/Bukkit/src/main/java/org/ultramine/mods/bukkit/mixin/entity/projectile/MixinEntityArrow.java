package org.ultramine.mods.bukkit.mixin.entity.projectile;

import net.minecraft.entity.projectile.EntityArrow;
import org.bukkit.craftbukkit.event.CraftEventFactory;
import org.objectweb.asm.Opcodes;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.ultramine.mods.bukkit.interfaces.entity.projectile.IMixinEntityArrow;

@Mixin(net.minecraft.entity.projectile.EntityArrow.class)
public class MixinEntityArrow implements IMixinEntityArrow
{
	@Shadow
	public boolean inGround;
	@Shadow
	private int knockbackStrength;

	@Override
	public boolean isInGround()
	{
		return inGround;
	}

	@Override
	public int getKnockbackStrength()
	{
		return knockbackStrength;
	}

	@Inject(method = "onUpdate", at = @At(value = "FIELD", opcode = Opcodes.GETFIELD, target = "Lnet/minecraft/util/MovingObjectPosition;entityHit:Lnet/minecraft/entity/Entity;", ordinal = 3, shift = At.Shift.BY, by = -2))
	private void onUpdateInject(CallbackInfo ci)
	{
		CraftEventFactory.callProjectileHitEvent((EntityArrow) (Object) this);
	}
}
