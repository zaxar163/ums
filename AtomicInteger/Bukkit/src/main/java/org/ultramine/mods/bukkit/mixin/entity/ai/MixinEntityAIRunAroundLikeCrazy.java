package org.ultramine.mods.bukkit.mixin.entity.ai;

import net.minecraft.entity.ai.EntityAIRunAroundLikeCrazy;
import net.minecraft.entity.passive.EntityHorse;
import net.minecraft.entity.player.EntityPlayer;
import org.bukkit.craftbukkit.event.CraftEventFactory;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;
import org.spongepowered.asm.mixin.Shadow;

@Mixin(EntityAIRunAroundLikeCrazy.class)
public class MixinEntityAIRunAroundLikeCrazy
{
	@Shadow private EntityHorse horseHost;

	@Overwrite
	public void updateTask()
	{
		if (this.horseHost.getRNG().nextInt(50) == 0)
		{
			if (this.horseHost.riddenByEntity instanceof EntityPlayer)
			{
				int i = this.horseHost.getTemper();
				int j = this.horseHost.getMaxTemper();
				if (j > 0 && this.horseHost.getRNG().nextInt(j) < i && !CraftEventFactory.callEntityTameEvent(this.horseHost, (EntityPlayer) this.horseHost.riddenByEntity).isCancelled() && this.horseHost.riddenByEntity instanceof EntityPlayer)
				{
					this.horseHost.setTamedBy((EntityPlayer) this.horseHost.riddenByEntity);
					this.horseHost.worldObj.setEntityState(this.horseHost, (byte) 7);
					return;
				}
				this.horseHost.increaseTemper(5);
			}
			this.horseHost.riddenByEntity.mountEntity(null);
			this.horseHost.riddenByEntity = null;
			this.horseHost.makeHorseRearWithSound();
			this.horseHost.worldObj.setEntityState(this.horseHost, (byte) 6);
		}
	}
}
