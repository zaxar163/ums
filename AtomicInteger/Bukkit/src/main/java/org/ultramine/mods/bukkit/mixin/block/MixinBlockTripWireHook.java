package org.ultramine.mods.bukkit.mixin.block;

import net.minecraft.block.BlockTripWireHook;
import net.minecraft.world.World;
import org.bukkit.Bukkit;
import org.bukkit.block.Block;
import org.bukkit.event.block.BlockRedstoneEvent;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.ultramine.mods.bukkit.interfaces.world.IMixinWorld;

@Mixin(BlockTripWireHook.class)
public class MixinBlockTripWireHook
{
	@Inject(method = "func_150136_a", at = @At(value = "INVOKE", target = "Lnet/minecraft/block/BlockTripWireHook;func_150135_a(Lnet/minecraft/world/World;IIIZZZZ)V"))
	private void func_150136_aInject(World world, int x, int y, int z, boolean p_150136_5_, int p_150136_6_, boolean p_150136_7_, int p_150136_8_, int p_150136_9_, CallbackInfo ci)
	{
		Block block = ((IMixinWorld) world).getWorld().getBlockAt(x, y, z);
		BlockRedstoneEvent eventRedstone = new BlockRedstoneEvent(block, 15, 0);
		Bukkit.getPluginManager().callEvent(eventRedstone);
		if (eventRedstone.getNewCurrent() > 0)
			ci.cancel();
	}
}
