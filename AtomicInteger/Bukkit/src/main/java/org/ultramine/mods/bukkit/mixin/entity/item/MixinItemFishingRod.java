package org.ultramine.mods.bukkit.mixin.entity.item;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.projectile.EntityFishHook;
import net.minecraft.item.ItemFishingRod;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import org.bukkit.Bukkit;
import org.bukkit.entity.Fish;
import org.bukkit.event.player.PlayerFishEvent;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import org.ultramine.mods.bukkit.interfaces.entity.IMixinEntity;

@Mixin(ItemFishingRod.class)
public class MixinItemFishingRod
{
	@Inject(method = "onItemRightClick", cancellable = true, at = @At(value = "INVOKE", target = "Lnet/minecraft/world/World;playSoundAtEntity(Lnet/minecraft/entity/Entity;Ljava/lang/String;FF)V", shift = At.Shift.BEFORE))
	private void onItemRightClickInject(ItemStack itemStack, World world, EntityPlayer player, CallbackInfoReturnable<ItemStack> cir)
	{
		EntityFishHook hook = new EntityFishHook(world, player);
		PlayerFishEvent playerFishEvent = new PlayerFishEvent((org.bukkit.entity.Player) ((IMixinEntity) player).getBukkitEntity(), null, (Fish) ((IMixinEntity) hook).getBukkitEntity(), PlayerFishEvent.State.FISHING);
		Bukkit.getPluginManager().callEvent(playerFishEvent);
		if (playerFishEvent.isCancelled())
		{
			cir.setReturnValue(itemStack);
			cir.cancel();
		}
	}
}
