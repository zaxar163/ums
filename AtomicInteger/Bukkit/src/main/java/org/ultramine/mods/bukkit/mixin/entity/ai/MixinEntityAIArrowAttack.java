package org.ultramine.mods.bukkit.mixin.entity.ai;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.IRangedAttackMob;
import net.minecraft.entity.ai.EntityAIArrowAttack;
import org.bukkit.craftbukkit.event.CraftEventFactory;
import org.bukkit.event.entity.EntityTargetEvent;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(EntityAIArrowAttack.class)
public class MixinEntityAIArrowAttack
{
	@Final
	@Shadow private IRangedAttackMob rangedAttackEntityHost;
	@Shadow private EntityLivingBase attackTarget;

	@Inject(method = "resetTask", at = @At(value = "HEAD"))
	private void resetTaskInject(CallbackInfo ci)
	{
		EntityTargetEvent.TargetReason reason = this.attackTarget.isEntityAlive() ? EntityTargetEvent.TargetReason.FORGOT_TARGET : EntityTargetEvent.TargetReason.TARGET_DIED;
		CraftEventFactory.callEntityTargetEvent((Entity) rangedAttackEntityHost, null, reason);
	}
}
