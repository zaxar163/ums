package org.ultramine.mods.bukkit.mixin.block;

import net.minecraft.block.BlockRedstoneWire;
import net.minecraft.world.ChunkPosition;
import net.minecraft.world.World;
import org.bukkit.Bukkit;
import org.bukkit.event.block.BlockRedstoneEvent;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;
import org.spongepowered.asm.mixin.Shadow;
import org.ultramine.mods.bukkit.interfaces.world.IMixinWorld;

import java.util.Set;

@Mixin(BlockRedstoneWire.class)
public abstract class MixinBlockRedstoneWire
{
	@Shadow private boolean field_150181_a;
	@Shadow private Set<ChunkPosition> field_150179_b;
	@Shadow protected abstract int func_150178_a(World p_150178_1_, int p_150178_2_, int p_150178_3_, int p_150178_4_, int p_150178_5_);

	@Overwrite
	private void func_150175_a(World world, int x, int y, int z, int p_150175_5_, int p_150175_6_, int p_150175_7_)
	{
		int k1 = world.getBlockMetadata(x, y, z);
		byte b0 = 0;
		int i3 = this.func_150178_a(world, p_150175_5_, p_150175_6_, p_150175_7_, b0);
		this.field_150181_a = false;
		int l1 = world.getStrongestIndirectPower(x, y, z);
		this.field_150181_a = true;
		if (l1 > 0 && l1 > i3 - 1)
		{
			i3 = l1;
		}

		int i2 = 0;

		for (int j2 = 0; j2 < 4; ++j2)
		{
			int k2 = x;
			int l2 = z;
			if (j2 == 0)
			{
				k2 = x - 1;
			}

			if (j2 == 1)
			{
				++k2;
			}

			if (j2 == 2)
			{
				l2 = z - 1;
			}

			if (j2 == 3)
			{
				++l2;
			}

			if (k2 != p_150175_5_ || l2 != p_150175_7_)
			{
				i2 = this.func_150178_a(world, k2, y, l2, i2);
			}

			if (world.getBlock(k2, y, l2).isNormalCube() && !world.getBlock(x, y + 1, z).isNormalCube())
			{
				if ((k2 != p_150175_5_ || l2 != p_150175_7_) && y >= p_150175_6_)
				{
					i2 = this.func_150178_a(world, k2, y + 1, l2, i2);
				}
			} else if (!world.getBlock(k2, y, l2).isNormalCube() && (k2 != p_150175_5_ || l2 != p_150175_7_) && y <= p_150175_6_)
			{
				i2 = this.func_150178_a(world, k2, y - 1, l2, i2);
			}
		}

		if (i2 > i3)
		{
			i3 = i2 - 1;
		} else if (i3 > 0)
		{
			--i3;
		} else
		{
			i3 = 0;
		}

		if (l1 > i3 - 1)
		{
			i3 = l1;
		}

		if (k1 != i3)
		{
			BlockRedstoneEvent event = new BlockRedstoneEvent(((IMixinWorld) world).getWorld().getBlockAt(x, y, z), k1, i3);
			Bukkit.getPluginManager().callEvent(event);
			i3 = event.getNewCurrent();
		}
		// CraftBukkit end
		if (k1 != i3)
		{
			world.setBlockMetadataWithNotify(x, y, z, i3, 2);
			this.field_150179_b.add(new ChunkPosition(x, y, z));
			this.field_150179_b.add(new ChunkPosition(x - 1, y, z));
			this.field_150179_b.add(new ChunkPosition(x + 1, y, z));
			this.field_150179_b.add(new ChunkPosition(x, y - 1, z));
			this.field_150179_b.add(new ChunkPosition(x, y + 1, z));
			this.field_150179_b.add(new ChunkPosition(x, y, z - 1));
			this.field_150179_b.add(new ChunkPosition(x, y, z + 1));
		}

	}
}
