package org.ultramine.mods.bukkit.mixin.block;

import net.minecraft.block.BlockRedstoneTorch;
import net.minecraft.init.Blocks;
import net.minecraft.world.World;
import org.bukkit.Bukkit;
import org.bukkit.block.Block;
import org.bukkit.event.block.BlockRedstoneEvent;
import org.bukkit.plugin.PluginManager;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;
import org.spongepowered.asm.mixin.Shadow;
import org.ultramine.mods.bukkit.interfaces.world.IMixinWorld;

import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.WeakHashMap;

@Mixin(BlockRedstoneTorch.class)
public abstract class MixinBlockRedstoneTorch
{
	@Shadow private boolean field_150113_a;
	@Shadow private static Map field_150112_b = new WeakHashMap();
	@Shadow protected abstract boolean func_150110_m(World p_150110_1_, int p_150110_2_, int p_150110_3_, int p_150110_4_);
	@Shadow protected abstract boolean func_150111_a(World p_150111_1_, int p_150111_2_, int p_150111_3_, int p_150111_4_, boolean p_150111_5_);

	@Overwrite
	public void updateTick(World world, int x, int y, int z, Random random)
	{
		boolean flag = this.func_150110_m(world, x, y, z);
		List list = (List) field_150112_b.get(world);

		while (list != null && !list.isEmpty() && world.getTotalWorldTime() - ((BlockRedstoneTorch.Toggle) list.get(0)).field_150844_d > 60L)
		{
			list.remove(0);
		}

		PluginManager manager = Bukkit.getPluginManager();
		Block block = ((IMixinWorld) world).getWorld().getBlockAt(x, y, z);
		int oldCurrent = this.field_150113_a ? 15 : 0;
		BlockRedstoneEvent event = new BlockRedstoneEvent(block, oldCurrent, oldCurrent);
		if (this.field_150113_a)
		{
			if (flag)
			{
				if (oldCurrent != 0)
				{
					event.setNewCurrent(0);
					manager.callEvent(event);
					if (event.getNewCurrent() != 0)
					{
						return;
					}
				}
				world.setBlock(x, y, z, Blocks.unlit_redstone_torch, world.getBlockMetadata(x, y, z), 3);
				if (this.func_150111_a(world, x, y, z, true))
				{
					world.playSoundEffect(x + 0.5F, y + 0.5F, z + 0.5F, "random.fizz", 0.5F, 2.6F + (world.rand.nextFloat() - world.rand.nextFloat()) * 0.8F);

					for (int l = 0; l < 5; ++l)
					{
						double d0 = x + random.nextDouble() * 0.6D + 0.2D;
						double d1 = y + random.nextDouble() * 0.6D + 0.2D;
						double d2 = z + random.nextDouble() * 0.6D + 0.2D;
						world.spawnParticle("smoke", d0, d1, d2, 0.0D, 0.0D, 0.0D);
					}
				}
			}
		}
		else if (!flag && !this.func_150111_a(world, x, y, z, false))
		{
			if (oldCurrent != 15)
			{
				event.setNewCurrent(15);
				manager.callEvent(event);
				if (event.getNewCurrent() != 15)
				{
					return;
				}
			}
			world.setBlock(x, y, z, Blocks.redstone_torch, world.getBlockMetadata(x, y, z), 3);
		}
	}
}
