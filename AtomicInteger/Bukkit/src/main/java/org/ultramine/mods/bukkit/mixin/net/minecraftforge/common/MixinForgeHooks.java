package org.ultramine.mods.bukkit.mixin.net.minecraftforge.common;

import net.minecraft.block.Block;
import net.minecraft.block.BlockSapling;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBucket;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.stats.StatList;
import net.minecraft.world.World;
import net.minecraftforge.common.ForgeHooks;
import net.minecraftforge.common.util.BlockSnapshot;
import net.minecraftforge.event.ForgeEventFactory;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.TreeType;
import org.bukkit.block.BlockState;
import org.bukkit.craftbukkit.block.CraftBlockState;
import org.bukkit.entity.Player;
import org.bukkit.event.world.StructureGrowEvent;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;
import org.ultramine.mods.bukkit.interfaces.entity.IMixinEntity;
import org.ultramine.mods.bukkit.interfaces.world.IMixinWorld;
import org.ultramine.mods.bukkit.util.UMTreeTypeToBukkit;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Mixin(ForgeHooks.class)
public class MixinForgeHooks
{

	@Overwrite
	public static boolean onPlaceItemIntoWorld(ItemStack itemstack, EntityPlayer player, World world, int x, int y, int z, int side, float hitX, float hitY, float hitZ)
	{
		int meta = itemstack.getItemDamage();
		int size = itemstack.stackSize;
		NBTTagCompound nbt = null;
		if (itemstack.getTagCompound() != null)
		{
			nbt = (NBTTagCompound) itemstack.getTagCompound().copy();
		}

		if (!(itemstack.getItem() instanceof ItemBucket))
		{
			world.captureBlockSnapshots = true;
			if (itemstack.getItem() instanceof net.minecraft.item.ItemDye && itemstack.getItemDamage() == 15)
			{
				Block block = world.getBlock(x, y, z);
				if (block != null && (block instanceof net.minecraft.block.BlockSapling || block instanceof net.minecraft.block.BlockMushroom))
					((IMixinWorld) world).setCaptureTreeGeneration(true);
			}
			// Cauldron end

//			if(!((IMixinWorld) world).isCaptureTreeGeneration())
//				world.captureItemDrops = true; // Thermos real og right here, prevent sugar cane dupe glitch amongst other issues
		}

//		ItemStack.currentPlayer = player; // Cauldron
		boolean flag = itemstack.getItem().onItemUse(itemstack, player, world, x, y, z, side, hitX, hitY, hitZ);
//		ItemStack.currentPlayer = null; // Cauldron
		world.captureBlockSnapshots = false;
//		if(!((IMixinWorld) world).isCaptureTreeGeneration()) // Thermos stop capturing item drops to the world
//			world.captureItemDrops = false;
		// Cauldron start
		if (flag && ((IMixinWorld) world).isCaptureTreeGeneration() && world.capturedBlockSnapshots.size() > 0)
		{
			((IMixinWorld) world).setCaptureTreeGeneration(false);
			Location loc = new Location(((IMixinWorld) world).getWorld(), x, y, z);
			TreeType type = UMTreeTypeToBukkit.getBukkitTreeType(BlockSapling.treeType);
			net.minecraft.block.BlockSapling.treeType = null;
			List<BlockState> states = new ArrayList<>();
			for (net.minecraftforge.common.util.BlockSnapshot snapshot : (List<net.minecraftforge.common.util.BlockSnapshot>) world.capturedBlockSnapshots.clone())
			{
				states.add(new CraftBlockState(snapshot));
			}

			world.capturedBlockSnapshots.clear();
			StructureGrowEvent event = null;
			if (type != null)
			{
				event = new StructureGrowEvent(loc, type, false, (Player) ((IMixinEntity) player).getBukkitEntity(), states);
				Bukkit.getPluginManager().callEvent(event);
			}


			Iterator<BlockSnapshot> var19;
			BlockSnapshot blocksnapshot;
			if (event == null || !event.isCancelled())
			{
				for (BlockState state : states)
				{
					state.update(true);
				}
			}
			return flag;
		}
		else
		{
			((IMixinWorld) world).setCaptureTreeGeneration(false); // Cauldron end
			if (flag)
			{
				// save new item data
				int newMeta = itemstack.getItemDamage();
				int newSize = itemstack.stackSize;
				NBTTagCompound newNBT = null;
				if (itemstack.getTagCompound() != null)
				{
					newNBT = (NBTTagCompound) itemstack.getTagCompound().copy();
				}
				net.minecraftforge.event.world.BlockEvent.PlaceEvent placeEvent = null;
				List<net.minecraftforge.common.util.BlockSnapshot> blockSnapshots = (List<net.minecraftforge.common.util.BlockSnapshot>) world.capturedBlockSnapshots.clone();
				world.capturedBlockSnapshots.clear();
				// make sure to set pre-placement item data for event
				itemstack.setItemDamage(meta);
				itemstack.stackSize = size;
				if (nbt != null)
				{
					itemstack.setTagCompound(nbt);
				}
				if (blockSnapshots.size() > 1)
				{
					placeEvent = ForgeEventFactory.onPlayerMultiBlockPlace(player, blockSnapshots,
							net.minecraftforge.common.util.ForgeDirection.getOrientation(side));
				}
				else if (blockSnapshots.size() == 1)
				{
					// Call both Bukkit's BlockPlaceEvent and Forge's
					placeEvent = ForgeEventFactory.onPlayerBlockPlace(player, blockSnapshots.get(0),
							net.minecraftforge.common.util.ForgeDirection.getOrientation(side));
				}

				if (placeEvent != null && (placeEvent.isCanceled()))
				{
					flag = false; // cancel placement
					// revert back all captured blocks
					for (net.minecraftforge.common.util.BlockSnapshot blocksnapshot : blockSnapshots)
					{
						world.restoringBlockSnapshots = true;
						blocksnapshot.restore(true, false);
						world.restoringBlockSnapshots = false;
					}
//					world.itemStackSpawnQueue.clear(); // Thermos clear the potential drops because the event was cancelled
				}
				else
				{
					// Change the stack to its new content
					itemstack.setItemDamage(newMeta);
					itemstack.stackSize = newSize;
					if (nbt != null)
					{
						itemstack.setTagCompound(newNBT);
					}
					for (net.minecraftforge.common.util.BlockSnapshot blocksnapshot : blockSnapshots)
					{
						int blockX = blocksnapshot.x;
						int blockY = blocksnapshot.y;
						int blockZ = blocksnapshot.z;
						int metadata = world.getBlockMetadata(blockX, blockY, blockZ);
						int updateFlag = blocksnapshot.flag;
						Block oldBlock = blocksnapshot.replacedBlock;
						Block newBlock = world.getBlock(blockX, blockY, blockZ);
						if (newBlock != null && !(newBlock.hasTileEntity(metadata))) // Containers
						// get
						// placed
						// automatically
						{
							newBlock.onBlockAdded(world, blockX, blockY, blockZ);
						}
						world.markAndNotifyBlock(blockX, blockY, blockZ, null, oldBlock, newBlock, updateFlag);
					}
//					for (EntityItem eitm : world.itemStackSpawnQueue) // Thermos load em into the world, mission is a go!
//					{
//						world.addEntity(eitm, SpawnReason.DEFAULT); // Same as world.spawnEntity()
//					}
//					world.itemStackSpawnQueue.clear();
					player.addStat(StatList.objectUseStats[Item.getIdFromItem(itemstack.getItem())], 1);
				}
			}
		}

		world.capturedBlockSnapshots.clear();
		return flag;
	}
}
