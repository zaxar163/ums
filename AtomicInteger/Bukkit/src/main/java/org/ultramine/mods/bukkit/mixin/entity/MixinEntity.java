package org.ultramine.mods.bukkit.mixin.entity;

import jline.internal.Nullable;
import net.minecraft.block.material.Material;
import net.minecraft.entity.DataWatcher;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityList;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.effect.EntityLightningBolt;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.DamageSource;
import net.minecraft.util.MathHelper;
import net.minecraft.world.Teleporter;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.TravelAgent;
import org.bukkit.craftbukkit.CraftTravelAgent;
import org.bukkit.craftbukkit.CraftWorld;
import org.bukkit.craftbukkit.entity.CraftEntity;
import org.bukkit.craftbukkit.event.CraftEventFactory;
import org.bukkit.entity.Hanging;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Painting;
import org.bukkit.entity.Vehicle;
import org.bukkit.event.entity.EntityCombustByEntityEvent;
import org.bukkit.event.entity.EntityCombustEvent;
import org.bukkit.event.entity.EntityPortalEvent;
import org.bukkit.event.hanging.HangingBreakByEntityEvent;
import org.bukkit.event.painting.PaintingBreakByEntityEvent;
import org.bukkit.event.vehicle.VehicleEnterEvent;
import org.bukkit.event.vehicle.VehicleExitEvent;
import org.bukkit.plugin.PluginManager;
import org.bukkit.projectiles.ProjectileSource;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.Redirect;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.ultramine.mods.bukkit.interfaces.entity.IMixinEntity;
import org.ultramine.mods.bukkit.interfaces.management.IMixinServerConfigurationManager;
import org.ultramine.mods.bukkit.interfaces.world.IMixinWorld;

@Mixin(net.minecraft.entity.Entity.class)
public abstract class MixinEntity implements IMixinEntity
{
	@Shadow private int fire;
	@Shadow public World worldObj;
	@Shadow public float rotationYaw;
	@Shadow public float rotationPitch;
	@Shadow public Entity ridingEntity;
	@Shadow private double entityRiderPitchDelta;
	@Shadow private double entityRiderYawDelta;
	@Shadow protected boolean isImmuneToFire;
	@Shadow protected DataWatcher dataWatcher;
	@Shadow public int hurtResistantTime;
	@Shadow public double posX;
	@Shadow public double posY;
	@Shadow public double posZ;
	@Final
	@Shadow public AxisAlignedBB boundingBox;
	@Shadow public boolean isDead;
	@Shadow public int dimension;
	@Shadow public boolean addedToChunk;
	@Shadow public int chunkCoordX;
	@Shadow public int chunkCoordZ;

	@Shadow public abstract void setLocationAndAngles(double x, double y, double z, float yaw, float pitch);
	@Shadow public abstract void setFire(int p_70015_1_);
	@Shadow public abstract boolean attackEntityFrom(DamageSource p_70097_1_, float p_70097_2_);
	@Shadow public abstract int getAir();
	@Shadow public abstract void setAir(int p_70050_1_);
	@Shadow public abstract boolean isEntityInvulnerable();
	@Shadow protected abstract void setBeenAttacked();
	@Shadow public abstract void playSound(String p_85030_1_, float p_85030_2_, float p_85030_3_);
	@Shadow public abstract ItemStack[] getLastActiveItems();
	@Shadow public abstract void setDead();

	protected CraftEntity bukkitEntity;
	public ProjectileSource projectileSource;
	public String spawnReason;

	@Override
	public int getFireTicks()
	{
		return fire;
	}

	@Override
	public void setFireTicks(int ticks)
	{
		this.fire = ticks;
	}

	@Override
	public CraftEntity getBukkitEntity()
	{
		if(bukkitEntity == null)
			bukkitEntity = CraftEntity.getEntity(((IMixinWorld) worldObj).getServer(), (Entity) (Object) this);
		return bukkitEntity;
	}

	@Override
	public void setBukkitEntity(CraftEntity craftEntity) {
		this.bukkitEntity = craftEntity;
	}

	@Override
	public ProjectileSource getProjectileSource()
	{
		return projectileSource;
	}

	@Override
	public String getSpawnReason()
	{
		return spawnReason;
	}

	@Override
	public void setSpawnReason(String spawnReason)
	{
		this.spawnReason = spawnReason;
	}

	@Override
	public void setProjectileSource(ProjectileSource projectileSource)
	{
		this.projectileSource = projectileSource;
	}

	@Overwrite
	public void mountEntity(Entity entity)
	{
		setPassengerOf(entity);
	}

	@Override
	public void setPassengerOf(Entity entity)
	{
		// mountEntity(null) doesn't really fly for overloaded methods,
		// so this method is needed
		Entity originalVehicle = this.ridingEntity;
		Entity originalPassenger = this.ridingEntity == null ? null : this.ridingEntity.riddenByEntity;
		PluginManager pluginManager = Bukkit.getPluginManager();
		this.getBukkitEntity(); // make sure bukkitEntity is initialised
		// CraftBukkit end
		this.entityRiderPitchDelta = 0.0D;
		this.entityRiderYawDelta = 0.0D;

		if(entity == null)
		{
			if(this.ridingEntity != null)
			{
				// CraftBukkit start
				if((this.bukkitEntity instanceof LivingEntity) && (((IMixinEntity) ridingEntity).getBukkitEntity() instanceof Vehicle))
				{
					VehicleExitEvent event = new VehicleExitEvent((Vehicle) ((IMixinEntity) ridingEntity).getBukkitEntity(), (LivingEntity) this.bukkitEntity);
					pluginManager.callEvent(event);

					if(event.isCancelled() || this.ridingEntity != originalVehicle)
					{
						return;
					}
				}

				// CraftBukkit end
				pluginManager.callEvent(new org.spigotmc.event.entity.EntityDismountEvent(this.getBukkitEntity(), ((IMixinEntity) ridingEntity).getBukkitEntity()));     // Spigot
				this.setLocationAndAngles(this.ridingEntity.posX, this.ridingEntity.boundingBox.minY + (double) this.ridingEntity.height, this.ridingEntity.posZ, this.rotationYaw, this.rotationPitch);
				this.ridingEntity.riddenByEntity = null;
			}

			this.ridingEntity = null;
		}
		else
		{
			// CraftBukkit start
			if((this.bukkitEntity instanceof LivingEntity) && (((IMixinEntity) entity).getBukkitEntity() instanceof Vehicle) && entity.worldObj.chunkExists((int) entity.posX >> 4, (int) entity.posZ >> 4))
			{
				// It's possible to move from one vehicle to another.  We need to check if they're already in a vehicle, and fire an exit event if they are.
				VehicleExitEvent exitEvent = null;

				if(this.ridingEntity != null && ((IMixinEntity) ridingEntity).getBukkitEntity() instanceof Vehicle)
				{
					exitEvent = new VehicleExitEvent((Vehicle) ((IMixinEntity) ridingEntity).getBukkitEntity(), (LivingEntity) this.bukkitEntity);
					pluginManager.callEvent(exitEvent);

					if(exitEvent.isCancelled() || this.ridingEntity != originalVehicle || (this.ridingEntity != null && this.ridingEntity.riddenByEntity != originalPassenger))
					{
						return;
					}
				}

				VehicleEnterEvent event = new VehicleEnterEvent((Vehicle) ((IMixinEntity) entity).getBukkitEntity(), this.bukkitEntity);
				pluginManager.callEvent(event);

				// If a plugin messes with the vehicle or the vehicle's passenger
				if(event.isCancelled() || this.ridingEntity != originalVehicle || (this.ridingEntity != null && this.ridingEntity.riddenByEntity != originalPassenger))
				{
					// If we only cancelled the enterevent then we need to put the player in a decent position.
					if(exitEvent != null && this.ridingEntity == originalVehicle && this.ridingEntity != null && this.ridingEntity.riddenByEntity == originalPassenger)
					{
						this.setLocationAndAngles(this.ridingEntity.posX, this.ridingEntity.boundingBox.minY + (double) this.ridingEntity.height, this.ridingEntity.posZ, this.rotationYaw, this.rotationPitch);
						this.ridingEntity.riddenByEntity = null;
						this.ridingEntity = null;
					}

					return;
				}
			}

			// CraftBukkit end
			// Spigot Start
			if(entity.worldObj.chunkExists((int) entity.posX >> 4, (int) entity.posZ >> 4))
			{
				org.spigotmc.event.entity.EntityMountEvent event = new org.spigotmc.event.entity.EntityMountEvent(this.getBukkitEntity(), ((IMixinEntity) entity).getBukkitEntity());
				pluginManager.callEvent(event);

				if(event.isCancelled())
				{
					return;
				}
			}

			// Spigot End

			if(this.ridingEntity != null)
			{
				this.ridingEntity.riddenByEntity = null;
			}

			this.ridingEntity = entity;
			entity.riddenByEntity = (Entity) (Object) this;
		}
	}

	public void onStruckByLightning(EntityLightningBolt entity)
	{
		// CraftBukkit start
		final org.bukkit.entity.Entity thisBukkitEntity = this.getBukkitEntity();
		if(thisBukkitEntity == null) return; // Cauldron - skip mod entities with no wrapper (TODO: create a wrapper)
		if(entity == null) return; // Cauldron - skip null entities, see #392
		final org.bukkit.entity.Entity stormBukkitEntity = ((IMixinEntity) entity).getBukkitEntity();
		if(stormBukkitEntity == null) return; // Cauldron - skip mod entities with no wrapper (TODO: create a wrapper)
		final PluginManager pluginManager = Bukkit.getPluginManager();

		if(thisBukkitEntity instanceof Hanging)
		{
			HangingBreakByEntityEvent hangingEvent = new HangingBreakByEntityEvent((Hanging) thisBukkitEntity, stormBukkitEntity);
			PaintingBreakByEntityEvent paintingEvent = null;

			if(thisBukkitEntity instanceof Painting)
			{
				paintingEvent = new PaintingBreakByEntityEvent((Painting) thisBukkitEntity, stormBukkitEntity);
			}

			pluginManager.callEvent(hangingEvent);

			if(paintingEvent != null)
			{
				paintingEvent.setCancelled(hangingEvent.isCancelled());
				pluginManager.callEvent(paintingEvent);
			}

			if(hangingEvent.isCancelled() || (paintingEvent != null && paintingEvent.isCancelled()))
			{
				return;
			}
		}

		if(this.isImmuneToFire)
		{
			return;
		}
		CraftEventFactory.entityDamage = entity;
		if(!this.attackEntityFrom(DamageSource.inFire, 5.0F))
		{
			CraftEventFactory.entityDamage = null;
			return;
		}

		// CraftBukkit end
		++this.fire;

		if(this.fire == 0)
		{
			// CraftBukkit start - Call a combust event when lightning strikes
			EntityCombustByEntityEvent entityCombustEvent = new EntityCombustByEntityEvent(stormBukkitEntity, thisBukkitEntity, 8);
			pluginManager.callEvent(entityCombustEvent);

			if(!entityCombustEvent.isCancelled())
			{
				this.setFire(entityCombustEvent.getDuration());
			}

			// CraftBukkit end
		}
	}

	@Redirect(method = "moveEntity", at = @At(value = "INVOKE", target = "Lnet/minecraft/entity/Entity;setFire(I)V"))
	private void moveEntityRedirect(Entity entity, int fireDuration)
	{
		EntityCombustEvent event = new EntityCombustEvent(this.getBukkitEntity(), fireDuration);
		Bukkit.getPluginManager().callEvent(event);
		if (!event.isCancelled())
			this.setFire(event.getDuration());
	}

	@Inject(method = "setOnFireFromLava", cancellable = true, at = @At(value = "INVOKE", target = "Lnet/minecraft/entity/Entity;setFire(I)V"))
	private void setOnFireFromLavaInject(CallbackInfo ci)
	{
		if (((Entity) (Object) this) instanceof EntityLivingBase)
		{
			// TODO: Fix event spam, when player is in creative mode.Also, try to make event fire, when player is in survival mode.
			if (this.fire == 0)
			{
				org.bukkit.entity.Entity damagee = this.getBukkitEntity();
				org.bukkit.block.Block combusterBlock = getMaterialInBB(this.boundingBox, Material.lava);
				EntityCombustEvent combustEvent = new org.bukkit.event.entity.EntityCombustByBlockEvent(combusterBlock, damagee, 15);
				Bukkit.getPluginManager().callEvent(combustEvent);
				if (!combustEvent.isCancelled())
					this.setFire(combustEvent.getDuration());
			}
			else
			{
				this.setFire(15);
			}
			ci.cancel();
		}
	}

	@Nullable
	private org.bukkit.block.Block getMaterialInBB(AxisAlignedBB p_72875_1_, net.minecraft.block.material.Material material) {
		int minX = MathHelper.floor_double(p_72875_1_.minX);
		int maxX = MathHelper.floor_double(p_72875_1_.maxX + 1.0D);
		int minY = MathHelper.floor_double(p_72875_1_.minY);
		int maxY = MathHelper.floor_double(p_72875_1_.maxY + 1.0D);
		int minZ = MathHelper.floor_double(p_72875_1_.minZ);
		int maxZ = MathHelper.floor_double(p_72875_1_.maxZ + 1.0D);
		for(int x = minX; x < maxX; ++x)
			for(int y = minY; y < maxY; ++y)
				for(int z = minZ; z < maxZ; ++z)
					if(this.worldObj.getBlock(x, y, z).getMaterial() == material)
						return ((IMixinWorld) this.worldObj).getWorld().getBlockAt(x, y, z);
		return null;
	}

	@Overwrite
	public void travelToDimension(int p_71027_1_)
	{
		if (!this.worldObj.isRemote && !this.isDead)
		{
			this.worldObj.theProfiler.startSection("changeDimension");
			MinecraftServer minecraftserver = MinecraftServer.getServer();
			// CraftBukkit start - Move logic into new function "teleportToLocation"
			// int j = this.dimension;
			// Cauldron start - Allow Forge hotloading on teleport
			WorldServer exitWorld = minecraftserver.worldServerForDimension(p_71027_1_);
			Location enter = this.getBukkitEntity().getLocation();
			Location exit = exitWorld != null ? ((IMixinServerConfigurationManager) minecraftserver.getConfigurationManager()).calculateTarget(enter, minecraftserver.worldServerForDimension(p_71027_1_)) : null;
			boolean useTravelAgent = exitWorld != null && !(this.dimension == 1 && exitWorld.provider.dimensionId == 1); // don't use agent for custom worlds or return from THE_END
			Teleporter teleporter = exit != null ? ((CraftWorld) exit.getWorld()).getHandle().getDefaultTeleporter() : null;
			TravelAgent agent = (teleporter != null && teleporter instanceof TravelAgent) ? (TravelAgent) teleporter : new CraftTravelAgent(((CraftWorld) exit.getWorld()).getHandle()).DEFAULT;  // return arbitrary TA to compensate for implementation dependent plugins
			EntityPortalEvent event = new EntityPortalEvent(this.getBukkitEntity(), enter, exit, agent);
			event.useTravelAgent(useTravelAgent);
			Bukkit.getPluginManager().callEvent(event);
			if (event.isCancelled() || event.getTo() == null || this.isDead)
				return;
			exit = event.useTravelAgent() ? event.getPortalTravelAgent().findOrCreate(event.getTo()) : event.getTo();
			if (exit != null)
				this.teleportTo(exit, true);
		}
	}

	@Override
	public void teleportTo(Location exit, boolean portal)
	{
		WorldServer worldserver = ((CraftWorld) this.getBukkitEntity().getLocation().getWorld()).getHandle();
		WorldServer worldserver1 = ((CraftWorld) exit.getWorld()).getHandle();
		this.dimension = worldserver1.provider.dimensionId;
		this.worldObj.removeEntity((Entity) (Object) this);
		// Thermos silently remove the entity so it can be magically transported to another world :D
		if (!((Entity) (Object) this instanceof EntityLivingBase) && this.addedToChunk && this.worldObj.chunkExists(this.chunkCoordX, this.chunkCoordZ))
		{
			this.worldObj.getChunkFromChunkCoords(this.chunkCoordX, this.chunkCoordZ).removeEntity((Entity) (Object) this);
			this.worldObj.loadedEntityList.remove(this);
		}
		this.isDead = false;
		this.worldObj.theProfiler.startSection("reposition");
		// CraftBukkit start - Ensure chunks are loaded in case TravelAgent is not used which would initially cause chunks to load during find/create
		// minecraftserver.getPlayerList().a(this, j, worldserver, worldserver1);
		boolean before = worldserver1.theChunkProviderServer.loadChunkOnProvideRequest;  // Cauldron start - load chunks on provide request
		worldserver1.theChunkProviderServer.loadChunkOnProvideRequest = true;
		((IMixinServerConfigurationManager) worldserver1.func_73046_m().getConfigurationManager()).repositionEntity((Entity) (Object) this, exit, portal);
		worldserver1.theChunkProviderServer.loadChunkOnProvideRequest = before; // Cauldron end
		// CraftBukkit end
		this.worldObj.theProfiler.endStartSection("reloading");
		Entity entity = EntityList.createEntityByName(EntityList.getEntityString((Entity) (Object) this), worldserver1);
		if (entity != null)
		{
			entity.copyDataFrom((Entity) (Object) this, true);
			worldserver1.spawnEntityInWorld(entity);
			// CraftBukkit start - Forward the CraftEntity to the new entity
			this.getBukkitEntity().setHandle(entity);
			((IMixinEntity) entity).setBukkitEntity(this.getBukkitEntity());
			// CraftBukkit end
		}
		this.isDead = true;
		if ((Entity) (Object) this instanceof EntityItem) // Thermos kill this stack to avoid dupe glitch?
		{
			ItemStack stack = this.dataWatcher.getWatchableObjectItemStack(10);
			stack.stackSize = 0;
		}
		this.worldObj.theProfiler.endSection();
		worldserver.resetUpdateEntityTick();
		worldserver1.resetUpdateEntityTick();
		this.worldObj.theProfiler.endSection();
	}
}
