package org.ultramine.mods.bukkit.mixin.inventory;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.ContainerHopper;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import org.ultramine.mods.bukkit.interfaces.inventory.IMixinContainer;

@Mixin(ContainerHopper.class)
public class MixinContainerHopper
{
	@Inject(method = "canInteractWith", cancellable = true, at = @At(value = "HEAD"))
	private void canInteractWith(EntityPlayer p_75145_1_, CallbackInfoReturnable<Boolean> cir)
	{
		if (!((IMixinContainer) this).checkReachable())
		{
			cir.setReturnValue(true);
			cir.cancel();
		}
	}
}
