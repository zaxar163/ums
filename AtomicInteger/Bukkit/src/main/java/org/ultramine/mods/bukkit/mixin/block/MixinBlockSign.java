package org.ultramine.mods.bukkit.mixin.block;

import net.minecraft.block.Block;
import net.minecraft.block.BlockSign;
import net.minecraft.world.World;
import org.bukkit.Bukkit;
import org.bukkit.event.block.BlockRedstoneEvent;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.ultramine.mods.bukkit.interfaces.world.IMixinWorld;

@Mixin(BlockSign.class)
public class MixinBlockSign
{
	@Inject(method = "onNeighborBlockChange", at = @At(value = "RETURN"))
	private void onNeighborBlockChangeInject(World world, int x, int y, int z, Block block, CallbackInfo ci)
	{
		if (block != null && block.canProvidePower())
		{
			org.bukkit.block.Block bukkitBlock = ((IMixinWorld) world).getWorld().getBlockAt(x, y, z);
			int power = bukkitBlock.getBlockPower();
			BlockRedstoneEvent eventRedstone = new BlockRedstoneEvent(bukkitBlock, power, power);
			Bukkit.getPluginManager().callEvent(eventRedstone);
		}
	}
}
