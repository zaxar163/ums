package org.ultramine.mods.bukkit.mixin.management;

import com.mojang.authlib.GameProfile;
import cpw.mods.fml.common.FMLCommonHandler;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.network.play.server.S07PacketRespawn;
import net.minecraft.network.play.server.S1DPacketEntityEffect;
import net.minecraft.potion.PotionEffect;
import net.minecraft.scoreboard.ServerScoreboard;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.management.BanList;
import net.minecraft.server.management.IPBanEntry;
import net.minecraft.server.management.UserListBans;
import net.minecraft.server.management.UserListBansEntry;
import net.minecraft.server.network.NetHandlerLoginServer;
import net.minecraft.util.ChunkCoordinates;
import net.minecraft.util.MathHelper;
import net.minecraft.world.Teleporter;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.TravelAgent;
import org.bukkit.craftbukkit.CraftWorld;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerPortalEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.util.Vector;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;
import org.spongepowered.asm.mixin.Shadow;
import org.ultramine.core.service.InjectService;
import org.ultramine.mods.bukkit.CraftPlayerCache;
import org.ultramine.mods.bukkit.interfaces.entity.IMixinEntity;
import org.ultramine.mods.bukkit.interfaces.management.IMixinServerConfigurationManager;
import org.ultramine.mods.bukkit.interfaces.management.IMixinUserListEntry;
import org.ultramine.mods.bukkit.interfaces.world.IMixinTeleporter;
import org.ultramine.mods.bukkit.interfaces.world.IMixinWorld;

import java.net.SocketAddress;
import java.text.SimpleDateFormat;
import java.util.List;

@Mixin(value = net.minecraft.server.management.ServerConfigurationManager.class)
public abstract class MixinServerConfigurationManager implements IMixinServerConfigurationManager
{
	@InjectService
	private static CraftPlayerCache cPlayerCache;
	@Shadow
	private static SimpleDateFormat dateFormat;
	@Shadow
	private MinecraftServer mcServer;
	@Shadow
	public List<EntityPlayerMP> playerEntityList;
	@Shadow
	private UserListBans bannedPlayers;
	@Shadow
	private BanList bannedIPs;
	@Shadow
	protected int maxPlayers;

	@Shadow
	protected abstract void func_96456_a(ServerScoreboard p_96456_1_, EntityPlayerMP p_96456_2_);

	@Shadow
	public abstract boolean func_152607_e(GameProfile p_152607_1_);

	@Shadow public abstract void transferEntityToWorld(Entity p_82448_1_, int p_82448_2_, WorldServer p_82448_3_, WorldServer p_82448_4_, Teleporter teleporter);
	@Shadow public abstract void func_72375_a(EntityPlayerMP p_72375_1_, WorldServer p_72375_2_);
	@Shadow public abstract void updateTimeAndWeatherForPlayer(EntityPlayerMP p_72354_1_, WorldServer p_72354_2_);
	@Shadow public abstract void syncPlayerInventory(EntityPlayerMP p_72385_1_);

	// CraftBukkit start - Whole method, SocketAddress to LoginListener, added hostname to signature, return EntityPlayer
	@Override
	public EntityPlayerMP attemptLogin(NetHandlerLoginServer loginlistener, GameProfile gameprofile, String hostname, EntityPlayerMP entity)
	{
		// Instead of kicking then returning, we need to store the kick reason
		// in the event, check with plugins to see if it's ok, and THEN kick
		// depending on the outcome.
		SocketAddress socketaddress = loginlistener.field_147333_a.getSocketAddress();
		cPlayerCache.updateReferences(entity);
		Player player = (Player) ((IMixinEntity) entity).getBukkitEntity();
		PlayerLoginEvent event = new PlayerLoginEvent(player, hostname, ((java.net.InetSocketAddress) socketaddress).getAddress(),
				((java.net.InetSocketAddress) loginlistener.field_147333_a.channel().remoteAddress()).getAddress()); // Spigot
		String s;

		if(this.bannedPlayers.func_152702_a(gameprofile) && !((IMixinUserListEntry) this.bannedPlayers.func_152683_b(gameprofile)).hasBanExpiredPub())
		{
			UserListBansEntry banentry = (UserListBansEntry) this.bannedPlayers.func_152683_b(gameprofile);
			s = "You are banned from this server!\nReason: " + banentry.getBanReason();

			if(banentry.getBanEndDate() != null)
			{
				s = s + "\nYour ban will be removed on " + dateFormat.format(banentry.getBanEndDate());
			}

			// return s;
			event.disallow(PlayerLoginEvent.Result.KICK_BANNED, s);
		}
		else if(!this.func_152607_e(gameprofile))
		{
			// return "You are not white-listed on this server!";
			event.disallow(PlayerLoginEvent.Result.KICK_WHITELIST, "You are not white-listed on this server!");
		}
		else if(this.bannedIPs.func_152708_a(socketaddress) && !((IMixinUserListEntry) this.bannedPlayers.func_152683_b(gameprofile)).hasBanExpiredPub())
		{
			IPBanEntry ipbanentry = this.bannedIPs.func_152709_b(socketaddress);
			s = "Your IP address is banned from this server!\nReason: " + ipbanentry.getBanReason();

			if(ipbanentry.getBanEndDate() != null)
			{
				s = s + "\nYour ban will be removed on " + dateFormat.format(ipbanentry.getBanEndDate());
			}
			// return s;
			event.disallow(PlayerLoginEvent.Result.KICK_BANNED, s);
		}
		else
		{
			// return this.players.size() >= this.maxPlayers ? "The server is full!" : null;
			if(this.playerEntityList.size() >= this.maxPlayers)
			{
				event.disallow(PlayerLoginEvent.Result.KICK_FULL, "The server is full!");
			}
		}

		Bukkit.getServer().getPluginManager().callEvent(event);

		if(event.getResult() != PlayerLoginEvent.Result.ALLOWED)
		{
			loginlistener.func_147322_a(event.getKickMessage());
			return null;
		}

		return entity;
	}
	// CraftBukkit end

	public void sendScoreboard(ServerScoreboard sb, EntityPlayerMP player)
	{
		func_96456_a(sb, player);
	}

	// TODO: FIX RETURN COORDINATES!
	@Override
	// Copy of original a(Entity, int, WorldServer, WorldServer) method with only location calculation logic
	public Location calculateTarget(Location enter, World target)
	{
		WorldServer worldserver = ((CraftWorld) enter.getWorld()).getHandle();
		WorldServer worldserver1 = (((IMixinWorld) target).getWorld()).getHandle();
		int i = worldserver.provider.dimensionId;
		double y = enter.getY();
		float yaw = enter.getYaw();
		float pitch = enter.getPitch();
		double d0 = enter.getX();
		double d1 = enter.getZ();
		if (i == -1)
		{
			d0 /= 8.0D;
			d1 /= 8.0D;
		}
		else if (i == 0)
		{
			d0 *= 8.0D;
			d1 *= 8.0D;
		}
		else
		{
			ChunkCoordinates chunkcoordinates;
			if (i == 1)
			{
				// use default NORMAL world spawn instead of target
				worldserver1 = this.mcServer.worldServers[0];
				chunkcoordinates = worldserver1.getSpawnPoint();
			}
			else
			{
				chunkcoordinates = worldserver1.getEntrancePortalLocation();
			}
			// Cauldron start - validate chunkcoordinates
			if (chunkcoordinates != null)
			{
				d0 = (double) chunkcoordinates.posX;
				y = (double) chunkcoordinates.posY;
				d1 = (double) chunkcoordinates.posZ;
				yaw = 90.0F;
				pitch = 0.0F;
			}
			// Cauldron end
		}
		if (i != 1)
		{
			d0 = (double) MathHelper.clamp_int((int) d0, -29999872, 29999872);
			d1 = (double) MathHelper.clamp_int((int) d1, -29999872, 29999872);
		}
		return new Location(((IMixinWorld) worldserver1).getWorld(), d0, y, d1, yaw, pitch);
	}

	@Override
	// copy of original a(Entity, int, WorldServer, WorldServer) method with only entity repositioning logic
	public void repositionEntity(Entity entity, Location exit, boolean portal)
	{
		int i = entity.dimension;
		WorldServer worldserver = (WorldServer) entity.worldObj;
		WorldServer worldserver1 = ((CraftWorld) exit.getWorld()).getHandle();
		worldserver.theProfiler.startSection("moving");
		entity.setLocationAndAngles(exit.getX(), exit.getY(), exit.getZ(), exit.getYaw(), exit.getPitch());
		if (entity.isEntityAlive())
		{
			worldserver.updateEntityWithOptionalForce(entity, false);
		}
		worldserver.theProfiler.endSection();
		if (i != 1)
		{
			worldserver.theProfiler.startSection("placing");
			if (entity.isEntityAlive())
			{
				if (portal)
				{
					Vector velocity = ((IMixinEntity) entity).getBukkitEntity().getVelocity();
					((IMixinTeleporter) worldserver1.getDefaultTeleporter()).adjustExit(entity, exit, velocity); // Should be getTravelAgententity.setLocationAndAngles(exit.getX(), exit.getY(), exit.getZ(), exit.getYaw(), exit.getPitch());
					if (entity.motionX != velocity.getX() || entity.motionY != velocity.getY() || entity.motionZ != velocity.getZ())
						((IMixinEntity) entity).getBukkitEntity().setVelocity(velocity);
				}
				worldserver1.spawnEntityInWorld(entity);
				worldserver1.updateEntityWithOptionalForce(entity, false);
			}
			worldserver.theProfiler.endSection();
		}
		entity.setWorld(worldserver1);
	}

	// MOVED TO PlayerHandlerEvent.
//	//@Overwrite
//	public void transferPlayerToDimension(EntityPlayerMP p_72356_1_, int p_72356_2_, Teleporter teleporter) // mods such as Twilight Forest call this method directly
//	{
//		this.transferPlayerToDimension(p_72356_1_, p_72356_2_, teleporter, PlayerTeleportEvent.TeleportCause.MOD); // use our mod cause
//	}
//
//	public void transferPlayerToDimension(EntityPlayerMP par1EntityPlayerMP, int par2, PlayerTeleportEvent.TeleportCause cause)
//	{
//		this.transferPlayerToDimension(par1EntityPlayerMP, par2, mcServer.worldServerForDimension(par2).getDefaultTeleporter(), cause);
//	}
//
//	public void transferPlayerToDimension(EntityPlayerMP par1EntityPlayerMP, int targetDimension, Teleporter teleporter, PlayerTeleportEvent.TeleportCause cause) // Cauldron - add TeleportCause
//	{
//		// Allow Forge hotloading on teleport
//		WorldServer fromWorld = this.mcServer.worldServerForDimension(par1EntityPlayerMP.dimension);
//		WorldServer exitWorld = this.mcServer.worldServerForDimension(targetDimension);
//		// CraftBukkit start - Replaced the standard handling of portals with a more customised method.
//		Location enter = ((IMixinEntity) par1EntityPlayerMP).getBukkitEntity().getLocation();
//		Location exit = null;
//		boolean useTravelAgent = false;
//		if (exitWorld != null)
//		{
//			exit = this.calculateTarget(enter, exitWorld);
//			if (cause != PlayerTeleportEvent.TeleportCause.MOD) // don't use travel agent for custom dimensions
//				useTravelAgent = true;
//		}
//		// allow forge mods to be the teleporter
//		TravelAgent agent = null;
//		if (exit != null && teleporter == null)
//		{
//			teleporter = ((CraftWorld) exit.getWorld()).getHandle().getDefaultTeleporter();
//			if (teleporter instanceof TravelAgent)
//				agent = (TravelAgent) teleporter;
//		}
//		else
//		{
//			if (teleporter instanceof TravelAgent)
//				agent = (TravelAgent) teleporter;
//		}
//		if (agent == null) // mod teleporter such as Twilight Forest
//			agent = org.bukkit.craftbukkit.CraftTravelAgent.DEFAULT; // return arbitrary TA to compensate for implementation dependent plugins
//		PlayerPortalEvent event = new PlayerPortalEvent((Player) ((IMixinEntity) par1EntityPlayerMP).getBukkitEntity(), enter, exit, agent, cause);
//		event.useTravelAgent(useTravelAgent);
//		Bukkit.getServer().getPluginManager().callEvent(event);
//		if (event.isCancelled() || event.getTo() == null)
//			return;
//		exit = event.useTravelAgent() && cause != PlayerTeleportEvent.TeleportCause.MOD ? event.getPortalTravelAgent().findOrCreate(event.getTo()) : event.getTo(); // make sure plugins don't override travelagent for mods
//		if (exit == null)
//			return;
//		exitWorld = ((CraftWorld) exit.getWorld()).getHandle();
//		Vector velocity = ((IMixinEntity) par1EntityPlayerMP).getBukkitEntity().getVelocity();
//		boolean before = exitWorld.theChunkProviderServer.loadChunkOnProvideRequest;
//		exitWorld.theChunkProviderServer.loadChunkOnProvideRequest = true;
//		((IMixinTeleporter) exitWorld.getDefaultTeleporter()).adjustExit(par1EntityPlayerMP, exit, velocity);
//		exitWorld.theChunkProviderServer.loadChunkOnProvideRequest = before;
//		par1EntityPlayerMP.dimension = targetDimension;
//		par1EntityPlayerMP.playerNetServerHandler.sendPacket(new S07PacketRespawn(par1EntityPlayerMP.dimension, par1EntityPlayerMP.worldObj.difficultySetting,
//				par1EntityPlayerMP.worldObj.getWorldInfo().getTerrainType(), par1EntityPlayerMP.theItemInWorldManager.getGameType()));
//		fromWorld.removePlayerEntityDangerously(par1EntityPlayerMP);
//		par1EntityPlayerMP.isDead = false;
//		this.transferEntityToWorld(par1EntityPlayerMP, fromWorld.provider.dimensionId, fromWorld, exitWorld, teleporter);
//		this.func_72375_a(par1EntityPlayerMP, fromWorld);
//		par1EntityPlayerMP.playerNetServerHandler.setPlayerLocation(par1EntityPlayerMP.posX, par1EntityPlayerMP.posY, par1EntityPlayerMP.posZ, par1EntityPlayerMP.rotationYaw, par1EntityPlayerMP.rotationPitch);
//		par1EntityPlayerMP.theItemInWorldManager.setWorld(exitWorld);
//		this.updateTimeAndWeatherForPlayer(par1EntityPlayerMP, exitWorld);
//		this.syncPlayerInventory(par1EntityPlayerMP);
//		for (Object o : par1EntityPlayerMP.getActivePotionEffects())
//		{
//			PotionEffect potioneffect = (PotionEffect) o;
//			par1EntityPlayerMP.playerNetServerHandler.sendPacket(new S1DPacketEntityEffect(par1EntityPlayerMP.getEntityId(), potioneffect));
//		}
//		FMLCommonHandler.instance().firePlayerChangedDimensionEvent(par1EntityPlayerMP, fromWorld.provider.dimensionId, targetDimension);
//	}
//
//	@Overwrite
//	public void transferEntityToWorld(Entity p_82448_1_, int p_82448_2_, WorldServer p_82448_3_, WorldServer p_82448_4_)
//	{
//		// CraftBukkit start - Split into modular functions
//		// transferEntityToWorld(p_82448_1_, p_82448_2_, p_82448_3_, p_82448_4_, p_82448_4_.getDefaultTeleporter());
//		Location exit = this.calculateTarget(((IMixinEntity) p_82448_1_).getBukkitEntity().getLocation(), p_82448_4_);
//		this.repositionEntity(p_82448_1_, exit, true);
//		// CraftBukkit end
//	}
}
