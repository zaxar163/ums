package org.ultramine.mods.bukkit.mixin.entity.monster;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.monster.EntityBlaze;
import net.minecraft.entity.monster.EntityEnderman;
import net.minecraft.entity.monster.EntityGiantZombie;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.monster.EntitySilverfish;
import net.minecraft.entity.monster.EntitySpider;
import net.minecraft.world.World;
import org.bukkit.craftbukkit.entity.CraftEntity;
import org.bukkit.craftbukkit.event.CraftEventFactory;
import org.bukkit.event.entity.EntityTargetEvent;
import org.objectweb.asm.Opcodes;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Redirect;

@Mixin(EntityMob.class)
public class MixinEntityMob extends EntityCreature
{
	public MixinEntityMob(World p_i1602_1_)
	{
		super(p_i1602_1_);
	}

	@Redirect(method = "attackEntityFrom", at = @At(value = "FIELD", opcode = Opcodes.PUTFIELD, target = "Lnet/minecraft/entity/monster/EntityMob;entityToAttack:Lnet/minecraft/entity/Entity;"))
	private void attackEntityFromRedirect(EntityMob entityMob, Entity entity)
	{
		if (entity != this.entityToAttack && (((EntityMob)(Object) this) instanceof EntityBlaze || ((EntityMob)(Object) this) instanceof EntityEnderman || ((EntityMob)(Object) this) instanceof EntitySpider || ((EntityMob)(Object) this) instanceof EntityGiantZombie || ((EntityMob)(Object) this) instanceof EntitySilverfish))
		{
			EntityTargetEvent event = CraftEventFactory.callEntityTargetEvent(this, entity, EntityTargetEvent.TargetReason.TARGET_ATTACKED_ENTITY);
			if (!event.isCancelled())
				if (event.getTarget() == null)
					this.entityToAttack = null;
				else
					this.entityToAttack = ((CraftEntity) event.getTarget()).getHandle();
		}
		else
		{
			this.entityToAttack = entity;
		}
	}
}
