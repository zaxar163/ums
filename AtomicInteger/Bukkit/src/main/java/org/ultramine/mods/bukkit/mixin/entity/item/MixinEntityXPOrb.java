package org.ultramine.mods.bukkit.mixin.entity.item;

import net.minecraft.block.material.Material;
import net.minecraft.entity.Entity;
import net.minecraft.entity.item.EntityXPOrb;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;
import org.bukkit.craftbukkit.entity.CraftEntity;
import org.bukkit.craftbukkit.event.CraftEventFactory;
import org.bukkit.event.entity.EntityTargetEvent;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Redirect;

@Mixin(EntityXPOrb.class)
public abstract class MixinEntityXPOrb extends Entity
{
	@Shadow public int xpColor;
	@Shadow public int xpOrbAge;
	@Shadow public int field_70532_c;
	@Shadow public int xpValue;
	@Shadow private EntityPlayer closestPlayer;
	@Shadow private int xpTargetColor;

	public MixinEntityXPOrb(World p_i1582_1_)
	{
		super(p_i1582_1_);
	}

	@Overwrite
	public void onUpdate()
	{
		super.onUpdate();
		if (this.field_70532_c > 0)
			--this.field_70532_c;
		this.prevPosX = this.posX;
		this.prevPosY = this.posY;
		this.prevPosZ = this.posZ;
		this.motionY -= 0.029999999329447746D;
		if (this.worldObj.getBlock(MathHelper.floor_double(this.posX), MathHelper.floor_double(this.posY), MathHelper.floor_double(this.posZ)).getMaterial() == Material.lava)
		{
			this.motionY = 0.20000000298023224D;
			this.motionX = (double) ((this.rand.nextFloat() - this.rand.nextFloat()) * 0.2F);
			this.motionZ = (double) ((this.rand.nextFloat() - this.rand.nextFloat()) * 0.2F);
			this.playSound("random.fizz", 0.4F, 2.0F + this.rand.nextFloat() * 0.4F);
		}
		this.func_145771_j(this.posX, (this.boundingBox.minY + this.boundingBox.maxY) / 2.0D, this.posZ);
		double d0 = 8.0D;
		if (this.xpTargetColor < this.xpColor - 20 + this.getEntityId() % 100)
		{
			if (this.closestPlayer == null || this.closestPlayer.getDistanceSqToEntity(this) > d0 * d0)
				this.closestPlayer = this.worldObj.getClosestPlayerToEntity(this, d0);
			this.xpTargetColor = this.xpColor;
		}
		if (this.closestPlayer != null)
		{
			EntityTargetEvent event = CraftEventFactory.callEntityTargetEvent(this, closestPlayer, EntityTargetEvent.TargetReason.CLOSEST_PLAYER);
			Entity target = event.getTarget() == null ? null : ((CraftEntity) event.getTarget()).getHandle();
			if (!event.isCancelled() && target != null)
			{
				double d1 = (target.posX - this.posX) / d0;
				double d2 = (target.posY + (double) target.getEyeHeight() - this.posY) / d0;
				double d3 = (target.posZ - this.posZ) / d0;
				double d4 = Math.sqrt(d1 * d1 + d2 * d2 + d3 * d3);
				double d5 = 1.0D - d4;
				if (d5 > 0.0D)
				{
					d5 *= d5;
					this.motionX += d1 / d4 * d5 * 0.1D;
					this.motionY += d2 / d4 * d5 * 0.1D;
					this.motionZ += d3 / d4 * d5 * 0.1D;
				}
			}
			this.moveEntity(this.motionX, this.motionY, this.motionZ);
			float f = 0.98F;
			if (this.onGround)
				f = this.worldObj.getBlock(MathHelper.floor_double(this.posX), MathHelper.floor_double(this.boundingBox.minY) - 1, MathHelper.floor_double(this.posZ)).slipperiness * 0.98F;
			this.motionX *= (double) f;
			this.motionY *= 0.9800000190734863D;
			this.motionZ *= (double) f;
			if (this.onGround)
				this.motionY *= -0.8999999761581421D;
			++this.xpColor;
			++this.xpOrbAge;
			if (this.xpOrbAge >= 6000)
				this.setDead();
		}
	}

	@Redirect(method = "onCollideWithPlayer", at = @At(value = "INVOKE", target = "Lnet/minecraft/entity/player/EntityPlayer;addExperience(I)V"))
	private void onCollideWithPlayer(EntityPlayer entityPlayer, int xp)
	{
		entityPlayer.addExperience(CraftEventFactory.callPlayerExpChangeEvent(entityPlayer, xp).getAmount());
	}
}
