package org.ultramine.mods.bukkit.mixin.entity.monster;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.monster.EntitySkeleton;
import net.minecraft.entity.projectile.EntityArrow;
import net.minecraft.world.World;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.event.CraftEventFactory;
import org.bukkit.event.entity.EntityCombustEvent;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.Redirect;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;
import org.ultramine.mods.bukkit.interfaces.entity.IMixinEntity;

@Mixin(EntitySkeleton.class)
public class MixinEntitySkeleton extends EntityMob
{
	public MixinEntitySkeleton(World p_i1738_1_)
	{
		super(p_i1738_1_);
	}

	@Redirect(method = "onLivingUpdate", at = @At(value = "INVOKE", target = "Lnet/minecraft/entity/monster/EntitySkeleton;setFire(I)V"))
	private void onLivingUpdateRedirect(EntitySkeleton entitySkeleton, int fireDuration)
	{
		EntityCombustEvent event = new EntityCombustEvent(((IMixinEntity) this).getBukkitEntity(), fireDuration);
		Bukkit.getPluginManager().callEvent(event);
		if (!event.isCancelled())
			this.setFire(event.getDuration());
	}

	@Inject(method = "attackEntityWithRangedAttack", cancellable = true, at = @At(value = "INVOKE", target = "Lnet/minecraft/entity/monster/EntitySkeleton;playSound(Ljava/lang/String;FF)V"), locals = LocalCapture.CAPTURE_FAILEXCEPTION)
	private void attackEntityWithRangedAttackInject(EntityLivingBase p_82196_1_, float p_82196_2_, CallbackInfo ci, EntityArrow entityarrow, int i, int j)
	{
		EntityShootBowEvent event = CraftEventFactory.callEntityShootBowEvent(this, this.getHeldItem(), entityarrow, 0.8F);
		if (event.isCancelled())
		{
			event.getProjectile().remove();
			ci.cancel();
			return;
		}
		if (event.getProjectile() == ((IMixinEntity) entityarrow).getBukkitEntity())
			worldObj.spawnEntityInWorld(entityarrow);
	}

	@Redirect(method = "attackEntityWithRangedAttack", at = @At(value = "INVOKE", target = "Lnet/minecraft/world/World;spawnEntityInWorld(Lnet/minecraft/entity/Entity;)Z"))
	private boolean attackEntityWithRangedAttackInvokeDelete(World world, Entity entity)
	{
		// Do nothing, it's just method invoke removing.
		return false;
	}
}
