package org.ultramine.mods.bukkit.mixin.entity.monster;

import net.minecraft.entity.Entity;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.monster.EntitySpider;
import net.minecraft.world.World;
import org.bukkit.Bukkit;
import org.bukkit.event.entity.EntityTargetEvent;
import org.objectweb.asm.Opcodes;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Redirect;
import org.ultramine.mods.bukkit.interfaces.entity.IMixinEntity;

@Mixin(EntitySpider.class)
public class MixinEntitySpider extends EntityMob
{
	public MixinEntitySpider(World p_i1738_1_)
	{
		super(p_i1738_1_);
	}

	@Redirect(method = "attackEntity", at = @At(value = "FIELD", opcode = Opcodes.PUTFIELD, target = "Lnet/minecraft/entity/monster/EntitySpider;entityToAttack:Lnet/minecraft/entity/Entity;"))
	private void attackEntityRedirect(EntitySpider entitySpider, Entity value)
	{
		EntityTargetEvent event = new EntityTargetEvent(((IMixinEntity) this).getBukkitEntity(), null, EntityTargetEvent.TargetReason.FORGOT_TARGET);
		Bukkit.getPluginManager().callEvent(event);
		if (!event.isCancelled())
			if (event.getTarget() == null)
				this.entityToAttack = null;
			else
				this.entityToAttack = ((org.bukkit.craftbukkit.entity.CraftEntity) event.getTarget()).getHandle();
	}
}
