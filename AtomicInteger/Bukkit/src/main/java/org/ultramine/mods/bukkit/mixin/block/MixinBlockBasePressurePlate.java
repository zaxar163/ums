package org.ultramine.mods.bukkit.mixin.block;

import net.minecraft.block.BlockBasePressurePlate;
import net.minecraft.world.World;
import org.bukkit.Bukkit;
import org.bukkit.event.block.BlockRedstoneEvent;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;
import org.spongepowered.asm.mixin.Shadow;
import org.ultramine.mods.bukkit.interfaces.world.IMixinWorld;

@Mixin(BlockBasePressurePlate.class)
public abstract class MixinBlockBasePressurePlate
{
	@Shadow protected abstract int func_150065_e(World var1, int var2, int var3, int var4);
	@Shadow protected abstract void func_150064_a_(World p_150064_1_, int p_150064_2_, int p_150064_3_, int p_150064_4_);
	@Shadow protected abstract int func_150066_d(int var1);
	@Shadow public abstract int tickRate(World p_149738_1_);

	@Overwrite
	protected void func_150062_a(World world, int x, int y, int z, int p_150062_5_)
	{
		int i1 = this.func_150065_e(world, x, y, z);
		boolean flag = p_150062_5_ > 0;
		boolean flag1 = i1 > 0;
		if (flag != flag1)
		{
			BlockRedstoneEvent eventRedstone = new BlockRedstoneEvent(((IMixinWorld) world).getWorld().getBlockAt(x, y, z), p_150062_5_, i1);
			Bukkit.getPluginManager().callEvent(eventRedstone);
			flag1 = eventRedstone.getNewCurrent() > 0;
			i1 = eventRedstone.getNewCurrent();
		}
		if (p_150062_5_ != i1)
		{
			world.setBlockMetadataWithNotify(x, y, z, this.func_150066_d(i1), 2);
			this.func_150064_a_(world, x, y, z);
			world.markBlockRangeForRenderUpdate(x, y, z, x, y, z);
		}

		if (!flag1 && flag)
		{
			world.playSoundEffect(x + 0.5D, y + 0.1D, z + 0.5D, "random.click", 0.3F, 0.5F);
		}
		else if (flag1 && !flag)
		{
			world.playSoundEffect(x + 0.5D, y + 0.1D, z + 0.5D, "random.click", 0.3F, 0.6F);
		}

		if (flag1)
		{
			world.scheduleBlockUpdate(x, y, z, ((BlockBasePressurePlate) (Object) this), this.tickRate(world));
		}

	}
}
