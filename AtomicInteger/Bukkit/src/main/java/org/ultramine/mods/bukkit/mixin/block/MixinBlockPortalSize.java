package org.ultramine.mods.bukkit.mixin.block;

import net.minecraft.block.Block;
import net.minecraft.block.BlockPortal;
import net.minecraft.init.Blocks;
import net.minecraft.util.ChunkCoordinates;
import net.minecraft.util.Direction;
import net.minecraft.world.World;
import org.bukkit.Bukkit;
import org.bukkit.event.world.PortalCreateEvent;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;
import org.spongepowered.asm.mixin.Shadow;
import org.ultramine.mods.bukkit.interfaces.world.IMixinWorld;
import org.ultramine.mods.bukkit.util.BlockPortalPos;

import java.util.ArrayList;
import java.util.List;

@Mixin(BlockPortal.Size.class)
public abstract class MixinBlockPortalSize
{
	@Final
	@Shadow private World field_150867_a;
	@Final
	@Shadow private int field_150865_b;
	@Final
	@Shadow private int field_150866_c;
	@Shadow private ChunkCoordinates field_150861_f;
	@Shadow private int field_150862_g;
	@Shadow private int field_150868_h;
	@Shadow private int field_150864_e = 0;
	@Shadow protected abstract boolean func_150857_a(Block p_150857_1_);

	private List<org.bukkit.block.Block> frameBlocks = new ArrayList<org.bukkit.block.Block>();

	// TODO: Сделать нормальный список блоков, которые пойдут в ивент.Исправить двойной(тройной?) вызов ивента, когда он отменен.
	protected int func_150858_a()
	{
		int i;
		int j;
		int k;
		int l;
		org.bukkit.World bworld = ((IMixinWorld) this.field_150867_a).getWorld();
		label56:
		for (this.field_150862_g = 0; this.field_150862_g < 21; ++this.field_150862_g)
		{
			i = this.field_150861_f.posY + this.field_150862_g;

			for (j = 0; j < this.field_150868_h; ++j)
			{
				k = this.field_150861_f.posX + j * Direction.offsetX[BlockPortal.field_150001_a[this.field_150865_b][1]];
				l = this.field_150861_f.posZ + j * Direction.offsetZ[BlockPortal.field_150001_a[this.field_150865_b][1]];
				Block block = this.field_150867_a.getBlock(k, i, l);
				if (!this.func_150857_a(block))
				{
					break label56;
				}

				if (block == Blocks.portal)
				{
					++this.field_150864_e;
				}

				if (j == 0)
				{
					block = this.field_150867_a.getBlock(k + Direction.offsetX[BlockPortal.field_150001_a[this.field_150865_b][0]], i, l + Direction.offsetZ[BlockPortal.field_150001_a[this.field_150865_b][0]]);
					if (block != Blocks.obsidian)
					{
						break label56;
					} else
					{
						frameBlocks.add(bworld.getBlockAt(k + Direction.offsetX[BlockPortal.field_150001_a[this.field_150865_b][0]], i, l + Direction.offsetZ[BlockPortal.field_150001_a[this.field_150865_b][0]]));
					}
				}
				else if (j == this.field_150868_h - 1)
				{
					block = this.field_150867_a.getBlock(k + Direction.offsetX[BlockPortal.field_150001_a[this.field_150865_b][1]], i, l + Direction.offsetZ[BlockPortal.field_150001_a[this.field_150865_b][1]]);
					if (block != Blocks.obsidian)
					{
						break label56;
					}
					else
					{
						frameBlocks.add(bworld.getBlockAt(k + Direction.offsetX[BlockPortal.field_150001_a[this.field_150865_b][1]], i, l + Direction.offsetZ[BlockPortal.field_150001_a[this.field_150865_b][1]]));
					}
				}
			}
		}

		for (i = 0; i < this.field_150868_h; ++i)
		{
			j = this.field_150861_f.posX + i * Direction.offsetX[BlockPortal.field_150001_a[this.field_150865_b][1]];
			k = this.field_150861_f.posY + this.field_150862_g;
			l = this.field_150861_f.posZ + i * Direction.offsetZ[BlockPortal.field_150001_a[this.field_150865_b][1]];
			if (this.field_150867_a.getBlock(j, k, l) != Blocks.obsidian)
			{
				this.field_150862_g = 0;
				break;
			}
		}

		if (this.field_150862_g <= 21 && this.field_150862_g >= 3)
		{
			return this.field_150862_g;
		}
		else
		{
			this.field_150861_f = null;
			this.field_150868_h = 0;
			this.field_150862_g = 0;
			return 0;
		}
	}

	@Overwrite
	public void func_150859_c()
	{
		List<BlockPortalPos> portalBlocksPos = new ArrayList<BlockPortalPos>();
		for (int i = 0; i < this.field_150868_h; ++i)
		{
			int j = this.field_150861_f.posX + Direction.offsetX[this.field_150866_c] * i;
			int k = this.field_150861_f.posZ + Direction.offsetZ[this.field_150866_c] * i;

			for (int l = 0; l < this.field_150862_g; ++l)
			{
				int i1 = this.field_150861_f.posY + l;
				portalBlocksPos.add(new BlockPortalPos(j, i1, k, this.field_150865_b, 2));
//				this.field_150867_a.setBlock(j, i1, k, Blocks.portal, this.field_150865_b, 2);
			}
		}
//		for (BlockPortalPos portalBlockPos : portalBlocksPos)
//		{
//			// Convert portal blocks to list
//		}
		PortalCreateEvent portalCreateEvent = new PortalCreateEvent(frameBlocks, ((IMixinWorld) this.field_150867_a).getWorld(), PortalCreateEvent.CreateReason.FIRE);
		Bukkit.getPluginManager().callEvent(portalCreateEvent);
		if (!portalCreateEvent.isCancelled())
		{
			for (BlockPortalPos portalBlockPos : portalBlocksPos)
			{
				this.field_150867_a.setBlock(
						portalBlockPos.getX(),
						portalBlockPos.getY(),
						portalBlockPos.getZ(),
						Blocks.portal,
						portalBlockPos.getField_150865_b(),
						portalBlockPos.getFlag()
				);
			}
		}
	}
}
