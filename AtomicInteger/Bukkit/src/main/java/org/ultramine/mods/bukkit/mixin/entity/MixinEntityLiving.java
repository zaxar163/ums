package org.ultramine.mods.bukkit.mixin.entity;

import org.bukkit.Bukkit;
import org.bukkit.event.entity.EntityUnleashEvent;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.ultramine.mods.bukkit.interfaces.entity.IMixinEntity;
import org.ultramine.mods.bukkit.interfaces.entity.IMixinEntityLiving;

@Mixin(net.minecraft.entity.EntityLiving.class)
public abstract class MixinEntityLiving implements IMixinEntityLiving
{
	@Shadow
	private boolean persistenceRequired;
	@Shadow
	private boolean canPickUpLoot;

	@Override
	public boolean isPersistenceRequired()
	{
		return persistenceRequired;
	}

	@Override
	public void setPersistenceRequired(boolean persistenceRequired)
	{
		this.persistenceRequired = persistenceRequired;
	}

	@Override
	public boolean isCanPickUpLoot()
	{
		return canPickUpLoot;
	}

	@Override
	public void setCanPickUpLoot(boolean canPickUpLoot)
	{
		this.canPickUpLoot = canPickUpLoot;
	}

	@Inject(method = "updateLeashedState", at = @At(value = "INVOKE", target = "Lnet/minecraft/entity/EntityLiving;clearLeashed(ZZ)V"))
	private void updateLeashedStateInject(CallbackInfo ci)
	{
		Bukkit.getPluginManager().callEvent(new EntityUnleashEvent(((IMixinEntity) this).getBukkitEntity(), EntityUnleashEvent.UnleashReason.HOLDER_GONE));
	}

	@Inject(method = "recreateLeash", at = @At(value = "INVOKE", target = "Lnet/minecraft/entity/EntityLiving;clearLeashed(ZZ)V"))
	private void recreateLeashInject(CallbackInfo ci)
	{
		Bukkit.getPluginManager().callEvent(new EntityUnleashEvent(((IMixinEntity) this).getBukkitEntity(), EntityUnleashEvent.UnleashReason.UNKNOWN));
	}
}
