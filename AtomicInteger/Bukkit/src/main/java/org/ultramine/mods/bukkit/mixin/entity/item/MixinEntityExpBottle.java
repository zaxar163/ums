package org.ultramine.mods.bukkit.mixin.entity.item;

import net.minecraft.entity.item.EntityExpBottle;
import net.minecraft.entity.item.EntityXPOrb;
import net.minecraft.entity.projectile.EntityThrowable;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;
import org.bukkit.craftbukkit.event.CraftEventFactory;
import org.bukkit.event.entity.ExpBottleEvent;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;

@Mixin(EntityExpBottle.class)
public class MixinEntityExpBottle extends EntityThrowable
{
	public MixinEntityExpBottle(World p_i1776_1_)
	{
		super(p_i1776_1_);
	}

	@Overwrite
	protected void onImpact(MovingObjectPosition p_70184_1_)
	{
		if (!this.worldObj.isRemote)
		{
			int i = 3 + this.worldObj.rand.nextInt(5) + this.worldObj.rand.nextInt(5);
			ExpBottleEvent event = CraftEventFactory.callExpBottleEvent(this, i);
			i = event.getExperience();
			if (event.getShowEffect())
				this.worldObj.playAuxSFX(2002, (int) Math.round(this.posX), (int) Math.round(this.posY), (int) Math.round(this.posZ), 0);
			while (i > 0)
			{
				int j = EntityXPOrb.getXPSplit(i);
				i -= j;
				this.worldObj.spawnEntityInWorld(new EntityXPOrb(this.worldObj, this.posX, this.posY, this.posZ, j));
			}
			this.setDead();
		}
	}
}
