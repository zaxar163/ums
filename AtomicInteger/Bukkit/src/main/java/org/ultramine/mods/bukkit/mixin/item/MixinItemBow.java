package org.ultramine.mods.bukkit.mixin.item;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.projectile.EntityArrow;
import net.minecraft.item.ItemBow;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import net.minecraftforge.event.entity.player.ArrowLooseEvent;
import org.bukkit.craftbukkit.event.CraftEventFactory;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.Redirect;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;
import org.ultramine.mods.bukkit.interfaces.entity.IMixinEntity;

@Mixin(ItemBow.class)
public class MixinItemBow
{
	@Inject(method = "onPlayerStoppedUsing", cancellable = true, at = @At(value = "INVOKE", target = "Lnet/minecraft/item/ItemStack;damageItem(ILnet/minecraft/entity/EntityLivingBase;)V"), locals = LocalCapture.CAPTURE_FAILEXCEPTION)
	private void onPlayerStoppedUsingInject(ItemStack itemStack, World world, EntityPlayer player, int p_77615_4_, CallbackInfo ci, int j, ArrowLooseEvent event, boolean flag, float f, EntityArrow entityarrow)
	{
		EntityShootBowEvent event1 = CraftEventFactory.callEntityShootBowEvent(player, itemStack, entityarrow, f);
		if (event1.isCancelled())
		{
			event1.getProjectile().remove();
			ci.cancel();
			return;
		}
		if (event1.getProjectile() == ((IMixinEntity) entityarrow).getBukkitEntity())
			world.spawnEntityInWorld(entityarrow);
	}

	@Redirect(method = "onPlayerStoppedUsing", at = @At(value = "INVOKE", target = "Lnet/minecraft/world/World;spawnEntityInWorld(Lnet/minecraft/entity/Entity;)Z"))
	private boolean onPlayerStoppedUsingInvokeDelete(World world, Entity entity)
	{
		// Do nothing, it's just method invoke removing.
		return false;
	}
}
