package org.ultramine.mods.bukkit.interfaces.network;

import com.mojang.authlib.GameProfile;
import net.minecraft.network.NetworkManager;
import net.minecraft.server.network.NetHandlerLoginServer;
import org.apache.logging.log4j.Logger;

public interface IMixinNetHLoginS
{
	GameProfile getGameProfile();

	NetworkManager getNetworkManager();

	Logger getLogger();

	void setLoginState(NetHandlerLoginServer.LoginState state);

	void func_147322_a(String p_147322_1_);
}
