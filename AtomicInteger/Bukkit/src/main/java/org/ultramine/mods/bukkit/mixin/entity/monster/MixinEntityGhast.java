package org.ultramine.mods.bukkit.mixin.entity.monster;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityFlying;
import net.minecraft.entity.monster.EntityGhast;
import net.minecraft.world.World;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.entity.CraftEntity;
import org.bukkit.event.entity.EntityTargetEvent;
import org.objectweb.asm.Opcodes;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Redirect;
import org.ultramine.mods.bukkit.interfaces.entity.IMixinEntity;

@Mixin(net.minecraft.entity.monster.EntityGhast.class)
public abstract class MixinEntityGhast extends EntityFlying
{
	@Shadow
	private Entity targetedEntity;

	public MixinEntityGhast(World p_i1587_1_)
	{
		super(p_i1587_1_);
	}

	@Redirect(method = "updateEntityActionState", at = @At(value = "FIELD", opcode = Opcodes.PUTFIELD, target = "Lnet/minecraft/entity/monster/EntityGhast;targetedEntity:Lnet/minecraft/entity/Entity;", ordinal = 0))
	private void updateEntityActionStateRedirect(EntityGhast entityGhast, Entity value)
	{
		EntityTargetEvent event = new EntityTargetEvent(((IMixinEntity) this).getBukkitEntity(), null, EntityTargetEvent.TargetReason.TARGET_DIED);
		Bukkit.getPluginManager().callEvent(event);
		if (!event.isCancelled())
			this.targetedEntity = event.getTarget() == null ? null : ((CraftEntity) event.getTarget()).getHandle();
	}

	@Redirect(method = "updateEntityActionState", at = @At(value = "FIELD", opcode = Opcodes.PUTFIELD, target = "Lnet/minecraft/entity/monster/EntityGhast;targetedEntity:Lnet/minecraft/entity/Entity;", ordinal = 1))
	private void updateEntityActionStateRedirect1(EntityGhast entityGhast, Entity value)
	{
		Entity target = this.worldObj.getClosestVulnerablePlayerToEntity(this, 100.0D);
		if (target != null)
		{
			EntityTargetEvent event = new EntityTargetEvent(((IMixinEntity) this).getBukkitEntity(), ((IMixinEntity) target).getBukkitEntity(), EntityTargetEvent.TargetReason.CLOSEST_PLAYER);
			Bukkit.getPluginManager().callEvent(event);
			if (!event.isCancelled())
				this.targetedEntity = event.getTarget() == null ? null : ((CraftEntity) event.getTarget()).getHandle();
		}
	}
}
