package org.ultramine.mods.bukkit.mixin.entity.projectile;

import net.minecraft.entity.Entity;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.item.EntityXPOrb;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.projectile.EntityFishHook;
import net.minecraft.world.World;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.event.CraftEventFactory;
import org.bukkit.entity.Fish;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerFishEvent;
import org.objectweb.asm.Opcodes;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.ModifyArg;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;
import org.ultramine.mods.bukkit.interfaces.entity.IMixinEntity;

@Mixin(EntityFishHook.class)
public abstract class MixinEntityFishHook extends Entity
{
	@Shadow public Entity field_146043_c;
	@Shadow public EntityPlayer field_146042_b;
	private PlayerFishEvent playerFishEvent;
	private boolean isProjectileHitEventFired = false;

	public MixinEntityFishHook(World p_i1582_1_)
	{
		super(p_i1582_1_);
	}

	@Inject(method = "func_146034_e", cancellable = true, at = @At(value = "FIELD", target = "Lnet/minecraft/entity/projectile/EntityFishHook;field_146042_b:Lnet/minecraft/entity/player/EntityPlayer;", opcode = Opcodes.GETFIELD, shift = At.Shift.BY, by = -1, ordinal = 0))
	private void func_146034_eInject0(CallbackInfoReturnable<Integer> cir)
	{
		PlayerFishEvent playerFishEvent = new PlayerFishEvent((Player) ((IMixinEntity) this.field_146042_b).getBukkitEntity(), ((IMixinEntity) this.field_146043_c).getBukkitEntity(), (Fish) ((IMixinEntity) this).getBukkitEntity(), PlayerFishEvent.State.CAUGHT_ENTITY);
		Bukkit.getPluginManager().callEvent(playerFishEvent);
		if (playerFishEvent.isCancelled())
		{
			cir.setReturnValue(0);
			cir.cancel();
		}
	}

	@Inject(method = "func_146034_e", cancellable = true, at = @At(value = "FIELD", target = "Lnet/minecraft/entity/projectile/EntityFishHook;field_146042_b : Lnet/minecraft/entity/player/EntityPlayer;", opcode = Opcodes.GETFIELD, ordinal = 3), locals = LocalCapture.CAPTURE_FAILEXCEPTION)
	private void func_146034_eInject1(CallbackInfoReturnable<Integer> cir, byte b0, EntityItem entityitem)
	{
		playerFishEvent = new PlayerFishEvent((Player) ((IMixinEntity) this.field_146042_b).getBukkitEntity(), ((IMixinEntity) entityitem).getBukkitEntity(), (Fish) ((IMixinEntity) this).getBukkitEntity(), PlayerFishEvent.State.CAUGHT_FISH);
		playerFishEvent.setExpToDrop(this.rand.nextInt(6) + 1);
		Bukkit.getPluginManager().callEvent(playerFishEvent);
		if (playerFishEvent.isCancelled())
		{
			cir.setReturnValue(0);
			cir.cancel();
		}
	}

	@ModifyArg(method = "func_146034_e", at = @At(value = "INVOKE", target = "Lnet/minecraft/world/World;spawnEntityInWorld(Lnet/minecraft/entity/Entity;)Z", ordinal = 1))
	private Entity func_146034_eLastArgModify(Entity entity)
	{
		if (entity instanceof EntityXPOrb)
		{
			((EntityXPOrb) entity).xpValue = this.playerFishEvent.getExpToDrop();
			return entity;
		}
		return entity;
	}

	@Inject(method = "func_146034_e", cancellable = true, at = @At(value = "FIELD", target = "Lnet/minecraft/entity/projectile/EntityFishHook;field_146051_au:Z", opcode = Opcodes.GETFIELD, shift = At.Shift.BY, by = 3))
	private void func_146034_eInject2(CallbackInfoReturnable<Integer> cir)
	{
		PlayerFishEvent playerFishEvent = new PlayerFishEvent((Player) ((IMixinEntity) this.field_146042_b).getBukkitEntity(), null, (Fish) ((IMixinEntity) this).getBukkitEntity(), PlayerFishEvent.State.IN_GROUND);
		Bukkit.getPluginManager().callEvent(playerFishEvent);
		if (playerFishEvent.isCancelled())
		{
			cir.setReturnValue(0);
			cir.cancel();
		}
	}

	@Inject(method = "func_146034_e", cancellable = true, at = @At(value = "INVOKE", target = "Lnet/minecraft/entity/projectile/EntityFishHook;setDead()V", shift = At.Shift.BEFORE), locals = LocalCapture.CAPTURE_FAILEXCEPTION)
	private void func_146034_eInject3(CallbackInfoReturnable<Integer> cir, byte b0)
	{
		if (b0 == 0)
		{
			PlayerFishEvent playerFishEvent = new PlayerFishEvent((Player) ((IMixinEntity) this.field_146042_b).getBukkitEntity(), null, (Fish) ((IMixinEntity) this).getBukkitEntity(), PlayerFishEvent.State.FAILED_ATTEMPT);
			Bukkit.getPluginManager().callEvent(playerFishEvent);
			if (playerFishEvent.isCancelled())
			{
				cir.setReturnValue(0);
				cir.cancel();
			}
		}
	}

	@Inject(method = "onUpdate", at = @At(value = "FIELD", opcode = Opcodes.GETFIELD, target = "Lnet/minecraft/util/MovingObjectPosition;entityHit:Lnet/minecraft/entity/Entity;", ordinal = 0, shift = At.Shift.BY, by = -2))
	private void onUpdateInject(CallbackInfo ci)
	{
		if (this.isProjectileHitEventFired)
			return;
		CraftEventFactory.callProjectileHitEvent(this);
		this.isProjectileHitEventFired = true;
	}
}

