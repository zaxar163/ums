package org.ultramine.mods.bukkit.mixin.entity;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityCreature;
import net.minecraft.pathfinding.PathEntity;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.entity.CraftEntity;
import org.bukkit.event.entity.EntityTargetEvent;
import org.bukkit.event.entity.EntityUnleashEvent;
import org.objectweb.asm.Opcodes;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.Redirect;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.ultramine.mods.bukkit.interfaces.entity.IMixinEntity;
import org.ultramine.mods.bukkit.interfaces.entity.IMixinEntityCreature;

@Mixin(net.minecraft.entity.EntityCreature.class)
public abstract class MixinEntityCreature implements IMixinEntityCreature
{
	@Shadow
	private PathEntity pathToEntity;
	@Shadow
	protected Entity entityToAttack;

	@Override
	public PathEntity getPathToEntity()
	{
		return pathToEntity;
	}

	@Override
	public void setPathToEntity(PathEntity pathToEntity)
	{
		this.pathToEntity = pathToEntity;
	}

	@Override
	public Entity getEntityToAttack()
	{
		return entityToAttack;
	}

	@Override
	public void setEntityToAttack(Entity entityToAttack)
	{
		this.entityToAttack = entityToAttack;
	}

	@Shadow protected abstract Entity findPlayerToAttack();

	@Redirect(method = "updateEntityActionState", at = @At(value = "FIELD", opcode = Opcodes.PUTFIELD, target = "Lnet/minecraft/entity/EntityCreature;entityToAttack:Lnet/minecraft/entity/Entity;", ordinal = 0))
	private void updateEntityActionStateRedirect0(EntityCreature entityCreature, Entity value)
	{
		Entity target = this.findPlayerToAttack();
		if (target != null)
		{
			EntityTargetEvent event = new EntityTargetEvent(((IMixinEntity) this).getBukkitEntity(), ((IMixinEntity) target).getBukkitEntity(), EntityTargetEvent.TargetReason.CLOSEST_PLAYER);
			Bukkit.getPluginManager().callEvent(event);
			if (!event.isCancelled())
				this.entityToAttack = event.getTarget() == null ? null : ((CraftEntity) event.getTarget()).getHandle();
		}
	}

	@Redirect(method = "updateEntityActionState", at = @At(value = "FIELD", opcode = Opcodes.PUTFIELD, target = "Lnet/minecraft/entity/EntityCreature;entityToAttack:Lnet/minecraft/entity/Entity;", ordinal = 1))
	private void updateEntityActionStateRedirect1(EntityCreature entityCreature, Entity value)
	{
		EntityTargetEvent event = new EntityTargetEvent(((IMixinEntity) this).getBukkitEntity(), null, EntityTargetEvent.TargetReason.TARGET_DIED);
		Bukkit.getPluginManager().callEvent(event);
		if (!event.isCancelled())
			this.entityToAttack = event.getTarget() == null ? null : ((CraftEntity) event.getTarget()).getHandle();
	}

	@Inject(method = "updateLeashedState", at = @At(value = "INVOKE", target = "Lnet/minecraft/entity/EntityCreature;clearLeashed(ZZ)V"))
	private void updateLeashedStateInject(CallbackInfo ci)
	{
		Bukkit.getPluginManager().callEvent(new EntityUnleashEvent(((IMixinEntity) this).getBukkitEntity(), EntityUnleashEvent.UnleashReason.DISTANCE));
	}
}
