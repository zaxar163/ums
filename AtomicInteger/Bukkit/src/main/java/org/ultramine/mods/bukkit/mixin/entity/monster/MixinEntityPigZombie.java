package org.ultramine.mods.bukkit.mixin.entity.monster;

import net.minecraft.entity.Entity;
import net.minecraft.entity.monster.EntityZombie;
import net.minecraft.world.World;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.entity.CraftEntity;
import org.bukkit.event.entity.EntityTargetEvent;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;
import org.spongepowered.asm.mixin.Shadow;
import org.ultramine.mods.bukkit.interfaces.entity.IMixinEntity;
import org.ultramine.mods.bukkit.interfaces.entity.monster.IMixinEntityPigZombie;

@Mixin(net.minecraft.entity.monster.EntityPigZombie.class)
public class MixinEntityPigZombie extends EntityZombie implements IMixinEntityPigZombie
{
	@Shadow
	private int angerLevel;
	@Shadow private int randomSoundDelay;

	public MixinEntityPigZombie(World p_i1745_1_)
	{
		super(p_i1745_1_);
	}

	@Override
	public int getAngerLevel()
	{
		return angerLevel;
	}

	@Override
	public void setAngerLevel(int angerLevel)
	{
		this.angerLevel = angerLevel;
	}

	@Overwrite
	private void becomeAngryAt(Entity entity)
	{
		org.bukkit.entity.Entity bukkitTarget = entity == null ? null : ((IMixinEntity) entity).getBukkitEntity();
		EntityTargetEvent event = new EntityTargetEvent(((IMixinEntity) this).getBukkitEntity(), bukkitTarget, EntityTargetEvent.TargetReason.PIG_ZOMBIE_TARGET);
		Bukkit.getPluginManager().callEvent(event);
		if (event.isCancelled())
			return;
		if (event.getTarget() == null)
		{
			this.entityToAttack = null;
			return;
		}
		entity = ((CraftEntity) event.getTarget()).getHandle();
		this.entityToAttack = entity;
		this.angerLevel = 400 + this.rand.nextInt(400);
		this.randomSoundDelay = this.rand.nextInt(40);
	}
}
