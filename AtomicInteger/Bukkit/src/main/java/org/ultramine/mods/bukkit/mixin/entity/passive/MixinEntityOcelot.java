package org.ultramine.mods.bukkit.mixin.entity.passive;

import net.minecraft.entity.ai.EntityAITempt;
import net.minecraft.entity.passive.EntityOcelot;
import net.minecraft.entity.passive.EntityTameable;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import org.bukkit.craftbukkit.event.CraftEventFactory;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;
import org.spongepowered.asm.mixin.Shadow;

@Mixin(EntityOcelot.class)
public abstract class MixinEntityOcelot extends EntityTameable
{
	@Shadow private EntityAITempt aiTempt;
	@Shadow public abstract void setTameSkin(int p_70912_1_);

	public MixinEntityOcelot(World world)
	{
		super(world);
	}

	@Overwrite
	public boolean interact(EntityPlayer player)
	{
		ItemStack itemstack = player.inventory.getCurrentItem();
		if (this.isTamed())
		{
			if (this.func_152114_e(player) && !this.worldObj.isRemote && !this.isBreedingItem(itemstack))
				this.aiSit.setSitting(!this.isSitting());
		}
		else if (this.aiTempt.isRunning() && itemstack != null && itemstack.getItem() == Items.fish && player.getDistanceSqToEntity(this) < 9.0D)
		{
			if (!player.capabilities.isCreativeMode)
				--itemstack.stackSize;
			if (itemstack.stackSize <= 0)
				player.inventory.setInventorySlotContents(player.inventory.currentItem, null);
			if (!this.worldObj.isRemote)
			{
				if (this.rand.nextInt(3) == 0 && !CraftEventFactory.callEntityTameEvent(this, player).isCancelled())
				{
					this.setTamed(true);
					this.setTameSkin(1 + this.worldObj.rand.nextInt(3));
					this.func_152115_b(player.getUniqueID().toString());
					this.playTameEffect(true);
					this.aiSit.setSitting(true);
					this.worldObj.setEntityState(this, (byte) 7);
				}
				else
				{
					this.playTameEffect(false);
					this.worldObj.setEntityState(this, (byte) 6);
				}
			}
			return true;
		}
		return super.interact(player);
	}
}
