package org.ultramine.mods.bukkit.mixin.enchantment;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnumEnchantmentType;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(Enchantment.class)
public class MixinEnchantment
{
	@Inject(method = "<init>", at = @At(value = "RETURN"))
	private void init(int p_i1926_1_, int p_i1926_2_, EnumEnchantmentType p_i1926_3_, CallbackInfo ci)
	{
		org.bukkit.enchantments.Enchantment.registerEnchantment(new org.bukkit.craftbukkit.enchantments.CraftEnchantment((Enchantment) (Object) this));
	}
}
