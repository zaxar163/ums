package org.ultramine.mods.bukkit.mixin.block;

import net.minecraft.block.BlockButton;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;
import org.bukkit.Bukkit;
import org.bukkit.block.Block;
import org.bukkit.event.block.BlockRedstoneEvent;
import org.bukkit.event.entity.EntityInteractEvent;
import org.objectweb.asm.Opcodes;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.At.Shift;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;
import org.ultramine.mods.bukkit.interfaces.entity.IMixinEntity;
import org.ultramine.mods.bukkit.interfaces.world.IMixinWorld;

import java.util.List;
import java.util.Random;

@Mixin(BlockButton.class)
public class MixinBlockButton
{
	@Inject(method = "func_150046_n", cancellable = true, at = @At(value = "INVOKE", target = "Ljava/util/List;isEmpty()Z", ordinal = 0, shift = Shift.BY, by = 11), locals = LocalCapture.CAPTURE_FAILEXCEPTION)
	private void func_150046_nInject(World world, int x, int y, int z, CallbackInfo ci, int l, int i1, boolean flag, List list, boolean flag1)
	{
		if (flag != flag1 && flag1)
		{
			Block block = ((IMixinWorld) world).getWorld().getBlockAt(x, y, z);
			boolean allowed = false;
			for (Object entityObject : list)
				if (entityObject != null)
				{
					EntityInteractEvent event = new EntityInteractEvent(((IMixinEntity) entityObject).getBukkitEntity(), block);
					Bukkit.getPluginManager().callEvent(event);
					if (!event.isCancelled())
					{
						allowed = true;
						break;
					}
				}
			if (!allowed)
				ci.cancel();
		}
	}

	@Inject(method = "onBlockActivated", at = @At(value = "INVOKE", target = "Lnet/minecraft/world/World;setBlockMetadataWithNotify(IIIII)Z"), locals = LocalCapture.CAPTURE_FAILEXCEPTION)
	private void onBlockActivatedInject(World world, int x, int y, int z, EntityPlayer player, int p_149727_6_, float p_149727_7_, float p_149727_8_, float p_149727_9_, CallbackInfoReturnable<Boolean> cir, int i1, int j1, int k1)
	{
		Block block = ((IMixinWorld) world).getWorld().getBlockAt(x, y, z);
		int old = (k1 != 8) ? 15 : 0;
		int current = (k1 == 8) ? 15 : 0;
		BlockRedstoneEvent eventRedstone = new BlockRedstoneEvent(block, old, current);
		Bukkit.getPluginManager().callEvent(eventRedstone);
		if ((eventRedstone.getNewCurrent() > 0) != (k1 == 8))
		{
			cir.setReturnValue(true);
			cir.cancel();
		}
	}

	@Inject(method = "updateTick", at = @At(value = "FIELD", opcode = Opcodes.GETFIELD, target = "Lnet/minecraft/block/BlockButton;field_150047_a:Z"))
	private void updateTickInject(World world, int x, int y, int z, Random random, CallbackInfo ci)
	{
		Block block = ((IMixinWorld) world).getWorld().getBlockAt(x, y, z);
		BlockRedstoneEvent eventRedstone = new BlockRedstoneEvent(block, 15, 0);
		Bukkit.getPluginManager().callEvent(eventRedstone);
		if (eventRedstone.getNewCurrent() > 0)
		{
			ci.cancel();
		}
	}
}
