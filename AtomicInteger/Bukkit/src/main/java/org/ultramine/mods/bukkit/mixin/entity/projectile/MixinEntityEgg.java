package org.ultramine.mods.bukkit.mixin.entity.projectile;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.entity.projectile.EntityEgg;
import net.minecraft.entity.projectile.EntityThrowable;
import net.minecraft.util.DamageSource;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;
import org.bukkit.Bukkit;
import org.bukkit.entity.Ageable;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.player.PlayerEggThrowEvent;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;
import org.ultramine.mods.bukkit.interfaces.entity.IMixinEntity;
import org.ultramine.mods.bukkit.interfaces.world.IMixinWorld;

@Mixin(EntityEgg.class)
public class MixinEntityEgg extends EntityThrowable
{
	public MixinEntityEgg(World p_i1776_1_)
	{
		super(p_i1776_1_);
	}

	@Overwrite
	protected void onImpact(MovingObjectPosition pos)
	{
		if (pos.entityHit != null)
			pos.entityHit.attackEntityFrom(DamageSource.causeThrownDamage(this, this.getThrower()), 0.0F);

		boolean hatching = !this.worldObj.isRemote && this.rand.nextInt(8) == 0;
		int numHatching = (this.rand.nextInt(32) == 0) ? 4 : 1;
		if (!hatching)
			numHatching = 0;
		EntityType hatchingType = EntityType.CHICKEN;
		Entity shooter = this.getThrower();
		if (shooter instanceof EntityPlayerMP)
		{
			Player player = (Player) ((IMixinEntity) shooter).getBukkitEntity();
			PlayerEggThrowEvent event = new PlayerEggThrowEvent(player, (org.bukkit.entity.Egg) ((IMixinEntity) this).getBukkitEntity(), hatching, (byte) numHatching, hatchingType);
			Bukkit.getPluginManager().callEvent(event);
			hatching = event.isHatching();
			numHatching = event.getNumHatches();
			hatchingType = event.getHatchingType();
		}
		if (hatching)
		{
			for (int k = 0; k < numHatching; k++)
			{
				org.bukkit.entity.Entity entity = ((IMixinWorld) worldObj).getWorld().spawn(new org.bukkit.Location(((IMixinWorld) worldObj).getWorld(), this.posX, this.posY, this.posZ, this.rotationYaw, 0.0F), hatchingType.getEntityClass(), CreatureSpawnEvent.SpawnReason.EGG);
				if (entity instanceof Ageable)
					((Ageable) entity).setBaby();
			}
		}
		for (int j = 0; j < 8; ++j)
			this.worldObj.spawnParticle("snowballpoof", this.posX, this.posY, this.posZ, 0.0D, 0.0D, 0.0D);

		if (!this.worldObj.isRemote)
			this.setDead();
	}
}
