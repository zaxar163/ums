package org.ultramine.mods.bukkit.mixin.block;

import net.minecraft.block.Block;
import net.minecraft.block.BlockNetherrack;
import net.minecraft.world.World;
import org.bukkit.Bukkit;
import org.bukkit.event.block.BlockRedstoneEvent;
import org.spongepowered.asm.mixin.Mixin;
import org.ultramine.mods.bukkit.interfaces.world.IMixinWorld;

@Mixin(BlockNetherrack.class)
public class MixinBlockNetherrack
{
	public void doPhysics(World world, int i, int j, int k, int l)
    {
	    if (Block.getBlockById(l) != null && Block.getBlockById(l).canProvidePower())
	    {
		    org.bukkit.block.Block block = ((IMixinWorld) world).getWorld().getBlockAt(i, j, k);
		    int power = block.getBlockPower();
		    BlockRedstoneEvent event = new BlockRedstoneEvent(block, power, power);
		    Bukkit.getPluginManager().callEvent(event);
	    }
    }
}
