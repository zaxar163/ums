package org.ultramine.mods.bukkit.mixin.inventory;

import net.minecraft.enchantment.EnchantmentData;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.init.Items;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.ContainerEnchantment;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.event.enchantment.EnchantItemEvent;
import org.bukkit.event.enchantment.PrepareItemEnchantEvent;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.Redirect;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;
import org.ultramine.mods.bukkit.interfaces.entity.IMixinEntity;
import org.ultramine.mods.bukkit.interfaces.inventory.IMixinContainer;
import org.ultramine.mods.bukkit.interfaces.world.IMixinWorld;

import java.util.List;
import java.util.Map;
import java.util.Random;

@Mixin(ContainerEnchantment.class)
public abstract class MixinContainerEnchantment extends Container
{
	@Shadow private World worldPointer;
	@Shadow private int posX;
	@Shadow private int posY;
	@Shadow private int posZ;
	@Shadow public long nameSeed;
	@Shadow public int[] enchantLevels = new int[3];
	@Shadow public IInventory tableInventory;
	@Shadow private Random rand;
	@Shadow public abstract void detectAndSendChanges();
	private Player player;

	@Inject(method = "<init>", at = @At(value = "RETURN"))
	private void init(InventoryPlayer p_i1811_1_, World p_i1811_2_, int p_i1811_3_, int p_i1811_4_, int p_i1811_5_, CallbackInfo ci)
	{
		player = (Player) ((IMixinEntity) p_i1811_1_.player).getBukkitEntity();
	}

	@Redirect(method = "onCraftMatrixChanged", at = @At(value = "INVOKE", target = "Lnet/minecraft/item/ItemStack;isItemEnchantable()Z"))
	private boolean onCraftMatrixChangedRedirect(ItemStack itemStack)
	{
		// if(itemstack != null && itemstack.isItemEnchantable()) --> if(itemstack != null)
		return true;
	}

	@Inject(method = "onCraftMatrixChanged", cancellable = true, at = @At(value = "INVOKE", target = "Lnet/minecraft/inventory/ContainerEnchantment;detectAndSendChanges()V"), locals = LocalCapture.CAPTURE_FAILEXCEPTION)
	private void onCraftMatrixChangedInject(IInventory p_75130_1_, CallbackInfo ci, ItemStack itemstack, int i)
	{
		if (((IMixinContainer) this).getBukkitView() != null)
		{
			CraftItemStack item = CraftItemStack.asCraftMirror(itemstack);
			PrepareItemEnchantEvent event = new PrepareItemEnchantEvent(player, ((IMixinContainer) this).getBukkitView(), ((IMixinWorld) this.worldPointer).getWorld().getBlockAt(this.posX, this.posY, this.posZ), item, this.enchantLevels, i);
			event.setCancelled(!itemstack.isItemEnchantable());
			Bukkit.getPluginManager().callEvent(event);
			if (event.isCancelled())
			{
				for (i = 0; i < 3; ++i)
				{
					this.enchantLevels[i] = 0;
				}
				ci.cancel();
			}
		}
	}

	@Overwrite
	public boolean enchantItem(EntityPlayer p_75140_1_, int p_75140_2_)
	{
		ItemStack itemstack = this.tableInventory.getStackInSlot(0);
		if (this.enchantLevels[p_75140_2_] <= 0 || itemstack == null || p_75140_1_.experienceLevel < this.enchantLevels[p_75140_2_] && !p_75140_1_.capabilities.isCreativeMode)
		{
			return false;
		}
		else
		{
			if (!this.worldPointer.isRemote)
			{
				List list = EnchantmentHelper.buildEnchantmentList(this.rand, itemstack, this.enchantLevels[p_75140_2_]);
				boolean flag = itemstack.getItem() == Items.book;
				if (list != null)
				{
					Map<org.bukkit.enchantments.Enchantment, Integer> enchants = new java.util.HashMap<org.bukkit.enchantments.Enchantment, Integer>();
					for (Object obj : list)
					{
						EnchantmentData instance = (EnchantmentData) obj;
						enchants.put(org.bukkit.enchantments.Enchantment.getById(instance.enchantmentobj.effectId), instance.enchantmentLevel);
					}

					CraftItemStack item = CraftItemStack.asCraftMirror(itemstack);
					EnchantItemEvent event = new EnchantItemEvent((Player) ((IMixinEntity) p_75140_1_).getBukkitEntity(), ((IMixinContainer) this).getBukkitView(), ((IMixinWorld) this.worldPointer).getWorld().getBlockAt(this.posX, this.posY, this.posZ), item, this.enchantLevels[p_75140_2_], enchants, p_75140_2_);
					if (((IMixinContainer) this).getBukkitView() != null)
						Bukkit.getPluginManager().callEvent(event); // Cauldron - allow vanilla mods to bypass
					int level = event.getExpLevelCost();

					if (event.isCancelled() || (level > p_75140_1_.experienceLevel && !p_75140_1_.capabilities.isCreativeMode) || enchants.isEmpty())
					{
						return false;
					}
					boolean applied = !flag;
					for (Map.Entry<org.bukkit.enchantments.Enchantment, Integer> entry : event.getEnchantsToAdd().entrySet())
					{
						try
						{
							if (flag)
							{
								int enchantId = entry.getKey().getId();
								if (net.minecraft.enchantment.Enchantment.enchantmentsList[enchantId] == null)
								{
									continue;
								}
								EnchantmentData enchantment = new EnchantmentData(enchantId, entry.getValue());
								Items.enchanted_book.addEnchantment(itemstack, enchantment);
								applied = true;
								itemstack.func_150996_a(Items.enchanted_book);
								break;
							}
							else
							{
								item.addEnchantment(entry.getKey(), entry.getValue());
							}
						}
						catch (IllegalArgumentException e)
						{
							/* Just swallow invalid enchantments */
						}
					}
					// Only down level if we've applied the enchantments
					if (applied)
					{
						p_75140_1_.addExperienceLevel(-level);
					}
					this.onCraftMatrixChanged(this.tableInventory);
				}
			}
			return true;
		}
	}

	@Inject(method = "canInteractWith", cancellable = true, at = @At(value = "HEAD"))
	private void canInteractWith(EntityPlayer p_75145_1_, CallbackInfoReturnable<Boolean> cir)
	{
		if (!((IMixinContainer) this).checkReachable())
		{
			cir.setReturnValue(true);
			cir.cancel();
		}
	}
}
