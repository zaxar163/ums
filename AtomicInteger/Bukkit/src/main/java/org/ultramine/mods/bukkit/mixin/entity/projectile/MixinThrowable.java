package org.ultramine.mods.bukkit.mixin.entity.projectile;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.world.World;
import org.bukkit.craftbukkit.event.CraftEventFactory;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.ultramine.mods.bukkit.interfaces.entity.projectile.IMixinThrowable;

@Mixin(net.minecraft.entity.projectile.EntityThrowable.class)
public abstract class MixinThrowable extends Entity implements IMixinThrowable
{
	@Shadow
	private EntityLivingBase thrower;
	@Shadow
	private String throwerName;

	public MixinThrowable(World p_i1582_1_)
	{
		super(p_i1582_1_);
	}

	@Override
	public EntityLivingBase getThrower()
	{
		return thrower;
	}

	@Override
	public void setThrower(EntityLivingBase thrower)
	{
		this.thrower = thrower;
	}

	@Override
	public String getThrowerName()
	{
		return throwerName;
	}

	@Override
	public void setThrowerName(String throwerName)
	{
		this.throwerName = throwerName;
	}

	@Inject(method = "onUpdate", at = @At(value = "INVOKE", target = "Lnet/minecraft/entity/projectile/EntityThrowable;onImpact(Lnet/minecraft/util/MovingObjectPosition;)V", shift = At.Shift.AFTER))
	private void onUpdateInject(CallbackInfo ci)
	{
		if (this.isDead)
			CraftEventFactory.callProjectileHitEvent(this);
	}
}
