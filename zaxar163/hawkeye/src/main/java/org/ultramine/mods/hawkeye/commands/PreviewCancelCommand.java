package org.ultramine.mods.hawkeye.commands;

import org.ultramine.mods.hawkeye.Undo;
import org.ultramine.mods.hawkeye.Rollback.RollbackType;
import org.ultramine.mods.hawkeye.util.HawkPermission;
import org.ultramine.mods.hawkeye.util.HawkUtil;

/**
 * Cancels a rollback preview. Error handling for user input is done using
 * exceptions to keep code neat.
 * 
 * @author oliverw92
 */
public class PreviewCancelCommand extends BaseCommand
{
	public PreviewCancelCommand()
	{
		name = "preview cancel";
		argLength = 0;
		usage = "<- cancel rollback preview";
	}

	@Override
	public boolean execute()
	{
		//Check if player already has a rollback processing
		if(!session.isInPreview())
		{
			HawkUtil.sendMessage(sender, "&cNo preview to cancel!");
			return true;
		}

		//Undo local changes to the player
		new Undo(RollbackType.LOCAL, session);

		HawkUtil.sendMessage(sender, "&cPreview rollback cancelled");
		session.setInPreview(false);
		return true;
	}

	@Override
	public void moreHelp()
	{
		HawkUtil.sendMessage(sender, "&cCancels results of a &7/hawk preview");
		HawkUtil.sendMessage(sender, "&cOnly affects you - no changes are seen by anyony else");
	}

	@Override
	public boolean permission()
	{
		return HawkPermission.preview(sender);
	}
}
