package org.ultramine.mods.hawkeye;

import java.util.List;
import java.util.Map;

public class HawkConfig
{
	public Map<String, Boolean> log;
	public HawkGeneral general;
	public HawkDatabase database;
	public List<String> commandFilter;
	public List<String> ignoreWorlds;
	public List<String> blockFilter;
	
	public static class HawkGeneral
	{
		public int maxLines = 0;
		public int maxRadius;
		public int defaultHereRadius;
		public String toolBlock;
		public String[] defaultToolCommand;
		public String cleanseAge;
		public String cleansePeriod;
		public boolean giveTool;
		public boolean debug;
		public boolean logIpAddresses;
		public boolean deleteDataOnRollback;
		public boolean logDeathDrops;
		public boolean logBlocksSubData;
	}
	
	public static class HawkDatabase
	{
		public String database;
		public String dataTable;
		public String playerTable;
		public String subdataTable;
	}
	
	public boolean isLogged(DataType dataType)
	{
		Boolean bool = log.get(dataType.getConfigName());
		return bool != null && bool.booleanValue();
	}
}
