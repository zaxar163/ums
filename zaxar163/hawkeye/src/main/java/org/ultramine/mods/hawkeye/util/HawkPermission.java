package org.ultramine.mods.hawkeye.util;

import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;

import org.ultramine.mods.hawkeye.HawkEye;
import org.ultramine.server.PermissionHandler;

/**
 * Permissions handler for HawkEye Supports multiple permissions systems
 * 
 * @author oliverw92
 */
public class HawkPermission
{
	/**
	 * Check permissions plugins, deciding which one to use
	 * 
	 * @param instance
	 */
	public HawkPermission(HawkEye instance)
	{
	}

	/**
	 * Private method for checking a users permission level. Permission checks
	 * from other classes should go through a separate method for each node.
	 * 
	 * @param sender
	 * @param node
	 * @return true if the user has permission, false if not
	 */
	private static boolean hasPermission(ICommandSender sender, String node)
	{
		if(!(sender instanceof EntityPlayerMP))
			return true;
		return PermissionHandler.getInstance().has(sender, node);
	}

	/**
	 * Permission to view different pages
	 * 
	 * @param player
	 * @return
	 */
	public static boolean page(ICommandSender player)
	{
		return hasPermission(player, "hawkeye.page");
	}

	/**
	 * Permission to search the logs
	 * 
	 * @param player
	 * @return
	 */
	public static boolean search(ICommandSender player)
	{
		return hasPermission(player, "hawkeye.search");
	}

	/**
	 * Permission to search a specific data type
	 * 
	 * @param player
	 * @return
	 */
	public static boolean searchType(ICommandSender player, String type)
	{
		return hasPermission(player, "hawkeye.search." + type.toLowerCase());
	}

	/**
	 * Permission to teleport to the location of a result
	 * 
	 * @param player
	 * @return
	 */
	public static boolean tpTo(ICommandSender player)
	{
		return hasPermission(player, "hawkeye.tpto");
	}

	/**
	 * Permission to use the rollback command
	 * 
	 * @param player
	 * @return
	 */
	public static boolean rollback(ICommandSender player)
	{
		return hasPermission(player, "hawkeye.rollback");
	}

	/**
	 * Permission to the hawkeye tool
	 * 
	 * @param player
	 * @return
	 */
	public static boolean tool(ICommandSender player)
	{
		return hasPermission(player, "hawkeye.tool");
	}

	/**
	 * Permission to be notified of rule breaks
	 * 
	 * @param player
	 * @return
	 */
	public static boolean notify(ICommandSender player)
	{
		return hasPermission(player, "hawkeye.notify");
	}

	/**
	 * Permission to preview rollbacks
	 * 
	 * @param player
	 * @return
	 */
	public static boolean preview(ICommandSender player)
	{
		return hasPermission(player, "hawkeye.preview");
	}

	/**
	 * Permission to bind a tool
	 * 
	 * @param player
	 * @return
	 */
	public static boolean toolBind(ICommandSender player)
	{
		return hasPermission(player, "hawkeye.tool.bind");
	}

	/**
	 * Permission to rebuild
	 * 
	 * @param player
	 * @return
	 */
	public static boolean rebuild(ICommandSender player)
	{
		return hasPermission(player, "hawkeye.rebuild");
	}

	/**
	 * Permission to delete entires
	 * 
	 * @param player
	 * @return
	 */
	public static boolean delete(ICommandSender player)
	{
		return hasPermission(player, "hawkeye.delete");
	}
}
