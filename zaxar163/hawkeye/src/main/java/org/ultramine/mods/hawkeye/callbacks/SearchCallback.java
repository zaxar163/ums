package org.ultramine.mods.hawkeye.callbacks;

import net.minecraft.command.ICommandSender;

import org.ultramine.mods.hawkeye.DisplayManager;
import org.ultramine.mods.hawkeye.PlayerSession;
import org.ultramine.mods.hawkeye.database.SearchQuery.SearchError;
import org.ultramine.mods.hawkeye.util.HawkUtil;

/**
 * Implementation of BaseCallback for use in search commands
 * 
 * @author oliverw92
 */
public class SearchCallback extends BaseCallback
{
	private final PlayerSession session;
	private final ICommandSender sender;
	private final boolean isOnePoint;

	public SearchCallback(PlayerSession session, boolean isOnePoint)
	{
		this.session = session;
		sender = session.getSender();
		this.isOnePoint = isOnePoint;
		HawkUtil.sendMessage(sender, "&cSearching for matching results...");
	}
	
	public SearchCallback(PlayerSession session)
	{
		this(session, false);
	}

	@Override
	public void execute()
	{
		session.setSearchResults(results, isOnePoint);
		DisplayManager.displayPage(session, 1);
	}

	@Override
	public void error(SearchError error, String message)
	{
		HawkUtil.sendMessage(sender, message);
	}
}
