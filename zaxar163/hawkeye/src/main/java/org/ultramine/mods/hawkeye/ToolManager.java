package org.ultramine.mods.hawkeye;

import java.util.Arrays;
import java.util.List;

import org.ultramine.mods.hawkeye.callbacks.SearchCallback;
import org.ultramine.mods.hawkeye.database.SearchQuery;
import org.ultramine.mods.hawkeye.database.SearchQuery.SearchDir;
import org.ultramine.mods.hawkeye.util.HawkBlockUtil;
import org.ultramine.mods.hawkeye.util.HawkInventoryUtil;
import org.ultramine.mods.hawkeye.util.HawkUtil;
import org.ultramine.mods.hawkeye.util.HawkVector;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.item.ItemStack;

/**
 * Contains methods for controlling the HawkEye tool
 * 
 * @author oliverw92
 */
public class ToolManager
{
	/**
	 * Enables the HawkEye tool
	 * 
	 * @param session
	 * @param player
	 */
	public static void enableTool(PlayerSession session, EntityPlayer player)
	{
		InventoryPlayer inv = player.inventory;
		session.setUsingTool(true);
		ItemStack stack = HawkBlockUtil.itemStringToStack(HawkEye.instance.config.general.toolBlock, 1);

		//If player doesn't have a tool, give them one if enabled in config
		if(!HawkInventoryUtil.contains(inv, stack) && HawkEye.instance.config.general.giveTool)
		{
			int first = HawkInventoryUtil.firstEmpty(inv);
			if(first != -1)
			{
				inv.setInventorySlotContents(first, stack);
			}
		}

		//If they aren't holding a tool, move the tool to their hand
		int first = HawkInventoryUtil.first(inv, HawkBlockUtil.itemStringToStack(HawkEye.instance.config.general.toolBlock, 1));
		if(!HawkBlockUtil.getItemString(inv.getCurrentItem()).equals(HawkEye.instance.config.general.toolBlock) && first != -1)
		{
			ItemStack back = inv.getCurrentItem().copy();
			inv.setInventorySlotContents(inv.currentItem, inv.getStackInSlot(first));
			if(back.stackSize == 0)
				inv.setInventorySlotContents(first, null);
			else
				inv.setInventorySlotContents(first, back);
		}

		HawkUtil.sendMessage(player, "&cHawkEye tool enabled! &7Left click a block or place the tool to get information");
	}

	/**
	 * Disables the HawkEye tool
	 * 
	 * @param session
	 * @param player
	 */
	public static void disableTool(PlayerSession session, EntityPlayer player)
	{
		session.setUsingTool(false);
		HawkUtil.sendMessage(player, "&cHawkEye tool disabled");
	}

	/**
	 * Performs a HawkEye tool search at the specified location
	 * 
	 * @param player
	 * @param loc
	 */
	public static void toolSearch(EntityPlayer player, HawkVector loc)
	{
		PlayerSession session = SessionManager.getSession(player);
		SearchParser parser;

		//If parameters aren't bound, do some default
		if(session.getToolCommand().length == 0 || session.getToolCommand()[0] == "")
		{
			parser = new SearchParser(player);
			for(DataType type : DataType.values())
				if(type.canHere())
					parser.actions.add(type);
		}
		//Else use the default ones
		else
		{
			parser = new SearchParser(player, Arrays.asList(session.getToolCommand()));
		}

		HawkVector vec = HawkUtil.getSimpleLocation(loc);

		parser.loc = vec;
		parser.minLoc = null;
		parser.maxLoc = null;
		parser.worlds = new String[] {Integer.toString(player.dimension)};
		new SearchQuery(new SearchCallback(SessionManager.getSession(player), true), parser, SearchDir.DESC);
	}

	/**
	 * Binds arguments to the HawkEye tool
	 * 
	 * @param player
	 *            player issueing the command
	 * @param session
	 *            session to save args to
	 * @param args
	 *            parameters
	 */
	public static void bindTool(EntityPlayer player, PlayerSession session, List<String> args)
	{
		try
		{
			new SearchParser(player, args);
		}
		catch(IllegalArgumentException e)
		{
			HawkUtil.sendMessage(player, "&c" + e.getMessage());
			return;
		}

		HawkUtil.sendMessage(player, "&cParameters bound to tool: &7" + HawkUtil.join(args, " "));
		session.setToolCommand(args.toArray(new String[0]));
		if(!session.isUsingTool())
			enableTool(session, player);

	}

	/**
	 * Reset tool to default parameters
	 * 
	 * @param session
	 */
	public static void resetTool(PlayerSession session)
	{
		session.setToolCommand(HawkEye.instance.config.general.defaultToolCommand);
	}
}
