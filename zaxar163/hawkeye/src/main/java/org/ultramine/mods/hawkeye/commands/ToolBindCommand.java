package org.ultramine.mods.hawkeye.commands;

import org.ultramine.mods.hawkeye.ToolManager;
import org.ultramine.mods.hawkeye.util.HawkPermission;
import org.ultramine.mods.hawkeye.util.HawkUtil;

public class ToolBindCommand extends BaseCommand
{
	public ToolBindCommand()
	{
		name = "tool bind";
		argLength = 1;
		usage = " <- bind custom parameters to the tool";
	}

	@Override
	public boolean execute()
	{
		ToolManager.bindTool(player, session, args);
		return true;
	}

	@Override
	public void moreHelp()
	{
		HawkUtil.sendMessage(sender, "&cAllows you to bind custom search parameters onto the tool");
		HawkUtil.sendMessage(sender, "&cSee &7/hawk search help for info on parameters");
	}

	@Override
	public boolean permission()
	{
		return HawkPermission.toolBind(sender);
	}
}