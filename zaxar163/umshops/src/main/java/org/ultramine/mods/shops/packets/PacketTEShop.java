package org.ultramine.mods.shops.packets;

import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketBuffer;
import org.ultramine.network.TEPacket;

import java.io.IOException;

public class PacketTEShop extends TEPacket
{
	public ItemStack item;
	public byte mode;
	public double sellPrice;
	public double buyPrice;
	public double maxSellPrice;
	public double maxBuyPrice;
	public int sellAmount;
	public int buyAmount;
	public int productAmount;
	public int places;
	
	public String owner;
	
	public PacketTEShop(){}
	public PacketTEShop(ItemStack item, byte mode, double sellPrice, double buyPrice, double maxSellPrice, double maxBuyPrice,
			int sellCount, int buyCount, int productCount, int places, String owner)
	{
		this.item = item;
		this.mode = mode;
		this.sellPrice = sellPrice;
		this.buyPrice = buyPrice;
		this.maxSellPrice = maxSellPrice;
		this.maxBuyPrice = maxBuyPrice;
		this.sellAmount = sellCount;
		this.buyAmount =  buyCount;
		this.productAmount = productCount;
		this.places = places;
		this.owner = owner;
	}

	@Override
	public void readPacketData(PacketBuffer buf) throws IOException
	{
		item = buf.readItemStackFromBuffer();
		mode = buf.readByte();
		sellPrice = buf.readDouble();
		buyPrice = buf.readDouble();
		maxSellPrice = buf.readDouble();
		maxBuyPrice = buf.readDouble();
		sellAmount = buf.readInt();
		buyAmount = buf.readInt();
		productAmount = buf.readInt();
		places = buf.readInt();
		owner = buf.readStringFromBuffer(16);
	}

	@Override
	public void writePacketData(PacketBuffer buf) throws IOException
	{
		buf.writeItemStackToBuffer(item);
		buf.writeByte(mode);
		buf.writeDouble(sellPrice);
		buf.writeDouble(buyPrice);
		buf.writeDouble(maxSellPrice);
		buf.writeDouble(maxBuyPrice);
		buf.writeInt(sellAmount);
		buf.writeInt(buyAmount);
		buf.writeInt(productAmount);
		buf.writeInt(places);
		buf.writeStringToBuffer(owner);
	}
}
