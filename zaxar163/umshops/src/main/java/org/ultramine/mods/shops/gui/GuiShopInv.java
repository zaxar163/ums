package org.ultramine.mods.shops.gui;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.inventory.IInventory;
import net.minecraft.util.ResourceLocation;
import org.ultramine.gui.ElementButton;
import org.ultramine.gui.GuiContainer;
import org.ultramine.gui.IGuiElement;
import org.ultramine.mods.shops.te.TileEntityShop;
import org.ultramine.util.ContainerChest;

import static org.ultramine.util.I18n.tlt;

@SideOnly(Side.CLIENT)
public class GuiShopInv extends GuiContainer
{
	private static final ResourceLocation texture = new ResourceLocation("textures/gui/container/generic_54.png");
	
	private IInventory player;
	private TileEntityShop shop;
	
	public GuiShopInv(IInventory player, TileEntityShop shop)
	{
		super(new ContainerChest(player, shop.inventory));
		this.player = player;
		this.shop = shop;
		
		this.setBG(texture);
		this.setSize(176, 222);
		
		addElement(new ElementButton(0, 164, 4, 8, 10, "X"));
		addElement(new ElementButton(1, 132, 4, 30, 10, tlt("ultramine.shop.gui.back")));
	}
	
	@Override
	public void actionPerformed(int id, IGuiElement el, Object... data)
	{
		switch(id)
		{
		case 0:
			Minecraft.getMinecraft().thePlayer.closeScreen();
			return;
			
		case 1:
			shop.sendGuiCommand(2);
			return;
		}
	}
	
	@Override
	protected void drawForeground(int mx, int my)
	{
		super.drawForeground(mx, my);
		mc.fontRenderer.drawString(tlt("ultramine.shop.guiinv.name"), guiLeft + 60, guiTop + 6, 0x404040);
		mc.fontRenderer.drawString(tlt(player.getInventoryName()), guiLeft + 8, guiTop + (ySize - 96) + 2, 0x404040);
	}
}
