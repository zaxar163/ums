package org.ultramine.mods.shops;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.registry.GameRegistry;
import org.ultramine.mods.shops.blocks.BlockIndicator;
import org.ultramine.mods.shops.blocks.BlockShop;
import org.ultramine.mods.shops.items.ItemBlockShop;
import org.ultramine.mods.shops.packets.PacketPlayerMoney;
import org.ultramine.mods.shops.packets.PacketShopMsg;
import org.ultramine.mods.shops.packets.PacketTEAdminShop;
import org.ultramine.mods.shops.packets.PacketTEShop;
import org.ultramine.mods.shops.packets.UMPacketOpenGui;
import org.ultramine.mods.shops.te.TileEntityAdminShop;
import org.ultramine.mods.shops.te.TileEntityIndicator;
import org.ultramine.mods.shops.te.TileEntityShop;
import org.ultramine.network.UMNetworkRegistry;
import org.ultramine.util.GuiHandler;

public abstract class InitCommon
{
	void initCommon()
	{
		FMLCommonHandler.instance().bus().register(this);
		NetworkRegistry.INSTANCE.registerGuiHandler(UMShops.instance(), new GuiHandler());
		
		GameRegistry.registerBlock(new BlockShop(), ItemBlockShop.class, "shop");
		GameRegistry.registerBlock(new BlockIndicator(), "indicator");
		
		GameRegistry.registerTileEntity(TileEntityShop.class, "um_shop");
		GameRegistry.registerTileEntity(TileEntityAdminShop.class, "um_shop_adm");
		GameRegistry.registerTileEntity(TileEntityIndicator.class, "um_indicator");
		
		UMNetworkRegistry.registerPacket(PacketPlayerMoney.class);
		UMNetworkRegistry.registerPacket(PacketShopMsg.class);
		UMNetworkRegistry.registerPacket(PacketTEShop.class);
		UMNetworkRegistry.registerPacket(PacketTEAdminShop.class);
		UMNetworkRegistry.registerPacket(UMPacketOpenGui.class);
	}
	
	abstract void initSided();
}
