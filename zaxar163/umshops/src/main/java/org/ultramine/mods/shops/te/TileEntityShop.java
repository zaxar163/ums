package org.ultramine.mods.shops.te;

import com.mojang.authlib.GameProfile;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.Packet;
import net.minecraft.server.MinecraftServer;
import net.minecraft.tileentity.TileEntity;
import org.ultramine.core.economy.Currency;
import org.ultramine.core.economy.account.Account;
import org.ultramine.core.economy.holdings.Holdings;
import org.ultramine.core.economy.service.DefaultCurrencyService;
import org.ultramine.core.economy.service.Economy;
import org.ultramine.core.service.InjectService;
import org.ultramine.gui.IGui;
import org.ultramine.mods.shops.UMShops;
import org.ultramine.mods.shops.economy.ClientEconomy;
import org.ultramine.mods.shops.gui.ContainerVetrino;
import org.ultramine.mods.shops.gui.GuiShopBuy;
import org.ultramine.mods.shops.gui.GuiShopInv;
import org.ultramine.mods.shops.gui.GuiShopMain;
import org.ultramine.mods.shops.gui.GuiShopSet;
import org.ultramine.mods.shops.packets.PacketShopMsg;
import org.ultramine.mods.shops.packets.PacketTEShop;
import org.ultramine.mods.shops.packets.UMPacketOpenGui;
import org.ultramine.network.ITEPacketHandler;
import org.ultramine.server.util.InventoryUtil;
import org.ultramine.util.ContainerChest;
import org.ultramine.util.IHasGui;
import org.ultramine.util.SimpleInventory;

import java.util.UUID;

public class TileEntityShop extends TileEntity implements IHasGui, ITEPacketHandler<PacketTEShop>
{
	private static final int GUI_MAIN_ID	= 0;
	private static final int GUI_SET_ID		= 1;
	private static final int GUI_INV_ID		= 2;
	private static final int GUI_BAY_ID		= 3;
	private static final int GUI_RENT_ID	= 4;

	@InjectService private static Economy economy;
	@InjectService private static DefaultCurrencyService defaultCurrency;

	protected GameProfile owner = new GameProfile(new UUID(0, 0), "_NONE");
	public byte mode;
	public double sellPrice = 0;
	public double buyPrice = 0;
	public double maxSellPrice = 0;
	public double maxBuyPrice = 0;
	public int sellAmount = -1;
	public int buyAmount = -1;
	
	protected ItemStack product;
	public final SimpleInventory inventory = new SimpleInventory(54)
	{
		@Override
		public void markDirty()
		{
			TileEntityShop.this.markDirty();
		}
		
		@Override
		public boolean isUseableByPlayer(EntityPlayer player)
		{
			return TileEntityShop.this.isUseableByPlayer(player);
		}
		
		@Override
		public String getInventoryName()
		{
			return "ultramine.shop.container.shop";
		}
	};
	
	public int productAmount = 0;
	public int places = 0;
	
	public GameProfile getOwner()
	{
		return owner;
	}
	public void setOwner(GameProfile owner)
	{
		this.owner = owner;
	}
	
	public ItemStack getProduct()
	{
		return product;
	}
	
	public IInventory getProductInventory()
	{
		return new ProductInventoryWrapper();
	}
	
	public boolean isAdminShop()
	{
		return false;
	}

	public boolean isUseableByPlayer(EntityPlayer entityplayer)
	{
		if (worldObj.getTileEntity(xCoord, yCoord, zCoord) != this)
		{
			return false;
		}

		return entityplayer.getDistanceSq(xCoord + 0.5D, yCoord + 0.5D, zCoord + 0.5D) <= 64D;
	}
	
	@SideOnly(Side.SERVER)
	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		super.readFromNBT(nbt);
		NBTTagCompound nbttagcompound2 = (NBTTagCompound)nbt.getTag("Item");
		if(nbttagcompound2 != null)
			product = ItemStack.loadItemStackFromNBT(nbttagcompound2);
		
		inventory.readFromNBT(nbt);
		
		mode = nbt.getByte("m");
		String name = nbt.getString("on");
		owner = new GameProfile(new UUID(nbt.getLong("u1"), nbt.getLong("u2")), name.isEmpty() ? "_NONE" : name);
		sellPrice = nbt.getDouble("sp");
		buyPrice = nbt.getDouble("bp");
		maxSellPrice = nbt.getDouble("msp");
		maxBuyPrice = nbt.getDouble("mbp");
		sellAmount = nbt.getInteger("sc");
		buyAmount = nbt.getInteger("bc");
		
		updatePlaces(false);
	}

	@SideOnly(Side.SERVER)
	@Override
	public void writeToNBT(NBTTagCompound nbt)
	{
		super.writeToNBT(nbt);
		if(product != null)
		{
			NBTTagCompound nbttagcompound1 = new NBTTagCompound();
			product.writeToNBT(nbttagcompound1);
			nbt.setTag("Item", nbttagcompound1);
		}
		
		inventory.writeToNBT(nbt);
		
		nbt.setByte("m", mode);
		nbt.setString("on", owner.getName());
		nbt.setLong("u1", owner.getId().getMostSignificantBits());
		nbt.setLong("u2", owner.getId().getLeastSignificantBits());
		
		nbt.setDouble("sp", sellPrice);
		nbt.setDouble("bp", buyPrice);
		nbt.setDouble("msp", maxSellPrice);
		nbt.setDouble("mbp", maxBuyPrice);
		nbt.setInteger("sc", sellAmount);
		nbt.setInteger("bc", buyAmount);
	}
	
	@SideOnly(Side.SERVER)
	@Override
	public Packet getDescriptionPacket()
	{
		return new PacketTEShop(product, mode, sellPrice, buyPrice, maxSellPrice, maxBuyPrice, sellAmount, buyAmount, productAmount, places, owner.getName())
			.form(this).toServerPacket();
	}

	@Override
	public boolean canUpdate() {
		return false;
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void handlePacketClient(PacketTEShop p)
	{
		product			= p.item;
		mode			= p.mode;
		sellPrice		= p.sellPrice;
		buyPrice		= p.buyPrice;
		maxSellPrice	= p.maxSellPrice;
		maxBuyPrice		= p.maxBuyPrice;
		sellAmount		= p.sellAmount;
		buyAmount		= p.buyAmount;
		productAmount	= p.productAmount;
		places			= p.places;
		owner			= new GameProfile(null, p.owner);
	}
	
	@SideOnly(Side.SERVER)
	@Override // Server handled client command
	public void handlePacketServer(PacketTEShop p, EntityPlayerMP player)
	{
		boolean isOwner = player.getGameProfile().getId().equals(owner.getId()) ||
				MinecraftServer.getServer().getConfigurationManager().func_152596_g(player.getGameProfile());
		
		switch(p.places)
		{
		case 0:
			if(isOwner)
				openGuiWithoutContainer(player, GUI_SET_ID);
			return;
		case 1:
			if(isOwner)
				openGUI(player, GUI_INV_ID);
			return;
		case 2:
			if(isOwner)
				openGUI(player, GUI_MAIN_ID);
			return;
		case 3:
			openGuiWithoutContainer(player, GUI_BAY_ID);
			return;
		case 4:
			if(!isOwner)
				return;
			this.mode = p.mode;
			this.sellPrice = p.sellPrice;
			this.buyPrice = p.buyPrice;
			this.maxSellPrice = p.maxSellPrice;
			this.maxBuyPrice = p.maxBuyPrice;
			this.sellAmount = p.sellAmount;
			this.buyAmount = p.buyAmount;
			this.updatePlaces(true);
			return;
		case 5:
			doSell(player, p.productAmount);
			return;
		case 6:
			doBuy(player, p.productAmount);
			return;
		case 7:
			if(isOwner)
				;//openGuiWithoutContainer(player, GUI_RENT_ID);
		case 9:
			if(isOwner)
				worldObj.func_147480_a(xCoord, yCoord, zCoord, true);
			return;
		}
	}
	
	@SideOnly(Side.CLIENT)
	public boolean activateClient(EntityPlayer player)
	{
		return true;
	}
	
	@SideOnly(Side.SERVER)
	public boolean activateServer(EntityPlayer player)
	{
		boolean isOwner = player.getGameProfile().getId().equals(owner.getId()) ||
				MinecraftServer.getServer().getConfigurationManager().func_152596_g(player.getGameProfile());
		
		if(isOwner)
		{
			player.openGui(UMShops.instance(), GUI_MAIN_ID, worldObj, xCoord, yCoord, zCoord);
		}
		else
		{
			openGuiWithoutContainer(player, GUI_BAY_ID);
		}

		return true;
	}
	
	@SideOnly(Side.SERVER)
	@Override
	public Container getGuiContainer(int id, EntityPlayer player)
	{
		switch(id)
		{
		case GUI_MAIN_ID:	return new ContainerVetrino(player.inventory, new ProductInventoryWrapper(), 0);
		case GUI_INV_ID:	return new ContainerChest(player.inventory, this.inventory);
		}
		return null;
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public IGui getGui(int id, EntityPlayer player)
	{
		switch(id)
		{
		case GUI_SET_ID:	return new GuiShopSet(this);
		case GUI_INV_ID:	return new GuiShopInv(player.inventory, this);
		case GUI_MAIN_ID:	return new GuiShopMain(player.inventory, this);
		case GUI_BAY_ID:	return new GuiShopBuy(this, player);
		}
		return null;
	}
	
	@SideOnly(Side.SERVER)
	@Override
	public void markDirty()
	{
		super.markDirty();
		updatePlaces(true);
	}
	
	@SideOnly(Side.CLIENT)
	public void sendGuiCommand(int cmd)
	{
		PacketTEShop p = new PacketTEShop();
		p.places = cmd;
		p.owner = "";
		p.form(this).sendToServer();
	}
	
	@SideOnly(Side.CLIENT)
	public void sendBuySellCommand(int cmd, int par1)
	{
		PacketTEShop p = new PacketTEShop();
		p.places = cmd;
		p.productAmount = par1;
		p.owner = "";
		p.form(this).sendToServer();
	}
	
	@SideOnly(Side.CLIENT)
	public void sendNewShopDataToServer()
	{
		PacketTEShop p = new PacketTEShop();
		p.places = 4;
		p.mode = mode;
		p.sellPrice = sellPrice;
		p.buyPrice = buyPrice;
		p.maxSellPrice = maxSellPrice;
		p.maxBuyPrice = maxBuyPrice;
		p.sellAmount = sellAmount;
		p.buyAmount = buyAmount;
		p.productAmount = productAmount;//For admin shop
		p.owner = "";
		p.form(this).sendToServer();
	}
	
	@SideOnly(Side.SERVER)
	public void updatePlaces(boolean notify)
	{
		if(worldObj != null && worldObj.isRemote) return;
		
		int oldprodamount = productAmount;
		int oldplaces = places;
		productAmount = 0;
		places = 0;
		if(product == null)
			return;
		
		for(ItemStack is : inventory.getContents())
		{
			if(is == null)
			{
				places += product.getMaxStackSize();
			}
			else if(InventoryUtil.isStacksEquals(is, product))
			{
				places += is.getMaxStackSize()-is.stackSize;
				productAmount += is.stackSize;
			}
		}
		if(notify && (oldprodamount != productAmount || oldplaces != places))
			sendTileEntityPacket();
	}
	
	private void msg(EntityPlayerMP player, int id)
	{
		new PacketShopMsg(id).sendTo(player);
	}
	
	public int getMaxAmountForSell()
	{
		return mode == 0 && sellAmount == -1 ? productAmount : mode == 0 ? Math.min(productAmount, sellAmount) : productAmount;
	}
	
	public int getMaxAmountForBuy()
	{
		return mode == 0 ? (buyAmount == -1 ? places : buyAmount) : Math.max(0, sellAmount - productAmount);
	}
	
	public boolean canSell(int amount)
	{
		return sellPrice != 0 && (mode != 2 || maxSellPrice != 0) && productAmount >= amount && (mode != 0 || amount <= sellAmount || sellAmount == -1);
	}
	
	public double getSellCost(int amount)
	{
		if(mode != 2)
			return sellPrice*amount;
		
		int min1 = Math.min(productAmount-1, sellAmount);
		int min2 = Math.min(productAmount-amount, sellAmount);
		return (sellPrice*2 + (maxSellPrice-sellPrice)*((sellAmount-min1)/(double)sellAmount + (sellAmount-min2)/(double)sellAmount))*amount/2d;
	}
	
	public double getCurrentSellPrice()
	{
		if(mode != 2)
			return sellPrice;
		
		int min = Math.min(productAmount-1, sellAmount);
		return sellPrice + (maxSellPrice-sellPrice)*((sellAmount-min)/(double)sellAmount);
	}
	
	public boolean canBuy(int amount)
	{
		return buyPrice != 0 && (mode != 2 || maxBuyPrice != 0) && places >= amount && (mode == 0 && (amount <= buyAmount || buyAmount == -1) || amount <= sellAmount - productAmount);
	}
	
	public double getBuyCost(int amount)
	{
		if(mode != 2)
			return buyPrice*amount;
		
		int min1 = Math.min(productAmount, sellAmount);
		int min2 = Math.min(productAmount+amount-1, sellAmount);
		return (buyPrice*2 + (maxBuyPrice-buyPrice)*((sellAmount-min1)/(double)sellAmount + (sellAmount-min2)/(double)sellAmount))*amount/2d;
	}
	
	public double getCurrentBuyPrice()
	{
		if(mode != 2)
			return buyPrice;
		
		int min = Math.min(productAmount, sellAmount);
		return buyPrice + (maxBuyPrice-buyPrice)*((sellAmount-min)/(double)sellAmount);
	}
	
	private void onPostSell(int amount)
	{
		if(mode == 0 && sellAmount != -1)
			sellAmount -= amount;
	}
	
	private void onPostBuy(int amount)
	{
		if(mode == 0 && buyAmount != -1)
			buyAmount -= amount;
	}
	
	@SideOnly(Side.CLIENT)
	public double getClientMoney()
	{
		return ClientEconomy.GSC.getMoney();
	}
	
	@SideOnly(Side.CLIENT)
	public String getClientMoneySign()
	{
		return "$";
	}

	@SideOnly(Side.SERVER)
	protected Currency getCurrency()
	{
		return defaultCurrency.getDefaultCurrency();
	}
	
	@SideOnly(Side.SERVER)
	protected boolean ownerHasMoney(double amount)
	{
		Account acc = economy.getPlayerAccount(owner);
		return acc.getHoldings(getCurrency()).hasEnough(amount);
	}

	@SideOnly(Side.SERVER)
	protected void ownerAddMoney(double amount)
	{
		Account acc = economy.getPlayerAccount(owner);
		acc.getHoldings(getCurrency()).deposit(amount);
	}

	@SideOnly(Side.SERVER)
	protected void ownerSubtractMoney(double amount)
	{
		Account acc = economy.getPlayerAccount(owner);
		acc.getHoldings(getCurrency()).withdraw(amount);
	}
	
	@SideOnly(Side.SERVER)
	private void doSell(EntityPlayerMP player, int amount) // from shop to player
	{
		if(amount <= 0)
			return;
		double cost = getSellCost(amount);
		Holdings holds = player.getAccount().getHoldings(getCurrency());
		if(!holds.hasEnough(cost))
		{
			msg(player, 1);
		}
		else if(!canSell(amount))
		{
			msg(player, 2);
		}
		else
		{
			holds.withdraw(cost);
			ownerAddMoney(cost);
			transferToPlayer(player, amount);
			onPostSell(amount);
			markDirty();
		}
	}
	
	@SideOnly(Side.SERVER)
	private void doBuy(EntityPlayerMP player, int amount) // from player to shop
	{
		if(amount <= 0)
			return;
		if(this.buyPrice == 0)
			return;
		double cost = getBuyCost(amount);
		if(!ownerHasMoney(cost))
		{
			msg(player, 6);
		}
		else if(!canBuy(amount))
		{
			msg(player, 3);
		}
		else if(InventoryUtil.countItems(player.inventory, product) < amount)
		{
			msg(player, 5);
		}
		else
		{
			player.getAccount().getHoldings(getCurrency()).deposit(cost);
			ownerSubtractMoney(cost);
			transferToShop(player, amount);
			onPostBuy(amount);
			markDirty();
		}
	}
	
	@SideOnly(Side.SERVER)
	protected void transferToPlayer(EntityPlayerMP player, int amount)
	{
		for(int i = inventory.getSizeInventory()-1; i >= 0 && amount > 0; i--)//inv item order
		{
			ItemStack is = inventory.getStackInSlot(i);
			if(is != null && InventoryUtil.isStacksEquals(is, product))
			{
				int isize = is.stackSize;
				if(isize <= amount)
				{
					amount -= isize;
					inventory.getContents()[i] = null;
					InventoryUtil.addItem(player, is);						
				}
				else
				{ // stack size larger then requested
					is.stackSize -= amount;
					is = is.copy();
					is.stackSize = amount;
					InventoryUtil.addItem(player, is);
					amount = 0;
				}
			}
		}
	}
	
	@SideOnly(Side.SERVER)
	protected void transferToShop(EntityPlayerMP player, int amount)
	{
		for(int i = 0; i < 36; i++) //36 - without armor slots
		{
			if(amount == 0) break;
			ItemStack is = player.inventory.getStackInSlot(i);
			if(is != null && InventoryUtil.isStacksEquals(is, product))
			{
				if(is.stackSize <= amount)
				{
					amount -= is.stackSize;
					player.inventory.setInventorySlotContents(i, null);
					InventoryUtil.addItem(inventory, is, false, false);
				}
				else
				{ // stack size larger then requested
					is.stackSize -= amount;
					player.inventory.setInventorySlotContents(i, is);
					is = is.copy();
					is.stackSize = amount;
					InventoryUtil.addItem(inventory, is, false, false);
					amount = 0;
				}
			}
		}
	}
	
	@SideOnly(Side.CLIENT)
	public double getMaxRenderDistanceSquared()
	{
		return 1024.0D;
	}
	
	private final void openGUI(EntityPlayer player, int id)
	{
		player.openGui(UMShops.instance(), id, worldObj, xCoord, yCoord, zCoord);
	}
	
	private final void openGuiWithoutContainer(EntityPlayer player, int id)
	{
		new UMPacketOpenGui(id, xCoord, yCoord, zCoord).sendTo((EntityPlayerMP)player);
	}
	
	protected void sendTileEntityPacket()
	{
		if(worldObj != null && !worldObj.isRemote)
			worldObj.markBlockForUpdate(xCoord, yCoord, zCoord);
		/*
		Packet p = getDescriptionPacket();
		MinecraftServer server = MinecraftServer.getServer();
		if(server == null || worldObj == null || worldObj.isRemote || p == null)
			return;
		PlayerInstance pi = ((WorldServer)worldObj).getPlayerManager().getOrCreateChunkWatcher(xCoord >> 4, zCoord >> 4, false);
		if(pi != null)
			pi.sendToAllPlayersWatchingChunk(p);
		*/
	}
	
	private class ProductInventoryWrapper implements IInventory
	{
		@Override
		public int getSizeInventory()
		{
			return 1;
		}

		@Override
		public ItemStack getStackInSlot(int slot)
		{
			return product;
		}

		@Override
		public ItemStack decrStackSize(int slot, int size)
		{
			ItemStack is = product;
			if(is != null)
			{
				if(is.stackSize <= size)
				{
					product = null;
					markDirty();
					return is;
				}

				ItemStack is2 = is.splitStack(size);

				if(is.stackSize == 0)
					product = null;

				markDirty();
				return is2;
			}
			else
			{
				return null;
			}
		}

		@Override
		public ItemStack getStackInSlotOnClosing(int slot)
		{
			ItemStack is = product;
			product = null;
			return is;
		}

		@Override
		public void setInventorySlotContents(int slot, ItemStack is)
		{
			product = is;
			markDirty();
		}

		@Override
		public String getInventoryName()
		{
			return "ultramine.shop.container.shop";
		}

		@Override
		public boolean hasCustomInventoryName()
		{
			return false;
		}

		@Override
		public int getInventoryStackLimit()
		{
			return 64;
		}

		@Override
		public void markDirty()
		{
			TileEntityShop.this.markDirty();
		}

		@Override
		public boolean isUseableByPlayer(EntityPlayer player)
		{
			return TileEntityShop.this.isUseableByPlayer(player);
		}

		@Override
		public void openInventory()
		{
			
		}

		@Override
		public void closeInventory()
		{
			
		}

		@Override
		public boolean isItemValidForSlot(int p_94041_1_, ItemStack p_94041_2_)
		{
			return false;
		}
	}
}
