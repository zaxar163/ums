package org.ultramine.mods.shops.blocks;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.MathHelper;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import org.ultramine.mods.shops.te.TileEntityIndicator;
import org.ultramine.mods.shops.te.TileEntityShop;

public class BlockIndicator extends BlockContainer
{
	public BlockIndicator()
	{
		super(Material.cloth);
		setCreativeTab(CreativeTabs.tabDecorations);
		setBlockName("um_shop_indicator");
		setBlockTextureName("minecraft:wool_colored_black");
		setBlockBounds(0.0F, 0.0F, 0.4F, 1.0F, 1.0F, 0.6F);
	}

	@Override
	public TileEntity createNewTileEntity(World world, int meta)
	{
		return new TileEntityIndicator();
	}

	@Override
	public void setBlockBoundsForItemRender()
	{
		setBlockBounds(0.0F, 0.0F, 0.4F, 1.0F, 1.0F, 0.6F);
	}

	@Override
	public void setBlockBoundsBasedOnState(IBlockAccess world, int x, int y, int z)
	{
		switch(world.getBlockMetadata(x, y, z))
		{
		case 3: setBlockBounds(0.875F, 0.0F, 0.0F, 1.0F, 1.0F, 1.0F); return;
		case 1: setBlockBounds(0.0F, 0.0F, 0.0F, 0.125F, 1.0F, 1.0F); return;
		case 0: setBlockBounds(0.0F, 0.0F, 0.875F, 1.0F, 1.0F, 1.0F); return;
		case 2: setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 1.0F, 0.125F); return;
		}
	}
	
	@SideOnly(Side.CLIENT)
	public AxisAlignedBB getSelectedBoundingBoxFromPool(World world, int x, int y, int z)
	{
		this.setBlockBoundsBasedOnState(world, x, y, z);
		return super.getSelectedBoundingBoxFromPool(world, x, y, z);
	}

	@Override
	public AxisAlignedBB getCollisionBoundingBoxFromPool(World world, int x, int y, int z)
	{
		setBlockBoundsBasedOnState(world, x, y, z);
		return super.getCollisionBoundingBoxFromPool(world, x, y, z);
	}
	
	@Override
	public boolean isOpaqueCube()
	{
		return false;
	}
	
	@Override
	public boolean renderAsNormalBlock()
	{
		return false;
	}
	
	@Override
	public void onBlockPlacedBy(World world, int x, int y, int z, EntityLivingBase entity, ItemStack is)
	{
		world.setBlockMetadataWithNotify(x, y, z, MathHelper.floor_double((double)((entity.rotationYaw * 4F) / 360F) + 0.5D) & 3, 3);
	}
	
	@Override
	public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int side, float hitX, float hitY, float hitZ)
	{
		TileEntity te = world.getTileEntity(x, y-1, z);
		if(te instanceof TileEntityShop)
		{
			if(world.isRemote)
				return ((TileEntityShop)te).activateClient(player);
			else /**/ if(FMLCommonHandler.instance().getSide().isServer())
				return ((TileEntityShop)te).activateServer(player);
		}
		
		return true;
	}
}
