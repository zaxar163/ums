package org.ultramine.mods.shops;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLServerStartingEvent;

@Mod(modid = "UMShops", name = "UltraMine Shops", version = "@version@", acceptableRemoteVersions = "*")
public class UMShops
{
	@Mod.Instance("UMShops")
	private static UMShops instance;
	
	@SidedProxy(clientSide="org.ultramine.mods.shops.InitClient", serverSide="org.ultramine.mods.shops.InitServer")
	public static InitCommon initializer;
	
	public static UMShops instance()
	{
		return instance;
	}
	
	@EventHandler
	public void init(FMLInitializationEvent e)
	{
		initializer.initCommon();
		initializer.initSided();
	}
	
	@EventHandler
	public void start(FMLServerStartingEvent e)
	{
		
	}
}
