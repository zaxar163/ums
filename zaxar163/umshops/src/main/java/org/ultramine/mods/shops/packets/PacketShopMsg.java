package org.ultramine.mods.shops.packets;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.network.NetHandlerPlayClient;
import net.minecraft.network.PacketBuffer;
import org.ultramine.gui.GuiScreenToGui;
import org.ultramine.mods.shops.gui.GuiShopBuy;
import org.ultramine.network.UMPacket;

import java.io.IOException;

public class PacketShopMsg extends UMPacket
{
	public int msg;
	
	public PacketShopMsg(){}
	public PacketShopMsg(int msg)
	{
		this.msg = msg;
	}

	@Override
	public void write(PacketBuffer buf) throws IOException
	{
		buf.writeByte(msg);
	}

	@Override
	public void read(PacketBuffer buf) throws IOException
	{
		msg = buf.readByte();
	}

	@SideOnly(Side.CLIENT)
	public void processClient(NetHandlerPlayClient net)
	{
		GuiScreen gui = Minecraft.getMinecraft().currentScreen;
		if(gui instanceof GuiScreenToGui)
    	{
			GuiScreenToGui gui1 = (GuiScreenToGui) gui;
			if(gui1.gui instanceof GuiShopBuy)
			{
	    		((GuiShopBuy)gui1.gui).acceptMessage(msg);
			}
    	}
	}
}
