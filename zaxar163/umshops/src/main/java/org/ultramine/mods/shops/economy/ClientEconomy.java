package org.ultramine.mods.shops.economy;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class ClientEconomy
{
	public static final ClientEconomy GSC = new ClientEconomy("\u00a7a:\u00a76%s$");
	public static final ClientEconomy RUR = new ClientEconomy("\u00a7a:\u00a76%sՔ");
	
	private final String format;
	private double money = 0.0F;
	
	private ClientEconomy(String format)
	{
		this.format = format;
	}
	
	public double getMoney()
	{
		return money;
	}
	
	public void setMoney(double amount)
	{
		money = amount;
	}
	
	public void add(double amount)
	{
		money += amount;
	}
	
	public void subtract(double amount)
	{
		money -= amount;
	}
	
	private double lastAmount;
	private String lastStr = "\u00a7a:\u00a760.00$";
	
	public String getGameString()
	{
		if(lastAmount == money)
			return lastStr;
		
		String moneyS = String.format("%.2f", getMoney()).replace(',', '.');
		String newStr = String.format(format, moneyS);
		
		lastAmount = money;
		lastStr = newStr;
		return  newStr;
	}
	
	public boolean enabled()
	{
		return money != 0.0d;
	}
}
