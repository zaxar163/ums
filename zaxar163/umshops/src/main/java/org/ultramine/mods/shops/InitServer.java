package org.ultramine.mods.shops;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.PlayerEvent;
import cpw.mods.fml.common.gameevent.TickEvent;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraftforge.common.MinecraftForge;
import org.ultramine.core.economy.service.Economy;
import org.ultramine.core.service.InjectService;
import org.ultramine.mods.shops.packets.PacketPlayerMoney;
import org.ultramine.server.util.GlobalExecutors;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@SideOnly(Side.SERVER)
public class InitServer extends InitCommon
{
	@InjectService private static Economy economy;
	private final Map<UUID, Double> lastBalances = new HashMap<>();

	@Override
	void initSided()
	{
		FMLCommonHandler.instance().bus().register(this);
		MinecraftForge.EVENT_BUS.register(this);
	}
	
	@SubscribeEvent
	public void onPlayerLoggedIn(PlayerEvent.PlayerLoggedInEvent e)
	{
		updateHoldings((EntityPlayerMP)e.player);
	}

	@SubscribeEvent
	public void onPlayerLoggedOut(PlayerEvent.PlayerLoggedOutEvent e)
	{
		//noinspection SuspiciousMethodCalls
		lastBalances.remove(e.player);
	}
	
	@SubscribeEvent
	public void onTick(TickEvent.ServerTickEvent e)
	{
		List<?> players = MinecraftServer.getServer().getConfigurationManager().playerEntityList;
		int index = MinecraftServer.getServer().getTickCounter() % Math.max(players.size(), 20);
		if(index < players.size())
			updateHoldings((EntityPlayerMP) players.get(index));
	}
	
	private void updateHoldings(final EntityPlayerMP player)
	{
		economy.getPlayerAccount(player).getDefaultHoldings().asAsync().getBalance().thenAcceptAsync(balance -> {
			if(player.playerNetServerHandler != null && MinecraftServer.getServer().getConfigurationManager().playerEntityList.contains(player)) {
				Double lastBalance = lastBalances.put(player.getGameProfile().getId(), balance);
				if(lastBalance == null || !lastBalance.equals(balance))
					new PacketPlayerMoney(balance, 0).sendTo(player);
			}
		}, GlobalExecutors.syncServer());
	}
}
