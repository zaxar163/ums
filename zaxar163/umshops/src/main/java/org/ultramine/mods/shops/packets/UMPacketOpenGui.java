package org.ultramine.mods.shops.packets;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.network.NetHandlerPlayClient;
import net.minecraft.network.PacketBuffer;
import org.ultramine.mods.shops.UMShops;
import org.ultramine.network.UMPacket;

import java.io.IOException;

public class UMPacketOpenGui extends UMPacket
{
	public int GuiID;
	public int x;
	public int y;
	public int z;

	public UMPacketOpenGui(){}
	public UMPacketOpenGui(int id, int x, int y, int z)
	{
		GuiID = id;
		this.x = x;
		this.y = y;
		this.z = z;
	}

	@Override
	public void write(PacketBuffer data) throws IOException
	{
		data.writeInt(GuiID);
		data.writeInt(x);
		data.writeInt(y);
		data.writeInt(z);
	}

	@Override
	public void read(PacketBuffer data) throws IOException
	{
		GuiID = data.readInt();
		x = data.readInt();
		y = data.readInt();
		z = data.readInt();
	}
	
	@SideOnly(Side.CLIENT)
	public void processClient(NetHandlerPlayClient net)
	{
		Minecraft.getMinecraft().thePlayer.openGui(UMShops.instance(), GuiID, Minecraft.getMinecraft().theWorld, x, y, z);
	}
}
