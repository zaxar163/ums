package org.ultramine.mods.shops.gui;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.inventory.IInventory;
import net.minecraft.util.ResourceLocation;
import org.ultramine.gui.ElementButton;
import org.ultramine.gui.GuiContainer;
import org.ultramine.gui.IGuiElement;
import org.ultramine.mods.shops.te.TileEntityAdminShop;
import org.ultramine.mods.shops.te.TileEntityShop;

import static org.ultramine.util.I18n.tlt;

@SideOnly(Side.CLIENT)
public class GuiShopMain extends GuiContainer
{
	private static final ResourceLocation texture = new ResourceLocation("umshops:textures/gui/shopmain.png");
	
	private IInventory playerInv;
	private TileEntityShop shop;
	
	public GuiShopMain(IInventory player, TileEntityShop shop)
	{
		super(new ContainerVetrino(player, shop.getProductInventory(), 0));
		this.playerInv = player;
		this.shop = shop;
		setSize(176, 166);
		setBG(texture);
		
		addElement(new ElementButton(0, 13, 30, 60, 14, tlt("ultramine.shop.guimain.settings")));
		if(!(shop instanceof TileEntityAdminShop))
			addElement(new ElementButton(1, 103, 30, 60, 14, tlt("ultramine.shop.guimain.inventory")));
		
		addElement(new ElementButton(3, 103, 46, 60, 14, tlt("ultramine.shop.guimain.buy")));
		//addElement(new ElementButton(4, 13, 46, 60, 14, "RENT"));
		addElement(new ElementButton(4, 130, 62, 40, 12, tlt("ultramine.shop.guimain.remove")));
	}
	
	@Override
	public void actionPerformed(int id, IGuiElement el, Object... data)
	{
		switch(id)
		{
		case 0:
			shop.sendGuiCommand(0);
			return;

		case 1: 
			shop.sendGuiCommand(1);
			return;
		case 2:
			
			return;
		case 3:
			shop.sendGuiCommand(3);
			return;
		
		case 4:
			shop.sendGuiCommand(9);
			return;
		}
	}
	
	@Override
	protected void drawForeground(int mx, int my)
	{
		super.drawForeground(mx, my);
		mc.fontRenderer.drawString(tlt(shop.inventory.getInventoryName()), guiLeft + 60, guiTop + 6, 0x404040);
		mc.fontRenderer.drawString(shop.getOwner().getName(), guiLeft + 120, guiTop + 20, 0x404040);
		mc.fontRenderer.drawString(tlt(playerInv.getInventoryName()), guiLeft + 8, guiTop + (ySize - 96) + 2, 0x404040);
	}
}
