package org.ultramine.mods.shops.logic;

import net.minecraft.nbt.NBTTagCompound;
import org.ultramine.mods.shops.packets.PacketTEShop;

public abstract class AbstractShopLogic
{
	public void countBuyCost(int count)
	{
		
	}
	
	public void countSellCost(int count)
	{
		
	}
	
	public abstract void readFromNBT(NBTTagCompound nbt);
	
	public abstract void writeToNBT(NBTTagCompound nbt);
	
	public abstract void writeToPacket(PacketTEShop p);
	
	public abstract void readFromPacket(PacketTEShop p);
}
