package org.ultramine.bootstrap.util;

import org.ultramine.bootstrap.util.Resources;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;

public class UmSslUtil
{
	private static boolean useHttps = true;

	public static boolean isUseHttps()
	{
		return useHttps;
	}

	public static void checkOrInstall()
	{
		//TODO connection caching issues
//		try
//		{
//			new URL("https://maven.ultramine.ru").openConnection().getInputStream().close();
//		}
//		catch (SSLHandshakeException e)
//		{
			installDSTRootCA();
//		}
//		catch(IOException e)
//		{
//			throw new ApplicationErrorException(e, "error.unavailable.maven", "maven.ultramine.ru");
//		}
	}

	private static void installDSTRootCA()
	{
		try
		{
			KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
			Path ksPath = Paths.get(System.getProperty("java.home"),
					"lib", "security", "cacerts");
			keyStore.load(Files.newInputStream(ksPath),
					"changeit".toCharArray());

			CertificateFactory cf = CertificateFactory.getInstance("X.509");
			try(InputStream caInput = new BufferedInputStream( Resources.getAsStream("/assets/crt/DSTRootCAX3.pem")))
			{
				Certificate crt = cf.generateCertificate(caInput);
				keyStore.setCertificateEntry("DSTRootCAX3", crt);
			}

			TrustManagerFactory tmf = TrustManagerFactory
					.getInstance(TrustManagerFactory.getDefaultAlgorithm());
			tmf.init(keyStore);
			SSLContext sslContext = SSLContext.getInstance("TLS");
			sslContext.init(null, tmf.getTrustManagers(), null);
			SSLContext.setDefault(sslContext);
		}
		catch (Throwable e)
		{
			useHttps = false;
		}
	}
}
