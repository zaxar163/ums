package org.ultramine.bootstrap.maven;

import org.ultramine.bootstrap.deps.IRepository;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class MavenRemoteRepository implements IRepository
{
	private final String url;

	public MavenRemoteRepository(String url)
	{
		this.url = url;
	}

	public String getURL()
	{
		return url;
	}

	@Override
	public InputStream resolve(String path) throws IOException
	{
		HttpURLConnection conn = makeConnection(new URL(getURL() + "/" + path));
		int status = conn.getResponseCode();
		if(status / 100 != 2)
			return null;
		return conn.getInputStream();
	}

	protected HttpURLConnection makeConnection(URL url) throws IOException
	{
		HttpURLConnection connection = (HttpURLConnection)url.openConnection();
		connection.setUseCaches(false);
		connection.setDefaultUseCaches(false);
		connection.setRequestProperty("Cache-Control", "no-store,max-age=0,no-cache");
		connection.setRequestProperty("Expires", "0");
		connection.setRequestProperty("Pragma", "no-cache");
		connection.setConnectTimeout(5000);
		connection.setReadTimeout(30000);
		return connection;
	}

	@Override
	public String toString()
	{
		return url;
	}
}
