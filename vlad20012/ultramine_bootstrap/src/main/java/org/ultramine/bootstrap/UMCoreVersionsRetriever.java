package org.ultramine.bootstrap;

import org.ultramine.bootstrap.maven.MavenMetadata;
import org.ultramine.bootstrap.versioning.DefaultArtifactVersion;

import java.util.ArrayList;
import java.util.List;

public class UMCoreVersionsRetriever
{
	private final List<DefaultArtifactVersion> betaVersions;
	private final List<DefaultArtifactVersion> stableVersions;

	public UMCoreVersionsRetriever()
	{
		List<DefaultArtifactVersion> versions = MavenMetadata.loadFromProject(Constants.UM_REPO, Constants.UM_CORE_GROUP, Constants.UM_CORE_NAME).getVersions();

		betaVersions = new ArrayList<>(versions.size());
		stableVersions = new ArrayList<>(versions.size());
		for(DefaultArtifactVersion version : versions)
			(version.getLabel().contains("beta") ? betaVersions : stableVersions).add(version);
	}

	public List<DefaultArtifactVersion> getBetaVersions()
	{
		return betaVersions;
	}

	public List<DefaultArtifactVersion> getStableVersions()
	{
		return stableVersions;
	}
}
